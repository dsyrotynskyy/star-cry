﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasScaler))]
public class CanvasScalerByDevice : MonoBehaviour {

	public CanvasScaler canvasScaler;
	public Vector2 normalRes = Vector3.zero;
	public Vector2 iphoneXRes= Vector3.zero;

	void Start () {
		if (DeviceHelper.IsIPhoneX) {
			canvasScaler.referenceResolution = iphoneXRes;
		} else {
			canvasScaler.referenceResolution = normalRes;
		}
	}
}
