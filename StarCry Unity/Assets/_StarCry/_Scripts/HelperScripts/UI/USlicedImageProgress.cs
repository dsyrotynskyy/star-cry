﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class USlicedImageProgress : MonoBehaviour {

	public RectTransform progressTransform;
	public float fullWidth = 500;

	public float FillAmount {
		set {
			float newWidth = value * fullWidth;

			var v = progressTransform.sizeDelta;
			v.x = newWidth;
			progressTransform.sizeDelta = v;
		}
		get {
			return progressTransform.rect.width / fullWidth;
		}
	}
}
