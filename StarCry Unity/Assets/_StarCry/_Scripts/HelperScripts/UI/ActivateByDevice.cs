﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateByDevice : MonoBehaviour {

	public GameObject[] normal;
	public GameObject[] iphoneXGO;

	void Start () {
		bool isIphoneX = DeviceHelper.IsIPhoneX;
		foreach (var v in normal) {
			v.SetActive (!isIphoneX);
		}

		foreach (var d in iphoneXGO) {
			d.SetActive (isIphoneX);
		}
	}
}
