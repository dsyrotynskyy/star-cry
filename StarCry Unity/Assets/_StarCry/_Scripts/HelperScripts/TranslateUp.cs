﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateUp : MonoBehaviour {

    public float moveUpPower = 0.1f;
	
	void Update () {
        transform.Translate(Vector2.up * moveUpPower);
	}
}
