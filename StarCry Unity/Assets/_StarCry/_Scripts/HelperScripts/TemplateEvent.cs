﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class TemplateEvent<T> : UnityEvent<T> {
    
}
