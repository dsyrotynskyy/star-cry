﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UResourcesPanel : MonoBehaviour {

    public Player myPlayer;
    public Text playerResources;

    void Start()
    {
        myPlayer.onResourceAmountChangedEvent.AddListener(UpdateUI);
        UpdateUI();
    }

	void UpdateUI()
    {
        playerResources.text = myPlayer.Energy.ToString();
    }
}
