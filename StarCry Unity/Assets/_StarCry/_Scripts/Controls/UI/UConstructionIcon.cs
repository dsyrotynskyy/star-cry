﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UConstructionIcon : MonoBehaviour 
     , IPointerEnterHandler
     , IPointerExitHandler {

    Color _imageOriginal;
    Color _imageOriginal2;
    public Button myButton;
    ConstructionMarker _marker;
    public Text constructionCost;
    public Text buildingTitle;
    public Building targetBuilding;

	void Start () {
        _imageOriginal = myButton.colors.normalColor;
        _imageOriginal2 = myButton.colors.highlightedColor;
        _marker = ConstructionMarker.GetInstance;
        myButton.onClick.AddListener(ButtonClick_Toggle);
        _marker.onStateChangedEvent.AddListener(UpdateVisuals);

        buildingTitle.text = targetBuilding.Title;
        constructionCost.text = targetBuilding.ConstructionCost.ToString ();
	}

    private void ButtonClick_Toggle()
    {
        if (_marker.targetBuilding == this.targetBuilding) {
            _marker.DisableBuildingMarker();
        } else {
            _marker.EnableBuildingMarker(targetBuilding);
        }

        UpdateVisuals();
    }

    private void OnDisable()
    {
        if (!_marker) {
            return;
        }

        if (_marker.targetBuilding == this.targetBuilding) {
            _marker.DisableBuildingMarker();
        } 
    }

    void UpdateVisuals () {
        if (_marker.targetBuilding == this.targetBuilding)
        {
            var n = myButton.colors;
            n.normalColor = Color.green;
            n.highlightedColor = Color.green;
            myButton.colors = n;
        }
        else
        {
            var n = myButton.colors;
            n.normalColor = _imageOriginal;
            n.highlightedColor = _imageOriginal2;
            myButton.colors = n;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _marker.enabled = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _marker.enabled = true;
    }
}
