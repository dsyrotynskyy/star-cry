﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UPlayerInfo : MonoBehaviour {

	public Nexus myNexus;
	public Text nexusHP;

	void Start () {
		myNexus.onDamageTakenEvent.AddListener (UpdateNexusLife);
		myNexus.onDestroyedEvent.AddListener (OnNexusDestroyed);
		Invoke ("UpdateNexusLife", 0f);
	}

	void UpdateNexusLife () {
		nexusHP.text = myNexus.curHP.ToString ();
	}

	void OnNexusDestroyed () {
		myNexus.onDamageTakenEvent.RemoveListener (UpdateNexusLife);
		myNexus.onDamageTakenEvent.RemoveListener (OnNexusDestroyed);
	}
}
