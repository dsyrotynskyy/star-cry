﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using _StarCry._Scripts.Game.Entities;
using System;

public class USpawnUnitButton : MonoBehaviour 
     , IPointerEnterHandler
     , IPointerExitHandler
{

	Color _imageOriginal;
    Color _imageOriginal2;
    public Button myButton;

	public Text price;
	public Text unitTitle;

	IUnitSpawner _unitSpawner;
	IUnit _unitPrefab;
    
    void Awake()
    {
        _imageOriginal = myButton.colors.normalColor;
        _imageOriginal2 = myButton.colors.highlightedColor;

		myButton.onClick.AddListener(ButtonClick);

		Player.Human.onResourceAmountChangedEvent.AddListener(OnResourcesAmountChanged);
    }

	private void OnResourcesAmountChanged()
	{
		UpdateVisuals();
	}

	internal void Display(IUnitSpawner currentSpawner, IUnit unitPrefab)
	{
		_unitSpawner = currentSpawner;
		_unitPrefab = unitPrefab;

		price.text = unitPrefab.Price.ToString ();
		unitTitle.text = unitPrefab.Title;

		UpdateVisuals();
	}

	private void ButtonClick()
    {
		_unitSpawner.TrySpawn(_unitPrefab);
    }

    void UpdateVisuals()
    {
		if (gameObject.activeInHierarchy) {
			myButton.interactable = _unitSpawner.CanSpawn(_unitPrefab);
		}
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
//        _marker.enabled = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
//        _marker.enabled = true;
    }
}
