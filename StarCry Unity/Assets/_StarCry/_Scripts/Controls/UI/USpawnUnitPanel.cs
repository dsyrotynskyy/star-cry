﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _StarCry._Scripts.Game.Entities;
using System;

public class USpawnUnitPanel : MonoBehaviour {

	InputController _inputController;
	ISelectableEntity current { get { return _inputController.CurrentEntity; } }
	public USpawnUnitButton[] myButtons; 

	private void Awake()
	{
		_inputController = InputController.GetInstance;
	}

    internal void PrepareUI(ISelectableEntity current)
    {
        var mono = ((MonoBehaviour)current);
        IUnitSpawner currentSpawner = mono.GetComponent<IUnitSpawner>();
        for (int i = 0; i < myButtons.Length; i++)
        {
            var b = myButtons[i];
            if (currentSpawner.Deck.Length < i)
            {
                b.gameObject.SetActive(false);
                continue;
            }

            var unit = currentSpawner.Deck[i];
            if (unit != null)
            {
                b.Display(currentSpawner, unit);
            }
            else
            {
                b.gameObject.SetActive(false);
            }
        }
    }
}
