﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UControlPanel : MonoBehaviour {

    InputController _inputController;
    public RallyPoint rallyPointPrefab;
    public ConstructionMarker constructionMarkerPrefab;

    RallyPoint rallyPoint;
    ConstructionMarker constructionMarker;
    public UConstructionPanel constructionPanel;
	public USpawnUnitPanel spawnUnitPanel;

	void Start () {
        constructionMarker = Instantiate(constructionMarkerPrefab);
        rallyPoint = Instantiate(rallyPointPrefab);

        _inputController = InputController.GetInstance;
        _inputController.onSelectableChangedEvent.AddListener(SetupUI);
        _inputController.onRallyPointChangedPosition.AddListener(OnRallyPointChangedPosition);

//        rallyPoint.OnRallyPointChangedEvent.AddListener(OnRallyPointChanged);

        SetupUI(null);
	}

    private void OnRallyPointChangedPosition(Vector3 pos)
    {
        rallyPoint.transform.position = pos;
    }

    //private void OnRallyPointChanged()
    //{
    //    if (!current.HasRallyPoint) {
    //        Debug.LogError("WTF");
    //    } else {
    //        current.SetRallyPoint(rallyPoint.transform.position);
    //    }
    //}

    void SetupUI (ISelectableEntity current) {
        if (current == null) {
            rallyPoint.gameObject.SetActive(false);
            constructionMarker.gameObject.SetActive(false);
            constructionPanel.gameObject.SetActive(false);
			spawnUnitPanel.gameObject.SetActive(false);
        } else {
			spawnUnitPanel.gameObject.SetActive(current.CanSpawnUnits);
            if (current.CanSpawnUnits) {
                spawnUnitPanel.PrepareUI(current);
            }

            constructionMarker.gameObject.SetActive(current.CanConstructBuildings);
            constructionPanel.gameObject.SetActive(current.CanConstructBuildings);

            rallyPoint.gameObject.SetActive(current.HasRallyPoint);
            if (current.HasRallyPoint) {
                OnRallyPointChangedPosition(current.RallyPoint);
            }
        }
	}
}
