﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UTouchTypeControlPanel : MonoBehaviour {

    public Button button;
    public Text selectionText;
    TouchInputController _touchInputController;

    private void Start()
    {
        button.onClick.AddListener(ToggleSelectionMode);

        _touchInputController = TouchInputController.GetInstance;
        _touchInputController.onTouchControllerChanged.AddListener(UpdateUI);

        UpdateUI();
    }

    void ToggleSelectionMode() {
        _touchInputController.ToggleTouchController();
    }

    void UpdateUI () {
        if (_touchInputController.IsCameraDragging) {
            selectionText.text = "OFF";
            selectionText.color = Color.red;
        } else {
            selectionText.text = "ON";
            selectionText.color = Color.green;
        }
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.LeftControl)) {
            ToggleSelectionMode();
        }    
    }
}
