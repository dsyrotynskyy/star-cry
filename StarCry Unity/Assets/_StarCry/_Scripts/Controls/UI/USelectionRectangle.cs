﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class USelectionRectangle : UnitySingleton<USelectionRectangle>, ITouchDragHandler
{
    public Image selectionRectangle;

    UnitSelectionManager unitSelectionManager;

    Camera _main;

    void Start()
    {
        _main = Camera.main;
        unitSelectionManager = UnitSelectionManager.GetOrCreateInstance;
    }

    public void DragLastFrame(Vector3 offset)
    {
    }

    public void OnDragEnded()
    {
        selectionRectangle.enabled = false;

        SelectMultipleUnits(_startPos, _endPos);
    }

    Vector3 _startPos;

    public void OnDragStarted(Vector3 startPos)
    {
        _startPos = startPos;
        selectionRectangle.enabled = false;
    }

    Vector3 _endPos;

    public void Drag(Vector3 startPos, Vector3 currentPos)
    {
        _startPos = startPos;
        _endPos = currentPos;

        selectionRectangle.gameObject.SetActive(true);
        selectionRectangle.enabled = true;

        Vector3 p1, p2;
        p1 = _startPos;
        p2 = currentPos;

        Vector2 leftUp = new Vector2(Math.Min(p1.x, p2.x), Math.Min(p1.y, p2.y));
        Vector2 rightDown = new Vector2(Math.Max(p1.x, p2.x), Math.Max(p1.y, p2.y));

        Debug.Log("Drawing rectange from " + leftUp + " to " + rightDown);
        Debug.DrawLine(leftUp, rightDown);
        //Utils.drawDebugRectangle(p1,p2);

        selectionRectangle.rectTransform.position = leftUp;

        Vector2 size = rightDown - leftUp;

        selectionRectangle.transform.position = leftUp;
        selectionRectangle.rectTransform.sizeDelta = size;
    }

    Vector3 ScreenToWorld (Vector3 screenPos) {
        Ray ray = _main.ScreenPointToRay(screenPos);

        RaycastHit hit;

        int layer_mask = LayerMask.GetMask("Ground");

        if (Physics.Raycast(ray, out hit, 100f, layer_mask))
        {
            Vector3 targetPosition;

            if (hit.transform.CompareTag("Ground"))
            {
                targetPosition = hit.point;
                return targetPosition;
            }
        }

        throw new ArgumentException();
    }

    void SelectMultipleUnits(Vector3 fromScreen, Vector3 toScreen)
    {
        Vector3 from = ScreenToWorld(fromScreen);
        Vector3 to = ScreenToWorld(toScreen);

        //var c1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //c1.transform.position = from;
        //c1.transform.localScale = Vector3.one / 3f;

        //var c2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //c2.transform.position = to;
        //c2.transform.localScale = Vector3.one / 3f;

        Rect rect = new Rect(Mathf.Min(from.x, to.x), Mathf.Min(from.z, to.z),
                      Mathf.Abs(to.x - from.x),
                      Mathf.Abs(to.z - from.z));

        //Select all units inside rect
        Player humanPlayer = Player.Human;

        unitSelectionManager.ClearSelection();

        foreach (var u in unitSelectionManager.AllSelectableUnits)
        {
            Vector2 position = new Vector2(u.transform.position.x, u.transform.position.z);
            if (rect.Contains(position))
            {
                unitSelectionManager.AddSelectedUnit(u);
            }
        }

        unitSelectionManager.FinishSelection();
    }

    public void HandleClick()
    {
        throw new NotImplementedException();
    }
}
