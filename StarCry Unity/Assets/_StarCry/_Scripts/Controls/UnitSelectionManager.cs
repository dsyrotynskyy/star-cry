﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSelectionManager : UnitySingleton<UnitSelectionManager>, ISelectableEntity {

    Vector3 lastRallyPoint = Vector3.zero;
    Player humanPlayer;
    List<UnitSelector> allSelectableUnits = new List<UnitSelector>();

    public List<UnitSelector> AllSelectableUnits { get { return allSelectableUnits; } }

    bool ISelectableEntity.CanSpawnUnits
    {
        get
        {
            return false;
        }
    }

    bool ISelectableEntity.CanConstructBuildings     {
        get
        {
            return false;
        }
    }

    bool ISelectableEntity.HasRallyPoint     {
        get
        {
            return true;
        }
    }

    Vector3 ISelectableEntity.RallyPoint {
        get
        {
            return lastRallyPoint;
        }
    }

    List<UnitSelector> selectedUnits = new List<UnitSelector>();

	void Start () {
        humanPlayer = Player.Human;
    }

    public void ClearSelection()
    {
        foreach (var u in this.selectedUnits)
        {
            u.OnDeselected();
        }
        selectedUnits.Clear();
    }

    public void AddSelectedUnit(UnitSelector u)
    {
        selectedUnits.Add(u);
        u.OnSelected();
    }

    public void FinishSelection()
    {
        InputController.GetInstance.Select(this);
    }

    void OrderMoveSelectedUnits(Vector3 targetPosition)
    {
        foreach (var u in this.selectedUnits)
        {
            u.SetRallyPoint(targetPosition);
        }
    }

    internal void RegisterUnit(UnitSelector unit)
    {
        this.allSelectableUnits.Add(unit);
    }

    internal void UnRegisterUnit(UnitSelector unit)
    {
        this.allSelectableUnits.Remove(unit);
    }

    void ISelectableEntity.OnDeselected()
    {

    }

    void ISelectableEntity.OnSelected()
    {

    }

    void ISelectableEntity.SetRallyPoint(Vector3 rallyPoint)
    {
        lastRallyPoint = rallyPoint;
        OrderMoveSelectedUnits(rallyPoint);
    }

    void ISelectableEntity.SetPlayerOwner(Player playerOwner)
    {

    }
}
