﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TouchInputController : UnitySingleton<TouchInputController>
{
    USelectionRectangle _selectionRectangle;
    MainCameraManager _cameraController;
    Camera _main;

    ITouchDragHandler _touchHandler;

    void Start()
    {
        _main = Camera.main;
        _cameraController = MainCameraManager.GetInstance;
        _selectionRectangle = USelectionRectangle.GetInstance;

        _touchHandler = _cameraController;
//        _touchHandler = _selectionRectangle;
    }

    bool _isDragging = false;
    Vector3 _dragActual;
    Vector3 _dragStart;
    public float minDragOffset = 100f;
    float minDragOffsetSqr { get { return minDragOffset * minDragOffset; } }
    public float offsetDragPower = 0.001f;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _dragActual = Input.mousePosition;
            _dragStart = _dragActual;

            _isDragging = false;
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 offset = _dragActual - Input.mousePosition;

            if (!_isDragging)
            {
                float sqr = (offset.sqrMagnitude);
                if (sqr > minDragOffset)
                {
                    _isDragging = true;
                    _dragStart = Input.mousePosition;
                    _touchHandler.OnDragStarted(_dragStart);
                }
            }

            if (_isDragging)
            {
                _dragActual = Input.mousePosition;

                Vector3 dragOffset = new Vector3(offset.x, 0f, offset.y) * offsetDragPower;
                _touchHandler.DragLastFrame(dragOffset);
                _touchHandler.Drag(_dragStart, _dragActual);
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (!_isDragging && UITouchProcessingFinished)
            {
                bool isPointerOverGameObject = UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject();
                if (!isPointerOverGameObject) {
                    HandleClick();
                }
            }

            _isDragging = false;
            _touchHandler.OnDragEnded();

            if (!IsCameraDragging && UITouchProcessingFinished)
            {
                ToggleTouchController();
            }
        }
    }

    void HandleClick()
    {
        Ray ray = _main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        //            int layer_mask = LayerMask.GetMask("Ground", "Enemy");
        int layer_mask = LayerMask.GetMask("Ground", "Building");

        if (Physics.Raycast(ray, out hit, 100f, layer_mask))
        {
            if (hit.transform == this.transform)
            {
                return;
            }

            Vector3 targetPosition;

            if (hit.transform.CompareTag("Ground"))
            {
                transform.position = hit.point;

                targetPosition = hit.point;

                InputController.GetInstance.HandleClick(targetPosition);
            }
            else
            {

                //SelectHandler d = hit.collider.GetComponent<SelectHandler>();
                //if (!d || !d.enabled) {
                //    transform.position = hit.collider.transform.position;
                //}
            }
            //                OnRallyPointChangedEvent.Invoke();
        }
    }

    public bool IsCameraDragging {
        get {
            return this._touchHandler == this._cameraController;
        }
    }

    public void ToggleTouchController () {
        if (!IsCameraDragging) {
            _touchHandler = _cameraController;
        } else {
            _touchHandler = _selectionRectangle;
            UITouchProcessingFinished = false;
            this.Invoke(0.1f, () => { UITouchProcessingFinished = true; });
        }

        onTouchControllerChanged.Invoke();
    }

    bool UITouchProcessingFinished = true;

    [HideInInspector]
    public UnityEvent onTouchControllerChanged = new UnityEvent();
}

public interface ITouchDragHandler
{
    void OnDragStarted(Vector3 _dragStart);
    void DragLastFrame(Vector3 offset);
    void Drag(Vector3 startPos, Vector3 currentPos);
    void OnDragEnded();
}

