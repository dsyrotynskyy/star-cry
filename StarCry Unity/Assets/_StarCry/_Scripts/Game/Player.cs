﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;
using System.Collections.Generic;

public class Player : MonoBehaviour {

    [HideInInspector]
    public List<IUnit> allUnits = new List<IUnit>();

	public int Id;

	public bool ComparePlayer (Player p) {
		return p.Id == this.Id;
	}

	public static Player Human;

	private void Awake()
	{      
		if (IsHuman) {
			Human = this;
		}
	}

	public bool IsHuman;

	[SerializeField]
	private int _energy = 100;
	public int Energy { 
		get { return _energy; } 
		private set { _energy = value; } 
	}

    public void AddResources(int resourceProduction)
    {
        Energy += resourceProduction;
        onResourceAmountChangedEvent.Invoke();
    }

    public UnityEvent onResourceAmountChangedEvent = new UnityEvent();

    void UseResources(int val)
    {
        Energy -= val;
        onResourceAmountChangedEvent.Invoke();
    }

    internal void AddUnit(IUnit unit)
    {
        allUnits.Add(unit);
    }

    internal void RemoveUnit(IUnit unit)
    {
        allUnits.Remove(unit);
    }

    public bool TryUseResources (int amount) {
        bool res = false;
        if (Energy >= amount) {
            UseResources(amount);
            res = true;
        }
        return res;
    }
}
