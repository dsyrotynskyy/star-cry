﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConfig : UnitySingleton<GameConfig> {

	protected override bool OrderDontDestroyOnLoad
	{
		get
		{
			return true;
		}
	}
}
