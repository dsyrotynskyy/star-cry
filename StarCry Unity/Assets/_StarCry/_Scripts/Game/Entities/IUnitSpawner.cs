﻿using UnityEngine;

namespace _StarCry._Scripts.Game.Entities
{
    public interface IUnitSpawner
    {
        IUnit[] Deck { get; }

        bool CanSpawn(IUnit prefab);
        void TrySpawn(IUnit prefab);
    }
}