﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Factory : Building {

    public int ResourceProduction = 5;
    public float ResourcesProducePeriod = 5f;

    protected override void Start()
    {
        base.Start();
        InvokeRepeating("ProduceResources", ResourcesProducePeriod, ResourcesProducePeriod);
    }

    void ProduceResources () {
        this.playerOwner.AddResources(ResourceProduction);
    }
}
