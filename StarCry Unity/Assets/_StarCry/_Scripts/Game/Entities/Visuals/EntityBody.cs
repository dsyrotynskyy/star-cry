using UnityEngine;
using System.Collections;

public class EntityBody : MonoBehaviour
{
    public GameObject player1Body;
    public GameObject player2Body;

	public void Init(Player owner)
	{
        bool player1 = owner.Id == 1;
        player1Body.SetActive(player1);
        player2Body.SetActive(!player1);
	}
}
