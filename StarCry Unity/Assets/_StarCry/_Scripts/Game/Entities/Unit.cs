﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : DestructableEntity, IUnit {
	public string Id;
	public int Price = 10;
    MovementController _movementController;
    Attacker _attacker;
    Transform _movementTarget;
    UnitManager _unitManager;

    protected override void Start()
    {
        base.Start();

        if (!_inited)
        {
            Init(playerOwner);
        }
    }

    bool _inited;

    public void Init(Player owner)
    {
        _inited = true;

        _movementTarget = new GameObject("Movement Target").transform;
        _movementTarget.transform.position = this.transform.position;
        _movementTarget.transform.parent = WorldManager.GetInstance.transform;

        _movementController = GetComponent<MovementController>();
        _attacker = GetComponent<Attacker>();
        playerOwner = owner;

        _attacker.onTargetChangedEvent.AddListener(CheckAttackerState);

        InvokeRepeating("UpdateAI", 0.5f, 0.5f);

        playerOwner.AddUnit(this);
    }

    private void OnDisable()
    {
        if (playerOwner)
            playerOwner.RemoveUnit(this);
    }

    void UpdateAI () {
        CheckAttackerState();
    }

    private void CheckAttackerState()
    {
        var attackTarget = _attacker.Target;

        float attackerToleranceDistance = 30f;
        bool interactWithTarget = false;

        if (attackTarget) {

            Vector3 offset = attackTarget.transform.position - _movementTarget.transform.position;
            if (offset.sqrMagnitude < attackerToleranceDistance)
            {
                if (_attacker.CanAttack)
                {
                    _movementController.StopMovement();
                }
                else
                {
                    _movementController.FollowTarget(attackTarget.transform, true);
                }

                interactWithTarget = true;
            }
        } 

        if (!interactWithTarget)
        {
            _movementController.FollowTarget(_movementTarget, false);
        }
    }

    public void SetMovementTarget(Transform target, bool follow)
    {
        Vector3 targetPoint = target.position;
        if (follow)
        {
            Debug.LogError("Not supported");
 //           _movementTarget = target;
        }
        _movementTarget.transform.position = targetPoint;
        UpdateAI();
    }

    public void SetMovementTarget(Vector3 movementTarget)
    {
        _movementTarget.transform.position = movementTarget;
        UpdateAI();
    }

    public Vector3 MovementTarget { get { return _movementTarget.position; } }

    Player IUnit.playerOwner
    {
        get
        {
            return this.playerOwner;
        }
    }

	int IUnit.Price
	{
		get
		{
			return Price;
		}
	}

	string IUnit.Id
	{
		get
		{
			return Id;
		}
	}

	public string Title;

	string IUnit.Title
	{
		get
		{
			return Title;
		}
	}
}

public interface IUnit
{
	string Id { get; }
	int Price { get; }
    Player playerOwner { get; }
    void Init(Player playerOwner);
    void SetMovementTarget(Transform target, bool follow);
    void SetMovementTarget(Vector3 movementTarget);
    string Title { get; }
}
