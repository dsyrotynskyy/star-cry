﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using System;

public class Attacker : MonoBehaviour
{
    public float seeRange = 5f;
    public float attackRadius = 3f;
    public float attackPower = 1f;
    public GameObject bulletVisuals;
    public Transform bulletSpawnPosition;
    internal UnityEvent onTargetChangedEvent = new UnityEvent();

    DestructableEntity _entity;
    public float attackPeriod = 1f;

    public Player playerOwner { get { return _entity.playerOwner; } }
    Entity _distanceCalculator;
    FrequencyTimer _attackTimer;

    private void Awake()
    {
        _entity = GetComponent<DestructableEntity>();
        _distanceCalculator = GetComponent<Entity>();
        _distanceCalculator.SetupSeekRange(seeRange);
        _distanceCalculator.onFoundNeighbourEvent.AddListener(CheckTarget);
        _attackTimer = SceneTimeManager.GetInstance.CreateFrequencyTimer(attackPeriod);
    }

    public bool CanAttack
    {
        get
        {
            if (!_target)
            {
                return false;
            }

            float sd = (_target.transform.position - transform.position).sqrMagnitude;
            return sd < attackRadius * attackRadius;
        }
    }

    private void FixedUpdate()
    {
        AttackLogic();
    }

    void AttackLogic()
    {
        if (!_attackTimer.NextTick())
        {
            return;
        }

        if (!_target)
        {
            onTargetChangedEvent.Invoke();
            return;
        }

        if (!CanAttack)
        {
            return;
        }

        float time = (transform.position - _target.transform.position).magnitude / 5f;
        var bullet = Instantiate(bulletVisuals, bulletSpawnPosition.position, bulletSpawnPosition.rotation);
        var t = DOTween.To(() => bullet.transform.position, x => bullet.transform.position = x, _target.transform.position, time);
        t.onComplete += () =>
        {
            Destroy(bullet.gameObject);
            if (_target)
            {
                _target.TakeDamage(transform, attackPower);
            }
        };
    }

    private void CheckTarget()
    {
        if (_target && _target.CompareTag("Unit"))
        {// If has unit target - exit
            return;
        }

        DestructableEntity bestCandidate = null;

        for (int i = 0; i < this._distanceCalculator.ClosestEntities.Count; i++)
        { // First time try to set random enemy
            int r = UnityEngine.Random.Range(0, this._distanceCalculator.ClosestEntities.Count);
            var e = this._distanceCalculator.ClosestEntities[r];
            if (TrySetTarget(e))
            {
                return;
            }
        }

        for (int i = 0; i < this._distanceCalculator.ClosestEntities.Count; i++)
        {
            var e = this._distanceCalculator.ClosestEntities[i];
            if (TrySetTarget(e))
            {
                return;
            }
        }

        SetTarget(bestCandidate);
    }

    bool TrySetTarget(Entity e)
    {
        DestructableEntity p = e.GetComponent<DestructableEntity>();
        if (p != null && p.playerOwner != this.playerOwner)
        {
            SetTarget(p);
            return true;
        }
        return false;
    }

    DestructableEntity _target;

    public DestructableEntity Target
    {
        get
        {
            return _target;
        }
    }

    void SetTarget(DestructableEntity e)
    {
        if (_target)
        {
            return;
        }

        _target = e;

        onTargetChangedEvent.Invoke();

        AttackLogic();
    }
}
