﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Entity : MonoBehaviour {

    public bool IsStructure;

    private Dictionary<Entity, float> _entitiesDistances = new Dictionary<Entity, float>();

    [HideInInspector]
    float _sqrSeekRange = -1;// Is setuped outside
    private List<Entity> _closestEntities; //Only if range bigger 0
    public List<Entity> ClosestEntities { get { return _closestEntities; }}//Only if range bigger 0
    public UnityEvent onFoundNeighbourEvent { get; private set; } //Only if range bigger 0

	EntityManager _entityManager;

	private void Awake()
    {
        _entityManager = EntityManager.GetOrCreateInstance;
    }

    private void OnEnable()
    {
        _entityManager.AddEntity(this);
    }

    private void OnDisable()
    {
        if (_entityManager)
            _entityManager.RemoveEntity(this);
    }

    public void SetupSeekRange (float range) {
        _sqrSeekRange = range * range;
        _closestEntities = new List<Entity>();
        onFoundNeighbourEvent = new UnityEvent();
    }

    public void PrepareCalculation () {
        if (_closestEntities != null) {
            _closestEntities.Clear();
        }

        _entitiesDistances.Clear();
    }
	
    public void CalculateDistance (Entity otherEntity) {
        float d;
        if (otherEntity._entitiesDistances.TryGetValue(otherEntity, out d)) {
            _entitiesDistances.Add(otherEntity, d);
        } else {
            var offset = otherEntity.transform.position - this.transform.position;
            d = offset.sqrMagnitude;
            _entitiesDistances.Add(otherEntity, d);
            otherEntity._entitiesDistances.Add(this, d);
        }
	}

    public void PostCalculation () {
        if (_sqrSeekRange > 0) {
            bool foundSomeone = false;
            foreach (var pair in _entitiesDistances) {
                if (pair.Value <= _sqrSeekRange) {
                    _closestEntities.Add(pair.Key);
                    foundSomeone = true;
                }
            }

            if (foundSomeone) {
                onFoundNeighbourEvent.Invoke();
            }
        }
    }
}
