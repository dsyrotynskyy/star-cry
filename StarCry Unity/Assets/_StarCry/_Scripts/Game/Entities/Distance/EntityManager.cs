﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityManager : UnitySingleton<EntityManager> {

    public float frequency = 1f;
    List<Entity> _allEntities = new List<Entity>();
    List<Entity> _allStructures = new List<Entity>();

    private void Start()
    {
        InvokeRepeating("UpdateLogic", 0f, frequency);
    }

    void UpdateLogic () {
        for (int i = 0; i < _allEntities.Count; i++) {
            var e = _allEntities[i];
            e.PrepareCalculation();
        }

        for (int i = 0; i < _allEntities.Count; i++)
        {
            var e = _allEntities[i];
            for (int j = i + 1; j < _allEntities.Count; j++)
            {
                var e2 = _allEntities[j];
                e.CalculateDistance(e2);
            }
        }

        for (int i = 0; i < _allEntities.Count; i++)
        {
            var e = _allEntities[i];
            e.PostCalculation();
        }
    }

    internal void RemoveEntity(Entity entityDistanceCalculator)
    {
        _allEntities.Remove(entityDistanceCalculator);
        _allStructures.Remove(entityDistanceCalculator);
    }

    internal void AddEntity(Entity entityDistanceCalculator)
    {
        _allEntities.Add(entityDistanceCalculator);
        _allStructures.Add(entityDistanceCalculator);
    }

    public List<Entity> AllStructures { get { return this._allStructures; }}
}
