﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using _StarCry._Scripts.Game.Entities;
using System.Linq;

public class Barracks : Building, IUnitSpawner
{
    public float SpawnPeriod = 1f;
    public Transform SpawnPoint;
    public Transform TargetPoint;

	public Unit[] defaultUnits;

	private void Awake()
	{
		Deck = defaultUnits.ToArray<IUnit> ();
	}

	public IUnit[] Deck { get; private set; }

    public bool CanSpawn(IUnit prefab)
    {
        var inDeck = false;
        foreach (var card in Deck)
        {
            if (card.Id == prefab.Id)
            {
                inDeck = true;
                break;
            }
        }
        
        if (playerOwner.Energy >= prefab.Price && inDeck)
        {
            return true;
        }

        return false;
    }

    public void TrySpawn(IUnit prefab)
    {
        if (CanSpawn(prefab))
        {
			if (playerOwner.TryUseResources (prefab.Price)) {
				SpawnUnit(prefab);
			}

            Debug.Log("Spawned " + prefab.Id);
        }
        else
        {
            Debug.Log("Couldn't spawn " + prefab.Id);
        }
    }


    void SpawnUnit(IUnit prefab)
    {
        IUnit p = Instantiate((MonoBehaviour) prefab, SpawnPoint.position, new Quaternion()).GetComponent<IUnit>();
        p.Init(playerOwner);
        p.SetMovementTarget(TargetPoint, false);
    }

    public void SetRallyPoint(Vector3 rallyPoint)
    {
        TargetPoint.transform.position = rallyPoint;
    }

    public Vector3 GetRallyPoint {
        get {
            return TargetPoint.transform.position;
        }
    }
}
