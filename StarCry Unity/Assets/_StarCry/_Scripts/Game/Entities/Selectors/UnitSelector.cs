﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSelector : MonoBehaviour, ISelectableEntity
{
    public GameObject selectionVisuals;
    Player _playerOwner;
    public Unit unit { get; private set; }
    UnitSelectionManager _selectionManager;

    protected virtual void Awake()
    {
        _selectionManager = UnitSelectionManager.GetOrCreateInstance;
        unit = GetComponent<Unit>();
        selectionVisuals.SetActive(false);
    }

    public void OnDeselected()
    {
        selectionVisuals.SetActive(false);
    }

    public void OnSelected()
    {
        selectionVisuals.SetActive(true);
    }

    public void SetRallyPoint(Vector3 rallyPoint)
    {
        unit.SetMovementTarget(rallyPoint);
    }

    void ISelectableEntity.SetPlayerOwner(Player playerOwner)
    {
        _playerOwner = playerOwner;
        _selectionManager.RegisterUnit(this);
    }

    private void OnDisable()
    {
        if (_selectionManager)
            _selectionManager.UnRegisterUnit(this);
    }

    protected bool CanBeSelected
    {
        get
        {
            return this._playerOwner && _playerOwner.IsHuman;
        }
    }

    public bool CanSpawnUnits
    {
        get
        {
            return false;
        }
    }

    public bool CanConstructBuildings { get { return false; } }

    public bool HasRallyPoint { get { return false; } }

    public Vector3 RallyPoint { get { return this.unit.MovementTarget; } }
}
