﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StructureSelectionHandler : MonoBehaviour, ISelectableEntity {
    public GameObject selectionVisuals;
    Player _playerOwner;

    protected virtual void Awake()
    {
        selectionVisuals.SetActive(false);
    }

	public abstract bool CanSpawnUnits
    {
        get;
    }

    public abstract bool CanConstructBuildings
    {
        get;
    }

    public abstract bool HasRallyPoint     {
        get;
    }

    public void OnDeselected()
    {
        selectionVisuals.SetActive(false);
    }

    public void OnSelected()
    {
        selectionVisuals.SetActive(true);
    }

    public void SetPlayerOwner(Player playerOwner)
    {
        _playerOwner = playerOwner;
    }

    public abstract void SetRallyPoint(Vector3 rallyPoint);

    public abstract Vector3 RallyPoint { get; }

    private void OnMouseUp()
    {
        if (CanBeSelected) {
            InputController.GetInstance.Select(this);
            UnitSelectionManager.GetInstance.ClearSelection();
        } else {
            InputController.GetInstance.SetRallyPoint(this.transform.position);
        }
    }

    protected bool CanBeSelected {
        get { 
            return this._playerOwner && _playerOwner.IsHuman;
        }
    }
}
