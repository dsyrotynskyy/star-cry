﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NexusSelector : StructureSelectionHandler {

    public override bool CanConstructBuildings
    {
        get
        {
            return true;
        }
    }

	public override bool CanSpawnUnits
    {
        get
        {
			return false;
        }
    }

    public override bool HasRallyPoint { get { return false; } }

    public override Vector3 RallyPoint
    {
        get
        {
            throw new NotSupportedException();
        }
    }

    public override void SetRallyPoint(Vector3 rallyPoint)
    {
        throw new NotSupportedException();
    }
}
