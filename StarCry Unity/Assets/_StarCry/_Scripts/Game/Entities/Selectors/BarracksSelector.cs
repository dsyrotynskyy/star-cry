﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarracksSelector : StructureSelectionHandler
{
    public Barracks _barracks;

    protected override void Awake()
    {
        base.Awake();

        _barracks = GetComponent<Barracks>();
    }

    public override bool CanConstructBuildings
    {
        get
        {
            return false;
        }
    }

	public override bool CanSpawnUnits
	{
		get
		{
			return true;
		}
	}

	public override bool HasRallyPoint { get { return true; }}

    public override void SetRallyPoint(Vector3 rallyPoint)
    {
        try
        { 
            _barracks.SetRallyPoint(rallyPoint);
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }
    }

    public override Vector3 RallyPoint { 
        get
        {
            return _barracks.GetRallyPoint;
        }
    }
}
