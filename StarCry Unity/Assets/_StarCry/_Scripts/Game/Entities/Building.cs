﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Building : DestructableEntity {

    public int ConstructionCost = 50;
    public string Title = "";

}
