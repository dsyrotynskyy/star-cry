﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Entity))]
public class DestructableEntity : MonoBehaviour {
    public float maxHP = 100f;
    public float curHP;
    public Player playerOwner;

    protected virtual void Start()
    {
        curHP = maxHP;
        var body = GetComponentInChildren<EntityBody> ();
        if (body) {
            body.Init(playerOwner);
        }

        var selectHander = GetComponent<ISelectableEntity>();
        if (selectHander != null)
        {
            selectHander.SetPlayerOwner(playerOwner);
        }
    }

    public GameObject damageParticles;
    public GameObject destroyParticles;

    public void TakeDamage(Transform attacker, float takeDamage)
    {
        Vector3 damagePosition = transform.position;
        if (attacker) {
            damagePosition = attacker.transform.position;
        }

        curHP -= takeDamage;
        if (damageParticles) {
            Instantiate(damageParticles, damagePosition, new Quaternion());
        }

        onDamageTakenEvent.Invoke();
        if (curHP <= 0)
        {
            if (destroyParticles) {
                Instantiate(destroyParticles, transform.position, new Quaternion());
            }
            Destroy(gameObject);
            onDestroyedEvent.Invoke();
        }
    }

	[HideInInspector]
    public UnityEvent onDamageTakenEvent = new UnityEvent();
	[HideInInspector]
    public UnityEvent onDestroyedEvent = new UnityEvent();

}
