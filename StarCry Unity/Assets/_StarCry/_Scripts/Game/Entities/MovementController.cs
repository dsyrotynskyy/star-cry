﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {
    public float rotationSpeed = 3f;
    UnityEngine.AI.NavMeshAgent _agent;
    Transform target;

    public bool RotateToTarget
    {
        get
        {
            return !_agent.updateRotation;
        }
        set {
            _agent.updateRotation = !value;
        }
    }

    void Awake () {
        _agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
	}

    public void FollowTarget(Transform t, bool rotateToTarget)
    {
        if (t == this.transform) {
            Debug.LogError("Bad argument!");
            return;
        }

        RotateToTarget = rotateToTarget;
        
        target = t;
        FollowTarget();
    }

    public void StopMovement () {
        _agent.destination = transform.position;
//        _agent.updateRotation = false;
    }

    void FollowTarget()
    {
        _agent.destination = target.position;
    }

	private void Update()
	{
        if (RotateToTarget && target) {
            Vector3 d = this.target.transform.position - transform.position;
            var q = Quaternion.LookRotation(d);
            transform.rotation = Quaternion.Lerp (transform.rotation, q, Time.deltaTime * rotationSpeed);
        }
	}
}
