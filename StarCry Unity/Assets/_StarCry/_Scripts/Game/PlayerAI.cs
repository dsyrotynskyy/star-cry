﻿using UnityEngine;
using System.Collections;

public class PlayerAI : MonoBehaviour {

	public float updateAIPeriod = 0.1f;
	public Nexus myNexus;
	public Transform[] targetPoints;

	void Start () {

		UpdateAI ();
		InvokeRepeating ("UpdateAI", 0f, updateAIPeriod);
	}

	void UpdateAI () {
		if (!myNexus) {
			return;
		}
		var t = targetPoints [Random.Range (0, targetPoints.Length)];
//		myNexus.targetPoint.position = t.position;
	}
}
