﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ConstructionMarker : UnitySingleton<ConstructionMarker> {
    public float constructionYTolerance = 0.5f;
    public float constructionToleranceToOtherStructures = 10f;
    public float targetY = 1f;

    public GameObject Visuals;
    public GameObject VisualsDisabled;

    Player humanPlayer;
    EntityManager _entityManager;
    Camera _main;

    void Start()
    {
        humanPlayer = Player.Human;
        _entityManager = EntityManager.GetOrCreateInstance;
        _main = Camera.main;
        DisableBuildingMarker();
    }
	
    void Update()
    {
        if (!HasBuilding)
        {
            return;
        }
    }

    private void FixedUpdate()
    {
        if (!HasBuilding) {
            return;
        }

        Ray ray = _main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        //            int layer_mask = LayerMask.GetMask("Ground", "Enemy");
        int layer_mask = LayerMask.GetMask("Ground", "Building");

        if (Physics.Raycast(ray, out hit, 100f, layer_mask))
        {
            if (hit.transform.CompareTag("Ground"))
            {
                transform.position = hit.point;
            }
        }

        bool canConstruct = CanConstructHere;

        Visuals.SetActive(canConstruct);
        VisualsDisabled.SetActive(!canConstruct);
    }

    bool CanConstructHere {
        get {
            Vector3 constructionPosition = transform.position;

            float y = constructionPosition.y - targetY;
            float absY = Mathf.Abs(y);
            if (absY > constructionYTolerance)
            {
                return false;
            }
            else {

                float d = GetClosestStructureDistanceSqr(_entityManager.AllStructures);

                return d > constructionToleranceToOtherStructures;
            }
        }
    }

    float GetClosestStructureDistanceSqr(List<Entity> structures)
    {
        Transform tMin = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPos = transform.position;
        foreach (var t in structures)
        {
            float dist = Vector3.SqrMagnitude(t.transform.position - currentPos);
            if (dist < minDist)
            {
                tMin = t.transform;
                minDist = dist;
            }
        }
        return minDist;
    }

    public void TryConstruction () {
        if (!Enabled) {
            return;
        }

        if (!CanConstructHere) {
            return;
        }

        Vector3 constructionPosition = transform.position;

        if (humanPlayer.TryUseResources(targetBuilding.ConstructionCost)) {
            var b = (Building)Instantiate(targetBuilding, constructionPosition, transform.rotation);
            b.playerOwner = humanPlayer;

            DisableBuildingMarker();
        }
    }

    public void DisableBuildingMarker () {
        Enabled = false;

        this.targetBuilding = null;
        CancelInvoke("StartConstruction");
        this.gameObject.SetActive(false);

        onStateChangedEvent.Invoke();
    }

    internal UnityEvent onStateChangedEvent = new UnityEvent();

    bool Enabled { get; set; }

    internal void EnableBuildingMarker(Building target)
    {
        Enabled = true;

        this.targetBuilding = target;
        this.gameObject.SetActive(true);

        onStateChangedEvent.Invoke();
    }

    public Building targetBuilding;

    public bool HasBuilding {
        get {
            return targetBuilding;
        }
    }
}
