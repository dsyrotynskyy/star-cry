﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraManager : UnitySingleton<MainCameraManager>, ITouchDragHandler
{
    public float xMax = 25f;
    public float zMax = 25f;

    void Drag(Vector3 dragOffset)
    {
        Vector3 newPos = this.transform.position + dragOffset;

        newPos.x = Mathf.Clamp(newPos.x, -xMax, xMax);
        newPos.z = Mathf.Clamp(newPos.z, -zMax, zMax);

        this.transform.position = newPos;
    }

    void ITouchDragHandler.DragLastFrame(Vector3 offset)
    {
        Drag(offset);
    }

    void ITouchDragHandler.Drag(Vector3 start, Vector3 end)
    {
    }

    void ITouchDragHandler.OnDragEnded()
    {
    }

    void ITouchDragHandler.OnDragStarted(Vector3 onDragStarted)
    {
    }
}
