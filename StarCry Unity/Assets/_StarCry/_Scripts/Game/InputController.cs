﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputController : UnitySingleton<InputController> {

    public ISelectableEntity CurrentEntity {
        set {
            if (_currentEntity == value) {
                return;
            }
            if (_currentEntity != null) {
                _currentEntity.OnDeselected();
            }
            _currentEntity = value;
            _currentEntity.OnSelected();

            onSelectableChangedEvent.Invoke(_currentEntity);
        }
        get {
            return _currentEntity;
        }
    }
    ISelectableEntity _currentEntity;

    public void Select (ISelectableEntity selectable) {
        CurrentEntity = selectable;
    }

    public TemplateEvent<ISelectableEntity> onSelectableChangedEvent = new TemplateEvent<ISelectableEntity>();

    public void SetRallyPoint(Vector3 rallyPoint) {
        if (CurrentEntity != null && CurrentEntity.HasRallyPoint) {
            CurrentEntity.SetRallyPoint(rallyPoint);

            onRallyPointChangedPosition.Invoke(rallyPoint);
        }
    }

    public TemplateEvent<Vector3> onRallyPointChangedPosition = new TemplateEvent<Vector3>();

    internal void HandleClick(Vector3 targetPosition)
    {
        if (CurrentEntity != null) {
            if (CurrentEntity.HasRallyPoint) {
                this.SetRallyPoint(targetPosition);
            } else
            if (CurrentEntity.CanConstructBuildings) {
                ConstructionMarker.GetInstance.TryConstruction();
            }
        }
    }
}

public interface ISelectableEntity
{
	bool CanSpawnUnits { get; }
    bool CanConstructBuildings { get; }
    bool HasRallyPoint { get; }

    void OnDeselected();
    void OnSelected();

    void SetRallyPoint(Vector3 rallyPoint);
    void SetPlayerOwner(Player playerOwner);

    Vector3 RallyPoint { get; }
}
