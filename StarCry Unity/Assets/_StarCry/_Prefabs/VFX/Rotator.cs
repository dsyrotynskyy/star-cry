﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{

    public float speed = 10f;
    public Vector3 rotation;

    void Update()
    {
        transform.eulerAngles += rotation * speed * Time.deltaTime;
    }
}
