﻿using UnityEngine;
using System.Collections;
//add namespace
using TileWorld;

public class RuntimeDemo : MonoBehaviour {
	
	public TileWorldCreator creator;
	public TileWorldObjectScatterer scatterer;
	
	string x = "20";
	string y = "20";
	string clusterSize = "5";
  
	bool merge = false;
	bool invert = false;
	bool setStartEndPos = false;
	bool scatter = false;
	
	string _status = "";
	
	float divider = 1;
	float mainProgress = 0;
	
	string[] options = new string[] { "", "", "" , "", "", "", "", "", ""};
	int selectedStartPosition = 0; //defaukt TopLeft
	int selectedEndPosition = 8; //default BottomRight
	
	void OnGUI()
	{
		
		merge = GUILayout.Toggle(merge, "merge", "Button", GUILayout.Height(50));
		invert = GUILayout.Toggle(invert, "invert", "Button", GUILayout.Height(50));
		scatter = GUILayout.Toggle(scatter, "scatter objects", "Button", GUILayout.Height(50));
		
		if (scatter)
		{
			setStartEndPos = GUILayout.Toggle(setStartEndPos, "set start/end position", "Button", GUILayout.Height(50));
		}
		
		if (setStartEndPos)
		{
			GUILayout.Label("Set start position:");
			selectedStartPosition = GUILayout.SelectionGrid(selectedStartPosition, options,3, GUILayout.Height(50));
			
			GUILayout.Label("Set end position:");
			selectedEndPosition = GUILayout.SelectionGrid(selectedEndPosition, options,3, GUILayout.Height(50));
		}
		
		GUILayout.Label("x");
		x = GUILayout.TextField(x);
		GUILayout.Label("y");
		y = GUILayout.TextField(y);

		GUILayout.Label ("cluster size:");
		clusterSize = GUILayout.TextField(clusterSize);


        if (GUILayout.Button("Generate", GUILayout.Height(100),GUILayout.Width(100)))
		{

            if (mainProgress < 100 && mainProgress > 0)
                return;
			
			//set new settings
			/*
				1. Invert
				2. Floodunjoined
                3. Floodholes
				4. Create floor      
				5. Layer count
				6. Layer inset
				7. Chance to start alive
				8. Number of steps
				9. Width
				10. Height
				11. Random seed (-1 use default random seed)
				12. Minimum size of generated map (max 50% of width*height size of a map, if generation takes to long or lags, try to reduce its value)
				13. Cluster size
				14. Build overlapping tiles
			
			*/

            int _minSize = (int.Parse(x) * int.Parse(y)) / 3;
			
			creator.settings = new TileWorldCreator.Settings(
				invert,
				true,
                false,
				true,
				1,
				1,
				0.45f,
				5,
				int.Parse(x),
				int.Parse(y),
				-1,
				_minSize,
				int.Parse(clusterSize),
				true
			);

			/* This is how you can assign a different preset to a layer
			 * if you are building multiple layers at runtime
			 * here we assign to the second layer the second preset

			creator.layerPresetIndex[1] = 1;

			 */

			_status = "generating";
			
			var _containerObject = GameObject.Find("TWC_Objects");
			var _containerObject2 = GameObject.Find("TWC_StartEnd");
			
			if (_containerObject != null)
			{
				Destroy (_containerObject);
			}
			
			if (_containerObject2 != null)
			{
				Destroy(_containerObject2);
			}
			
			creator.Generate(merge);
		
			//reset spawner progress to zero
			scatterer.progress = 0;
			
			
			if (scatter)
			{				
				Scatter();
				//set progress divider to 2
				divider = 2;	
			}
			else
			{
				//set progress divider to 1
				divider = 1;
			}
		}		
		
		GUI.Box(new Rect(Screen.width/ 2,20, 200, 50), _status);
		
		//setup progressbar
		float _p1 = creator.progress / divider;
		float _p2 = scatterer.progress / divider;

        mainProgress = _p1 + _p2;
		
		if (mainProgress == 100)
		{
			_status = "finish";
		}
		
		GUI.Box(new Rect(Screen.width/ 2 + 10, 40, Mathf.Abs(((mainProgress)*2) - 20)  , 20), mainProgress.ToString() + "%");
		
		GUI.Label(new Rect((Screen.width / 2) - 150, Screen.height - 25, 300, 25), "Middle Mouse = Pan, Mousewheel = Zoom"); 
	}

	
	void Scatter()
	{	
		//Instantiate random objects (merge, add collider)
		scatterer.PlaceRandomObjects(merge, false, false);
		//Instanitate start and end object
		//possible positions:
		/*
		topLeft
		topCenter
		topRight
		middleLeft
		middleCenter
		middleRight
		bottomLeft
		bottomCenter
		bottomeRight
		*/
		if (setStartEndPos)
		{
			scatterer.PlaceStartEndObjects(GetSelectedPosition(selectedStartPosition), GetSelectedPosition(selectedEndPosition));
		}	
	}
	
	//return spawnPosition based on selected integer index from GUI
	TileWorldObjectScatterer.spawnPosition GetSelectedPosition(int _selectedPositionIndex)
	{
		TileWorldObjectScatterer.spawnPosition _pos = TileWorldObjectScatterer.spawnPosition.topLeft;
		
		switch(_selectedPositionIndex)
		{
			case 0:
				_pos =  TileWorldObjectScatterer.spawnPosition.topLeft;
				break;
			case 1:
				_pos =  TileWorldObjectScatterer.spawnPosition.topCenter;
				break;
			case 2:
				_pos = TileWorldObjectScatterer.spawnPosition.topRight;
				break;
			case 3:
				_pos = TileWorldObjectScatterer.spawnPosition.middleLeft;
				break;
			case 4:
				_pos = TileWorldObjectScatterer.spawnPosition.middleCenter;
				break;
			case 5:
				_pos = TileWorldObjectScatterer.spawnPosition.middleRight;
				break;
			case 6:
				_pos = TileWorldObjectScatterer.spawnPosition.bottomLeft;
				break;
			case 7:
				_pos = TileWorldObjectScatterer.spawnPosition.bottomCenter;
				break;
			case 8:
				_pos = TileWorldObjectScatterer.spawnPosition.bottomRight;
				break;
		}
		return _pos;
	}
}
