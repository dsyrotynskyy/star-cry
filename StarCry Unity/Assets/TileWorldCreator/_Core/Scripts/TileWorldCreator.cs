﻿/* TILE WORLD CREATOR
 * Copyright (c) 2015 doorfortyfour OG
 * 
 * Create awesome tile worlds in seconds.
 *
 * Using:
 * Drag this script on an empty gameobject and start creating.
 * OR
 * Drag the prefab called TileWorldCreator to an empty scene.
 *
 *
 * Documentation: http://tileworldcreator.doofortyfour.com
 * Like us on Facebook: http://www.facebook.com/doorfortyfour2013
 * Web: http://www.doorfortyfour.com
 * Contact: mail@doorfortyfour.com Contact us for help, bugs or 
 * share your awesome work you've made with TileWorldCreator
*/

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Serialization;
using System.Reflection;
using System.Linq;

//Disable Log warnings for assigned but not used variables 
#pragma warning disable 0219

namespace TileWorld
{
    [System.Serializable]
    public class TileWorldCreator : MonoBehaviour
    {
        //mask interface
        public IMaskBehaviour[] iMasks;

        public bool firstTimeBuild = true;
        //------------------------------------------------------------------------------------------------------------------	
        public string worldName = "";
        // Bool settings
        public bool invert = false;
        [FormerlySerializedAs("floodfill")]
        public bool floodunjoined = true;
        public bool floodholes = true;
        public bool createFloor = true;
        //optimize is always on, so this is deprecated
        //we're setting optimization in the method BuildMapComplete and BuildMapPartial
        //public bool optimize = true; 

        //layers
        public int layerCount = 1;
        public int layerInset = 1;
        public bool buildOverlappingTiles = true;
       

        //world generation properties
        public int deathLimit = 4;
        public int birthLimit = 4;
        public float chanceToStartAlive = 0.45f;
        public int numberOfSteps = 5;
        public int randomSeed = -1;
        public int minSize = 10;
        public int optimizeSteps = 4; //new default is 4

        //grid size      
        public int width = 20;
        public int height = 20;

        //grid position
        public Vector3 origin = new Vector3(0, 0, 0);

        //build settings
        public bool merge = false;
        public int clusterSize = 5;
        public bool addCollider = true;
        public bool createMultiMaterialMesh = true;

        public float progress = 0;

        //if false use new assigned settings during runtime
        //if true use settings from prefab
        public bool useSettingsFromReference = false;

        //Global Generation Properties Class
        //----------------------------
        #region settings
        [System.Serializable]
        public class Settings
        {
            // Bool settings
            public bool invert = false;
            [FormerlySerializedAs("floodfill")]
            public bool floodunjoined = true;
            public bool floodholes = true;
            public bool createFloor = true;
            //public bool optimize = true;

            //layers
            public int layerCount = 1;
            public int layerInset = 1;
            public bool buildOverlappingTiles;
           

            //world generation properties
            public int deathLimit = 0;
            public int birthLimit = 0;
            public float chanceToStartAlive = 0.45f;
            public int numberOfSteps = 0;
            public int randomSeed = -1;
            public int minSize = 10;
            

            //grid size
            public int width = 10;
            public int height = 10;

            //build settings
            public int clusterSize = 5;

            //constructor with all settings
            public Settings(
                bool _invert,
                bool _floodunjoined,
                bool _floodholes,
                bool _createFloor,
                int _layerCount,
                int _layerInset,
                float _chanceToStartAlive,
                int _numberOfSteps,
                int _width,
                int _height,
                int _randomSeed,
                int _minSize,
                int _clusterSize,
                bool _buildOverlappingTiles)
            {
                invert = _invert;
                floodunjoined = _floodunjoined;
                floodholes = _floodholes;
                createFloor = _createFloor;
                layerCount = _layerCount;
                layerInset = _layerInset;
                chanceToStartAlive = _chanceToStartAlive;
                numberOfSteps = _numberOfSteps;
                width = _width;
                height = _height;
                randomSeed = _randomSeed;
                minSize = _minSize;
                clusterSize = _clusterSize;
                buildOverlappingTiles = _buildOverlappingTiles;
            }

            //constructor with only width and height
            public Settings(int _width, int _height)
            {
                width = _width;
                height = _height;

                //default values
                invert = false;
                floodunjoined = true;
                floodholes = true;
                createFloor = true;
                layerCount = 1;
                layerInset = 1;
                deathLimit = 4;
                birthLimit = 4;
                chanceToStartAlive = 0.45f;
                numberOfSteps = 5;
                randomSeed = -1;
                minSize = 10;
                clusterSize = 5;
                buildOverlappingTiles = true;
            }

            //default constructor
            public Settings()
            {
                //default values
                invert = false;
                floodunjoined = true;
                floodholes = true;
                createFloor = true;
                layerCount = 1;
                layerInset = 1;
                deathLimit = 4;
                birthLimit = 4;
                chanceToStartAlive = 0.45f;
                numberOfSteps = 5;
                width = 20;
                height = 20;
                randomSeed = -1;
                minSize = 10;
                clusterSize = 5;
                buildOverlappingTiles = true;
            }
        }
        
        //----------------------------    
        
        public Settings settings = new Settings();

        #endregion

        //Editor Properties
        public bool autoBuild = true;
        public bool brushx2 = true;
        public bool mergeReady = false;
        bool[,] cloneMap;

        public float globalScale = 1.0f;
        public bool scaleTiles = true;

        //maps
        #region maps
        [System.Serializable]
        public class Maps
        {
            //store the type of a tile (I, C, Ci, B, F)
            //public TileWorldCache.TileType[,] type = new TileWorldCache.TileType[0, 0];

            //main map
            public bool[,] map = new bool[0, 0];
            //cannot use multidimensional arrays
            //unity cannot serialize them so we create an additional
            //single array map
            public bool[] mapSingle = new bool[0];
            
            //NEW in 1.0.4
            //store map before modifications. So we can check
            //which part should be rebuilded new
            public bool[,] oldMap = new bool[0, 0];
            //store all builded tiles and replace tiles if map has changed
            public Mesh[,] meshObjects = new Mesh[0, 0];
            public GameObject[,] gameObjectsMap = new GameObject[0, 0];
            public GameObject[] gameObjectsMapSingle = new GameObject[0]; 

            //mask properties
            public bool useMask = false;
            public bool paintMask = false;
            public int selectedMask = 0;
            public bool[,] maskMap = new bool[0, 0];
            public bool[] maskMapSingle = new bool[0];

            
            public Maps(){}

            public Maps(int _x, int _z, bool _invert, bool _useMask, int _selectedMask)
            {
                //type = new TileWorldCache.TileType[_x, _z];
                map = new bool[_x, _z];
                mapSingle = new bool[_x * _z];
                oldMap = new bool[_x, _z];
                gameObjectsMap = new GameObject[_x, _z];
                gameObjectsMapSingle = new GameObject[_x * _z];
                meshObjects = new Mesh[_x, _z];

                useMask = _useMask;
                selectedMask = _selectedMask;

                int _index = 0;
                
                
                maskMapSingle = new bool[_x * _z];
                maskMap = new bool[_x, _z];
                

                for (int i = 0; i < _z; i++)
                {
                    for (int j = 0; j < _x; j++)
                    {
                        map[j, i] = _invert;
                        oldMap[j, i] = _invert;
                        gameObjectsMap[j, i] = null;
                        meshObjects[j, i] = null;

                        mapSingle[_index] = _invert;
                        _index++;
                    }
                }
            }
        }

        public int mapIndex = 0;
        int lastMapIndex = 0;
        public List<Maps> worldMap = new List<Maps>();

        #endregion

        public bool[,] cellmap = new bool[0, 0];
        public bool[,] fillmap = new bool[0, 0];
        public bool[,] fillWaterMap = new bool[0, 0];

        //GameObject Presets
        [System.Serializable]
        public class TileSets
        {
            //pattern
            //
            //	I = block with one edge
            //	C = corner block
            //	Ci = corner inverted block
            //	B = full block
            //	F = floor block

            public Vector3 blockOffset = new Vector3(0.5f, 0.5f, 0.5f);
            public Vector3 floorOffset = new Vector3(0.5f, 0f, 0.5f);

            //Blocks Prefabs
            public GameObject tileI;
            public GameObject tileC;
            public GameObject tileCi;
            public GameObject tileB;
            public GameObject tileF;

           

            public float[] yRotationOffset = new float[5] { 0f, 0f, 0f, 0f, 0f };
        }

        [System.Serializable]
        public class Presets
        {
            public List<TileSets> tiles = new List<TileSets>();

            public bool showPresetEditor = false;

            public Presets()
            {
                tiles.Add(new TileSets());
            }
        }
        public List<Presets> presets = new List<Presets>();

    
        public int blockPresetIndex = 0;


        //used for the editor
        //layerPresetIndex stores the assigned preset for each layer
        //blockPresets stores the available presets as string array
        [FormerlySerializedAs("blockLayerPresetIndex")]
        public int[] layerPresetIndex = new int[1];
        //[FormerlySerializedAs("blockLayerPresets")]
        public string[] blockLayerPresets = new string[0];


        public GameObject worldContainer;

        public List<GameObject> containers = new List<GameObject>();

        //Caverns
        [System.Serializable]
        public class Cavern
        {
            public List<int> x = new List<int>();
            public List<int> y = new List<int>();
            //store coordinates
            public Cavern(int _x, int _y)
            {
                x.Add(_x);
                y.Add(_y);
            }
        }

        public List<Cavern> caverns = new List<Cavern>();
        public List<Cavern> waterCaverns = new List<Cavern>();

        public int bigCavernIndex = 0;

        //orientation of blocks
        public static string orientation = "";


        //GUI / Editor Properties 
        public Vector3 mouseWorldPosition = new Vector3(0, 0, 0);
        public bool mouseOverGrid = false;

        public Color cellColor = new Color(120f / 255f, 255f / 255f, 120f / 255f, 150f / 255f);
        public Color floorCellColor = new Color(0f / 255f, 180f / 255f, 255f / 255f, 150f / 255f);
        public Color brushColor = new Color(226f / 255f, 41f / 255f, 32f / 255f, 150f / 255f);
        public Color maskColor = new Color(255f / 255f, 255f / 255f, 255f / 255f, 150f / 255f);

        public bool showGridAlways = false;
        public bool showGrid = true;
        public bool showAllMaps = false;

        public bool showGeneralWorldSettings = false;
        public bool showBlockPresets = false;
        public bool showEdit = false;

        public int brushSize = 2;

        //-------------------------------------------------------------------------------------------------------------


        void Awake()
        {
            progress = 0;
        }

        void Start()
        {
            //we have to load our single array map back to the multidim array maps
            LoadBack();
        }


        void AssignSettings()
        {
            invert = settings.invert;
            floodunjoined = settings.floodunjoined;
            floodholes = settings.floodholes;
            createFloor = settings.createFloor;

            layerCount = settings.layerCount;
            layerInset = settings.layerInset;

            chanceToStartAlive = settings.chanceToStartAlive;
            numberOfSteps = settings.numberOfSteps;

            width = settings.width;
            height = settings.height;

            randomSeed = settings.randomSeed;
            minSize = settings.minSize;
            clusterSize = settings.clusterSize;
            buildOverlappingTiles = settings.buildOverlappingTiles;
        }


        //--------------------------
        //MAP GENERATION
        //--------------------------
        #region mapgeneration
        /// <summary>
        /// Generate and build a new map from scratch. 
        /// Set merge true if the map should be merged
        /// </summary>
        /// <param name="_merge"></param>
        public void Generate(bool _merge)
        {
            //first generate Maps
            GenerateMaps();

            //optimization is set to false because
            //we do already an optimization in the GenerateMaps method
            BuildMapComplete(true, _merge, false);
        }

        
        /// <summary>
        /// Generate a new map from scratch without building it.
        /// use BuildMapComplete(); to build the generated map
        /// </summary>
        public void GenerateMaps()
        {

          

            //if firstTimeBuild is true, TWC will build the whole map from ground up
            //after that we set it to false so only changes will be rebuilded (partial build)
            firstTimeBuild = true;     

            mergeReady = false;

            if (Application.isPlaying && !useSettingsFromReference)
            {
                AssignSettings();
            }


            ////set map index to zero
            lastMapIndex = mapIndex;
            mapIndex = 0;

            caverns = new List<Cavern>();

            
            //get current mask settings and store them
            List<bool> _useMask = new List<bool>();
            List<int> _selectedMask = new List<int>();
            for (int ma = 0; ma < worldMap.Count; ma++)
            {
                _useMask.Add(worldMap[ma].useMask); 
                _selectedMask.Add(worldMap[ma].selectedMask);
            }

            //create new worldmap and initialize it
            worldMap = new List<Maps>();

            if (_useMask.Count > 0)
            {
                worldMap.Add(new Maps(width, height, false, _useMask[0], _selectedMask[0]));
            }
            else
            {
                worldMap.Add(new Maps(width, height, false, false, 0));
            }

            worldMap[0].map = initialiseMap(worldMap[0].map);

            //create new world map according
            //to the layer count
            for (int m = 1; m < layerCount; m++)
            {
                if (m > _useMask.Count - 1)
                {
                    AddNewLayer(false, _useMask[0], _selectedMask[0], false, 0);
                }
                else
                {
                    AddNewLayer(false, _useMask[m], _selectedMask[m], false, 0);
                }
            }

            //run simulation step for number of steps
            //more steps results in smoother worlds
            for (int i = 0; i < numberOfSteps; i++)
            {
                worldMap[mapIndex].map = DoSimulationStep(worldMap[mapIndex].map);
            }


            //create new containers
            NewContainer();


            //floodunjoined makes sure only biggest cavern will survive
            //HACK: We have to disable the floodunjoined algorithm especially the CountCaverns() method 
            //when map size is greater 150x150, or else we are running into a stackoverflow.
            if (floodunjoined && (width <= 150 || height <= 150))
            {
                //count how many caverns our world has
                CountCaverns();

                FloodFill();
            }

            //flood holes floods all smaller holes inside of a map
            if (floodholes && (width <= 150 || height <= 150))
            {
                CountWater();

                FloodFillWater();
            }


            //build cells for each layer
            //use layer inset if we have more than one layer
            for (int ms = 0; ms < layerCount; ms++)
            {
                //check if block is an inner terrain block
                for (int y = 0; y < worldMap[0].map.GetLength(1); y++)
                {
                    for (int x = 0; x < worldMap[0].map.GetLength(0); x++)
                    {
                        if (layerInset > 0)
                        {
                            int _itc = TileWorldNeighbourCounter.CountInnerTerrainBlocks(worldMap[ms].map, x, y, layerInset, invert);
                          
                            if (_itc >= (((layerInset * 2) + 1) * ((layerInset * 2) + 1)) - 1)
                            {
                                if (!invert)
                                {
                                    if (ms + 1 < worldMap.Count)
                                    {
                                        worldMap[ms + 1].map[x, y] = false;
                                    }
                                }
                                else
                                {
                                    if (ms + 1 < worldMap.Count)
                                    {
                                        //do not create cells on borders when invert is set to true
                                        if (x < 1 || x >= width - 1 || y >= height - 1 || y < 1)
                                        {
                                            worldMap[ms + 1].map[x, y] = false;
                                        }
                                        else
                                        {
                                            worldMap[ms + 1].map[x, y] = true;
                                        }

                                    }
                                }
                            }
                        }
                        else
                        {
                            //if layer inset is set to 0 copy the layer cells from the lower layer to the higher layer
                            if (ms + 1 < worldMap.Count)
                            {
                                worldMap[ms + 1].map[x, y] = worldMap[ms].map[x, y];
                            }
                        }
                    }
                }

               

            }

            if (minSize > (width * height) / 2)
            {
                Debug.LogWarning("TileWorldCreator: Minimum size to high. Set to max: " + ((width * height) / 2));
                minSize = (width * height) / 2;
            }

            //check if biggest cavern is big enough.
            //if not, regenerate map
            //only do this when the random seed is set to -1 so we don't get
            //a stackoverflow if a random seed generates a map who does not meet the min size.
            if (randomSeed == -1)
            {
                if (caverns.Count > 0)
                {
                    if (caverns[0].y.Count < minSize)
                    {
                        GenerateMaps();
                    }
                }
            }
          
            // OPTIMIZATION IS NOW ALWAY ON
            //Do one optimize pass for each layer
            //set optimize steps to 4
            optimizeSteps = 4;
            

            for (int a = 0; a < worldMap.Count; a++)
            {
                for (int o = 0; o < optimizeSteps; o++)
                {
                        OptimizePass(a);
                }
            }
            //}

            //Update single dim array for serialization
            //this is called as soon as bool[,] map changes to update the single array map
            //for serialization
            UpdateMap();

            mapIndex = lastMapIndex;
        }
        //-END MAIN LOOP
   

        //BUILD CURRENT MAP
        //-------------------
        /// <summary>
        /// Build the generated map.
        /// </summary>
        /// <param name="_coroutine"></param>
        /// <param name="_merge"></param>
        /// <param name="_optimize"></param>
        public void BuildMapComplete(bool _coroutine, bool _merge, bool _optimize)
        {
           
            //first step optimize map before building it
            if (_optimize)
            {

                for (int o = 0; o < optimizeSteps; o++)
                {
                    OptimizePass(mapIndex);
                }

            }

            if (_coroutine)
            {
                StartCoroutine(BuildMapIE(_merge, true));
            }
            else
            {
                BuildMap(true);
            }


        }


        //build map partial
        /// <summary>
        /// Only rebuild a part of the map from the x and y position
        /// </summary>
        /// <param name="_coroutine"></param>
        /// <param name="_optimize"></param>
        /// <param name="_xPos"></param>
        /// <param name="_yPos"></param>
        public void BuildMapPartial(bool _coroutine, bool _optimize, int _xPos, int _yPos)
        {
            //first step optimize map before building it
            if (_optimize)
            {
                for (int i = 0; i < optimizeSteps; i++)
                {
                    //optimize partial needs a x and y pos to know where on the map 
                    //it should optimize
                    OptimizePassPartial(mapIndex, _xPos, _yPos);
                }

            }

            PartialMapChecker(_coroutine);


        }

        //BuildMap Coroutine Method
        IEnumerator BuildMapIE(bool _merge, bool _allLayers)
        {
            merge = _merge;
            int _currentMapIndex = mapIndex;
            //mapIndex = 0;

            progress = 0;

            if (_allLayers)
            {
                //build world for each layer
                for (int i = 0; i < layerCount; i++)
                {
                    yield return StartCoroutine(BuildWorldIE(worldMap[i].map, i, i, true));

                    //assign the oldmap
                    worldMap[i].oldMap = new bool[width, height];
                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            worldMap[i].oldMap[x, y] = worldMap[i].map[x, y];
                        }
                    }
                } 
                

                if (_merge)
                {
                    yield return StartCoroutine(MergeWorldIE());
                }
            }
                //only build current selected layer
            else
            {
                yield return StartCoroutine(BuildWorldIE(worldMap[mapIndex].map, mapIndex, mapIndex, true));
                
                //assign the oldmap
                worldMap[mapIndex].oldMap = new bool[width, height];
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        worldMap[mapIndex].oldMap[x, y] = worldMap[mapIndex].map[x, y];
                    }
                } 
                
                if (_merge)
                {
                    yield return StartCoroutine(MergeWorldIE());
                }
            }

            mapIndex = _currentMapIndex;

            //assign all clusters to worldContainer
            for (int c = 0; c < containers.Count; c++)
            {
                containers[c].transform.parent = worldContainer.transform;
            }
        }


        //default BuildMap method
        void BuildMap(bool _allLayers)//public void BuildMapEditor()
        {
          
            mergeReady = true;

            int _currentMapIndex = mapIndex;
            //mapIndex = 0;

            if (firstTimeBuild)
            {
                NewContainer();
            }

            if (_allLayers)
            {
                //build world for each layer
                for (int i = 0; i < layerCount; i++)
                {

                    BuildWorld(worldMap[i].map, i, i, true);

                    //assign the oldmap
                    worldMap[i].oldMap = new bool[width, height];
                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            worldMap[i].oldMap[x, y] = worldMap[i].map[x, y];
                        }
                    }

                }
            }
            //only build current selected layer
            else
            {
                BuildWorld(worldMap[mapIndex].map, mapIndex, mapIndex, true);

                //assign the oldmap
                worldMap[mapIndex].oldMap = new bool[width, height];
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        worldMap[mapIndex].oldMap[x, y] = worldMap[mapIndex].map[x, y];
                    }
                }
            }
            
            //assign all clusters to worldContainer
            for (int c = 0; c < containers.Count; c++)
            {
                containers[c].transform.parent = worldContainer.transform;
            }

            mapIndex = _currentMapIndex;

            firstTimeBuild = false;
        }
        //-------------------------


        //Initialize Map
        //--------------
        bool[,] initialiseMap(bool[,] map)
        {
            if (randomSeed > -1)
            {
                Random.seed = randomSeed;
            }
            else
            {
                Random.seed = System.Environment.TickCount;
            }

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    float _rnd = Random.Range(0.0f, 1.0f);
                    if (_rnd < chanceToStartAlive)
                    {
                        map[x, y] = true;
                    }
                    else
                    {
                        //in isle mode we have to make sure that the border is always a floor tile.
                        if (!invert)
                        {
                            if (((x < width) && (y == 0 || y == height - 1)) || ((x == 0 || x == width - 1) && (y < height)))
                            {
                                map[x, y] = true;
                            }
                        }
                    }
                }
            }
            //reset seed
            //Random.seed = (int)System.DateTime.Now.Ticks;

            return map;
        }
        //-End initialize map


        //Update map single array for serialization
        //-----------------------------------------
        public void UpdateMap()
        {
            if (worldMap.Count < 1)
                return;

            for (int l = 0; l < layerCount; l++)
            {
                int _index = 0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        if (_index < worldMap[l].mapSingle.Length)
                        {
                            if (x < worldMap[l].map.GetLength(0) && y < worldMap[l].map.GetLength(1))
                            {
                                worldMap[l].mapSingle[_index] = worldMap[l].map[x, y];
                                
                                if (worldMap[l].maskMap.GetLength(0) > 0)
                                {
                                    worldMap[l].maskMapSingle[_index] = worldMap[l].maskMap[x, y];
                                }

                                worldMap[l].gameObjectsMapSingle[_index] = worldMap[l].gameObjectsMap[x, y];

                                _index++;
                            }
                        }
                    }
                }
            }
        }

        
        //Load back multidimensional array from single dim array
        void LoadBack()
        {
            
            for (int l = 0; l < layerCount; l++)
            {
                worldMap[l].map = new bool[width, height];
                worldMap[l].gameObjectsMap = new GameObject[width, height];
                worldMap[l].maskMap = new bool[width, height];
                //worldMap[l].type = new TileWorldCache.TileType[width, height];

                int _index = 0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        worldMap[l].map[x, y] = worldMap[l].mapSingle[_index];
                        if (worldMap[l].gameObjectsMap[x, y] != null)
                        {
                            worldMap[l].gameObjectsMap[x, y] = worldMap[l].gameObjectsMapSingle[_index];
                        }
                        _index++;
                    }
                }
            }
        }

        //Do Simulation steps
        //-------------------
        bool[,] DoSimulationStep(bool[,] oldMap)
        {
            bool[,] newMap = new bool[width, height];

            //Loop over each row and column of the map
            for (int x = 0; x < oldMap.GetLength(0); x++)
            {
                for (int y = 0; y < oldMap.GetLength(1); y++)
                {
                    int nbs = TileWorldNeighbourCounter.CountAllNeighbours(oldMap, x, y, 1, true);
                    //The new value is based on our simulation rules
                    //First, if a cell is alive but has too few neighbours, kill it.
                    if (oldMap[x, y])
                    {
                        if (nbs < deathLimit)
                        {
                            newMap[x, y] = false;
                        }
                        else
                        {
                            newMap[x, y] = true;
                        }
                        //make sure the cavern is always closed on the border edges
                        if (invert)
                        {
                            if (x < 1 || x >= width - 1 || y >= height - 1 || y < 1)
                            {
                                newMap[x, y] = true;
                            }
                        }


                    }
                    //Otherwise, if the cell is dead now, check if it has the right number of neighbours to be 'born'
                    else
                    {
                        if (nbs > birthLimit)
                        {
                            newMap[x, y] = true;
                        }
                        else
                        {
                            newMap[x, y] = false;
                        }
                        //make sure the cavern is always closed on the border edges
                        if (invert)
                        {
                            if (x < 1 || x >= width - 1 || y >= height - 1 || y < 1)
                            {
                                newMap[x, y] = true;
                            }
                        }
                    }
                }
            }

            return newMap;

        }
        //-END Do Simulation steps


        //COUNT CAVERNS
        //-------------
        //for counting the caverns we have to fill each cavern
        //first we generate a new fillmap with the same content of the worldMap
        //for each x and y length of the worldMap we call the 
        //FillCavern method which fills each cell.
        void CountCaverns()
        {
            fillmap = new bool[worldMap[mapIndex].map.GetLength(0), worldMap[mapIndex].map.GetLength(1)];
            caverns = new List<Cavern>();
            for (int cmy = 0; cmy < worldMap[mapIndex].map.GetLength(1); cmy++)
            {
                for (int cmx = 0; cmx < worldMap[mapIndex].map.GetLength(0); cmx++)
                {
                    fillmap[cmx, cmy] = worldMap[mapIndex].map[cmx, cmy];
                }
            }



            for (int y = 0; y < worldMap[mapIndex].map.GetLength(1); y++)
            {
                for (int x = 0; x < worldMap[mapIndex].map.GetLength(0); x++)
                {
                    if (fillmap[x, y] == false)
                    {
                        //ad new cavern
                        caverns.Add(new Cavern(x, y));
                        //fill cell as long as neighbour
                        //is not filled
                        FillCavern(x, y);

                    }
                }
            }
        }
        //-END Count Caverns


        //FillCavern
        //----------
        //this is where flood fill actually happens
        void FillCavern(int _x, int _y)
        {
            if (fillmap[_x, _y] != false)
            {
                return;
            }

            fillmap[_x, _y] = true;
            caverns[caverns.Count - 1].x.Add(_x);
            caverns[caverns.Count - 1].y.Add(_y);

            for (int y = -1; y < 2; y++)
            {
                for (int x = -1; x < 2; x++)
                {

                    int neighbour_x = _x + x;
                    int neighbour_y = _y + y;

                    if (y == 0 && x == 0)
                    {
                        fillmap[_x, _y] = true;
                    }
                    //off the edge of the map
                    else if (neighbour_x < 0 || neighbour_y < 0 || neighbour_x >= fillmap.GetLength(0) || neighbour_y >= fillmap.GetLength(1))
                    {

                    }
                    //normal check of the neighbour
                    else if (!fillmap[neighbour_x, neighbour_y])
                    {

                        FillCavern(neighbour_x, neighbour_y);

                    }

                }
            }
        }
        //-END Fill Cavern

        //FloodFill
        //---------
        //get biggest cavern from caverns list
        //flood all caverns in cellmap as long as it is not
        //the biggest cavern
        void FloodFill()
        {
            //get biggest cave
            int _bigCavernInx = 0;
            int _count = 0;
            int _oldCount = 0;
            for (int c = 0; c < caverns.Count; c++)
            {
                _count = caverns[c].x.Count;

                if (_count > _oldCount)
                {
                    _bigCavernInx = c;
                    _oldCount = _count;
                }
                else
                {
                    _count = _oldCount;
                }
            }

            for (int s = 0; s < caverns.Count; s++)
            {
                if (s != _bigCavernInx)
                {
                    for (int m = 0; m < caverns[s].x.Count; m++)
                    {
                        worldMap[mapIndex].map[caverns[s].x[m], caverns[s].y[m]] = true;
                    }
                }
            }

           
            
            bigCavernIndex = _bigCavernInx;
            //yield return null; 
        }
        //-END Flood Fill


        //flood water holes
        //-----------------
        void CountWater()
        {
            fillWaterMap = new bool[worldMap[mapIndex].map.GetLength(0), worldMap[mapIndex].map.GetLength(1)];
            waterCaverns = new List<Cavern>();
            for (int cmy = 0; cmy < worldMap[mapIndex].map.GetLength(1); cmy++)
            {
                for (int cmx = 0; cmx < worldMap[mapIndex].map.GetLength(0); cmx++)
                {
                    fillWaterMap[cmx, cmy] = worldMap[mapIndex].map[cmx, cmy];
                }
            }



            for (int y = 0; y < worldMap[mapIndex].map.GetLength(1); y++)
            {
                for (int x = 0; x < worldMap[mapIndex].map.GetLength(0); x++)
                {
                    if (fillWaterMap[x, y] == true)
                    {
                        //ad new cavern
                        waterCaverns.Add(new Cavern(x, y));
                        //fill cell as long as neighbour
                        //is not filled
                        FillWater(x, y);

                    }
                }
            }

        }

        //FillWater
        //----------
        //this is where flood fill actually happens
        void FillWater(int _x, int _y)
        {
            if (fillWaterMap[_x, _y] != true)
            {
                return;
            }

            fillWaterMap[_x, _y] = false;
            waterCaverns[waterCaverns.Count - 1].x.Add(_x);
            waterCaverns[waterCaverns.Count - 1].y.Add(_y);

            for (int y = -1; y < 2; y++)
            {
                for (int x = -1; x < 2; x++)
                {

                    int neighbour_x = _x + x;
                    int neighbour_y = _y + y;

                    if (y == 0 && x == 0)
                    {
                        fillWaterMap[_x, _y] = false;
                    }
                    //off the edge of the map
                    else if (neighbour_x < 0 || neighbour_y < 0 || neighbour_x >= fillWaterMap.GetLength(0) || neighbour_y >= fillWaterMap.GetLength(1))
                    {

                    }
                    //normal check of the neighbour
                    else if (fillWaterMap[neighbour_x, neighbour_y])
                    {

                        FillWater(neighbour_x, neighbour_y);

                    }

                }
            }
        }
        //-END FillWater

        
        void FloodFillWater()
        {
            //get biggest water cave
            int _bigCavernInx = 0;
            int _count = 0;
            int _oldCount = 0;
            for (int c = 0; c < waterCaverns.Count; c++)
            {
                _count = waterCaverns[c].x.Count;

                if (_count > _oldCount)
                {
                    _bigCavernInx = c;
                    _oldCount = _count;
                }
                else
                {
                    _count = _oldCount;
                }
            }

            for (int s = 0; s < waterCaverns.Count; s++)
            {
                if (s != _bigCavernInx)
                {
                    for (int m = 0; m < waterCaverns[s].x.Count; m++)
                    {
                        worldMap[mapIndex].map[waterCaverns[s].x[m], waterCaverns[s].y[m]] = false;
                    }
                }
            }
        }

        #endregion

        //---------------------------------
        //BUILD WORLD // MAP INSTANTIATION
        //---------------------------------
        #region mapinstantiation
        //build block gameobjects 
        //this is the runtime IEnumerator method
        IEnumerator BuildWorldIE(bool[,] _map, int _layerIndex, int _layerCount, bool _rebuild)
        {
            if (presets.Count < 1)
                yield break; ;
            if (worldMap.Count < 1)
                yield break; ;

            //Build world in clusters
            //Divide width and height by clusterSize
            //Build each cluster and parent objects in a new gameobject
            float _clusterSize = (float)clusterSize;
            if (_clusterSize == 0)
            {
                _clusterSize = 1;
            }
            float xSizeF = Mathf.Floor(width / ((float)width / _clusterSize));
            float ySizeF = Mathf.Floor(height / ((float)height / _clusterSize));

            int xSize = (int)xSizeF;
            int ySize = (int)ySizeF;

            int xStep = 0;
            int yStep = 0;

            int index = 0;

            GameObject _container = null;

            float _progressStep = 100f / (Mathf.Ceil((float)width / _clusterSize) * Mathf.Ceil((float)height / _clusterSize) );/// layerCount
           
            for (int s = 0; s < Mathf.Ceil((float)width / _clusterSize) * Mathf.Ceil((float)height / _clusterSize); s++)
            {
                if (containers.Count != Mathf.Ceil((float)width / _clusterSize) * Mathf.Ceil((float)height / _clusterSize))
                {

                    _container = new GameObject();
                    _container.transform.localScale = new Vector3(1, 1, 1);
                    _container.name = "cluster_" + s.ToString();
                    containers.Add(_container);

                }
                for (int y = 0 + (ySize * yStep); y < ySize + (ySize * yStep); y++)
                {
                    for (int x = 0 + (xSize * xStep); x < xSize + (xSize * xStep); x++)
                    {
                        CallInstantiation(_map, y, x, s, _layerCount, _layerIndex, _rebuild);
                        ApplyMask(_map, x, y, _layerIndex);  
                    }
                }

                if (index >= (width / _clusterSize) - 1)
                {
                    yStep++;
                    xStep = 0;
                    index = 0;
                }
                else
                {
                    xStep++;
                    index++;
                }


                //if (merge && _container != null)
                //{
                //    TileWorldCombiner.CombineMesh(_container, createMultiMaterialMesh, addCollider);
                //}

                progress += _progressStep;

                yield return null;
            }
        }

        //Called from BuildMapIE if merge is true
        IEnumerator MergeWorldIE()
        {
            for (int c = 0; c < containers.Count; c++)
            {
                TileWorldCombiner.CombineMesh(containers[c], addCollider);

                if (createMultiMaterialMesh)
                {
                    //wait till next frame to make sure all objects are destroyed correctly before
                    //doing a second merge operation with multi material
                    yield return new WaitForEndOfFrame();
                    
                    TileWorldCombiner.CombineMulitMaterialMesh(containers[c], addCollider);
                }
            }

            yield return null;
        }
    

        void BuildWorld(bool[,] _map, int _layerIndex, int _layerCount, bool _rebuild)
        {
           
            if (presets.Count < 1)
                return;


            float _clusterSize = (float)clusterSize;
            if (_clusterSize == 0)
            {
                _clusterSize = 1;
            }

            //Build world in clusters
            //Divide width and height by clusterSize
            //Build each cluster and parent objects in a new gameobject
            float xSizeF = Mathf.Floor(width / ((float)width / _clusterSize));
            float ySizeF = Mathf.Floor(height / ((float)height / _clusterSize));

            int xSize = (int)xSizeF;
            int ySize = (int)ySizeF;

            int xStep = 0;
            int yStep = 0;

            int index = 0;

         

            GameObject _container = null;

            for (int s = 0; s < Mathf.Ceil((float)width / _clusterSize) * Mathf.Ceil((float)height / _clusterSize); s++)
            {
                if (containers.Count != Mathf.Ceil((float)width / _clusterSize) * Mathf.Ceil((float)height / _clusterSize))
                {

                    _container = new GameObject();
                    _container.transform.localScale = new Vector3(1, 1, 1);
                    _container.name = "cluster_" + s.ToString();
                    containers.Add(_container);

                }

                for (int y = 0 + (ySize * yStep); y < ySize + (ySize * yStep); y++)
                {


                    for (int x = 0 + (xSize * xStep); x < xSize + (xSize * xStep); x++)
                    {
                        CallInstantiation(_map, y, x, s, _layerCount, _layerIndex, _rebuild);
                        ApplyMask(_map, x, y, _layerIndex);   
                    }
                }

                if (index >= (width / _clusterSize) - 1)
                {
                    yStep++;
                    xStep = 0;
                    index = 0;
                }
                else
                {
                    xStep++;
                    index++;
                }
            }

        }


        void ApplyMask(bool[,] _map, int _x, int _y, int _layerIndex)
        {
            if (worldMap[_layerIndex].useMask)
            {
                if (_map[_x, _y] != worldMap[_layerIndex].maskMap[_x, _y])
                {
                    if (_x < worldMap[_layerIndex].gameObjectsMap.GetLength(0) && _y < worldMap[_layerIndex].gameObjectsMap.GetLength(1))
                    {

                        if (worldMap[_layerIndex].gameObjectsMap[_x, _y] != null)
                        {
                            DestroyImmediate(worldMap[_layerIndex].gameObjectsMap[_x, _y].gameObject);
                        }
                    }
                }
            }
        }

        void PartialMapChecker(bool _coroutine)
        {
            var i = mapIndex;
            //for (int i = 0; i < layerCount; i++)
            //{
                if (worldMap[i].oldMap.GetLength(0) > 0 && worldMap[i].oldMap.GetLength(1) > 0)
                {
                    for (int y = 0; y < worldMap[i].map.GetLength(1); y++)
                    {
                        for (int x = 0; x < worldMap[i].map.GetLength(0); x++)
                        {
                            if (worldMap[i].map[x, y] != worldMap[i].oldMap[x, y])
                            {
                                //t = how many border tiles should be replaced
                                for (int t = 1; t < 2; t++)
                                {

                                    //Debug.Log("map is different at: " + x + " " + y);
                                    if (worldMap[i].gameObjectsMap[x, y] != null)
                                    {
                                        DestroyImmediate(worldMap[i].gameObjectsMap[x, y].gameObject);
                                        
                                    }

                                    if (x + t < width)
                                    {
                                        if (worldMap[i].gameObjectsMap[x + t, y] != null)
                                        {
                                            DestroyImmediate(worldMap[i].gameObjectsMap[x + t, y].gameObject);
                                           
                                        }
                                    }
                                    if (x - t >= 0)
                                    {
                                        if (worldMap[i].gameObjectsMap[x - t, y] != null)
                                        {
                                            DestroyImmediate(worldMap[i].gameObjectsMap[x - t, y].gameObject);
                                            
                                        }
                                    }
                                    if (y + t < height)
                                    {
                                        if (worldMap[i].gameObjectsMap[x, y + t] != null)
                                        {
                                            DestroyImmediate(worldMap[i].gameObjectsMap[x, y + t].gameObject);
                                            
                                        }
                                    }
                                    if (y - t >= 0)
                                    {
                                        if (worldMap[i].gameObjectsMap[x, y - t] != null)
                                        {
                                            DestroyImmediate(worldMap[i].gameObjectsMap[x, y - t].gameObject);
                                            
                                        }
                                    }
                                    if (x - t >= 0 && y - t >= 0)
                                    {
                                        if (worldMap[i].gameObjectsMap[x - t, y - t] != null)
                                        {
                                            DestroyImmediate(worldMap[i].gameObjectsMap[x - t, y - t].gameObject);
                                            
                                        }
                                    }
                                    if (x - t >= 0 && y + t < height)
                                    {
                                        if (worldMap[i].gameObjectsMap[x - t, y + t] != null)
                                        {
                                            DestroyImmediate(worldMap[i].gameObjectsMap[x - t, y + t].gameObject);
                                            
                                        }
                                    }
                                    if (x + t < width && y - t >= 0)
                                    {
                                        if (worldMap[i].gameObjectsMap[x + t, y - t] != null)
                                        {
                                            DestroyImmediate(worldMap[i].gameObjectsMap[x + t, y - t].gameObject);
                                            
                                        }
                                    }
                                    if (x + t < width && y + t < height)
                                    {
                                        if (worldMap[i].gameObjectsMap[x + t, y + t] != null)
                                        {
                                            DestroyImmediate(worldMap[i].gameObjectsMap[x + t, y + t].gameObject);
                                           
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            //}

            if (_coroutine)
            {
                StartCoroutine(BuildMapIE(false, false));
            }
            else
            {
                BuildMap(false);
            } 
        }

        void CallInstantiation(bool[,] _map, int y, int x, int s, int _layerCount, int _layerIndex, bool _rebuild)
        {
            int _rndTilesetIndex = Random.Range(0, presets[layerPresetIndex[_layerIndex]].tiles.Count);

            float _rX = presets[layerPresetIndex[_layerIndex]].tiles[_rndTilesetIndex].blockOffset.x;
            float _rY = presets[layerPresetIndex[_layerIndex]].tiles[_rndTilesetIndex].blockOffset.y;
            float _rZ = presets[layerPresetIndex[_layerIndex]].tiles[_rndTilesetIndex].blockOffset.z;

            float _rfX = presets[layerPresetIndex[_layerIndex]].tiles[_rndTilesetIndex].floorOffset.x;
            float _rfY = presets[layerPresetIndex[_layerIndex]].tiles[_rndTilesetIndex].floorOffset.y;
            float _rfZ = presets[layerPresetIndex[_layerIndex]].tiles[_rndTilesetIndex].floorOffset.z;

            Vector3 _selT = new Vector3(_rX, _rY, _rZ);
            Vector3 _selTFloor = new Vector3(_rfX, _rfY, _rfZ);
            

            //BUILD INVERT FALSE
            //-------------------
            if (!invert && y < height && y >= 0 && x < width && x >= 0)
            {
                //build blocks
                if (!_map[x, y])
                {
                    //origin = new Vector3(transform.position.x + blocks[layerPresetIndex[_layerIndex]].blockOffset.x + x, transform.position.y + blocks[layerPresetIndex[_layerIndex]].blockOffset.y + _layerIndex, transform.position.z + blocks[layerPresetIndex[_layerIndex]].blockOffset.z + y);
                    origin = new Vector3(transform.position.x + ( _selT.x + x) * globalScale, transform.position.y + ( _selT.y + _layerIndex) * globalScale, transform.position.z + ( _selT.z + y) * globalScale);

                    if (layerCount < 2)
                    {                       
                        InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, false, containers[s], _rndTilesetIndex);    
                    }
                    else
                    {
                        //we have to check if the tile in the layer above is overlapping
                        if (!buildOverlappingTiles)
                        {
                            bool _isOverlapping = true;
                            
                            //check if tiles from layer above is overlapping
                            int lc = _layerCount + 1;

                            if (lc < layerCount)
                            {
                                if (!worldMap[lc].useMask)
                                {
                                    //only instantiate if tiles are not overlapping
                                    if (!_map[x, y] && worldMap[lc].map[x, y])
                                    {
                                        _isOverlapping = false;
                                    }
                                    else
                                    {
                                        _isOverlapping = true;
                                    }
                                }
                                else
                                {
                                    if (!_map[x, y] && worldMap[lc].maskMap[x, y])
                                    {
                                        _isOverlapping = false;
                                    }
                                    else
                                    {
                                        _isOverlapping = true;
                                    }
                                }
                            }

                            //tiles from the top layer will always get builded
                            if (_layerCount == layerCount - 1 )
                            {
                                _isOverlapping = false;
                            }

                            if (!_isOverlapping)
                            {
                                InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, false, containers[s], _rndTilesetIndex);
                            }

                            /*
                            if (_layerCount + 1 < layerCount)
                            {
                                if (!worldMap[_layerCount + 1].useMask)
                                {
                                    //only instantiate if tiles are not overlapping
                                    if (!_map[x, y] && worldMap[_layerCount + 1].map[x, y])
                                    {
                                        InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, false, containers[s], _rndTilesetIndex);
                                    }
                                }
                                else
                                {
                                    //only instantiate if tiles are not overlapping with mask map
                                    if (!_map[x, y] && worldMap[_layerCount + 1].maskMap[x, y])
                                    {
                                        InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, false, containers[s], _rndTilesetIndex);
                                    }
                                }

                            }
                            else //no layers above current
                            {

                                InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, false, containers[s], _rndTilesetIndex);
                            }*/
                            
                        }
                        else
                        {         
                            InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, false, containers[s], _rndTilesetIndex);        
                        }
                    }
                }

                //build floor
                if (_map[x, y] && createFloor && _layerCount < 1)
                {
                    //origin = new Vector3(transform.position.x + blocks[layerPresetIndex[_layerIndex]].floorOffset.x + x, transform.position.y + blocks[layerPresetIndex[_layerIndex]].floorOffset.y + _layerIndex, transform.position.z + blocks[layerPresetIndex[_layerIndex]].floorOffset.z + y);
                    origin = new Vector3(transform.position.x + ( _selTFloor.x + x) * globalScale, transform.position.y + ( _selTFloor.y + _layerIndex) * globalScale, transform.position.z + ( _selTFloor.z + y) * globalScale);

                    if (layerCount < 2)
                    {
                        InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, true, containers[s], _rndTilesetIndex);
                    }
                    else
                    {
                        if (!buildOverlappingTiles)
                        {
                            if (_layerCount == 0 && _layerCount + 1 < layerCount)
                            {

                                //only instantiate if tiles are not overlapping
                                if (_map[x, y] && worldMap[_layerCount + 1].map[x, y])
                                {

                                    InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, true, containers[s], _rndTilesetIndex);
                                }
                            }
                            else
                            {
                                InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, true, containers[s], _rndTilesetIndex);
                            }
                        }
                        else
                        {
                            InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, true, containers[s], _rndTilesetIndex);
                        }
                    }
                }
            }


            //BUILD INVERT TRUE
            //-----------------
            else if (y < height && y >= 0 && x < width && x >= 0)
            {
                if (_map[x, y])
                {
                    //origin = new Vector3(transform.position.x + blocks[layerPresetIndex[_layerIndex]].blockOffset.x + x, transform.position.y + blocks[layerPresetIndex[_layerIndex]].blockOffset.y + _layerIndex, transform.position.z + blocks[layerPresetIndex[_layerIndex]].blockOffset.z + y);
                    origin = new Vector3(transform.position.x + ( _selT.x + x) * globalScale, transform.position.y + ( _selT.y + _layerIndex) * globalScale, transform.position.z + ( _selT.z + y) * globalScale);

                    if (layerCount < 2)
                    {
                        InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, false, containers[s], _rndTilesetIndex);
                    }
                    else
                    {
                        if (!buildOverlappingTiles)
                        {
                            //we have to check if the tile in the layer above is overlapping
                            if (_layerCount + 1 < layerCount)
                            {
                                if (!worldMap[_layerCount + 1].useMask)
                                {
                                    //only instantiate if tiles are not overlapping
                                    if (_map[x, y] && !worldMap[_layerCount + 1].map[x, y])
                                    {
                                        InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, false, containers[s], _rndTilesetIndex);
                                    }
                                }
                                else
                                {
                                    //only instantiate if tiles are not overlapping
                                    if (_map[x, y] && !worldMap[_layerCount + 1].maskMap[x, y])
                                    {
                                        InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, false, containers[s], _rndTilesetIndex);
                                    }
                                }
                            }
                            else //no layers above current
                            {
                                InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, false, containers[s], _rndTilesetIndex);
                            }

                        }
                        else
                        {
                            InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, false, containers[s], _rndTilesetIndex);
                        }
                    }

                }

                //build floor
                if (!_map[x, y] && createFloor && _layerCount < 1)
                {
                    //origin = new Vector3(transform.position.x + blocks[layerPresetIndex[_layerIndex]].floorOffset.x + x, transform.position.y + blocks[layerPresetIndex[_layerIndex]].floorOffset.y + _layerIndex, transform.position.z + blocks[layerPresetIndex[_layerIndex]].floorOffset.z + y);
                    origin = new Vector3(transform.position.x + (_selTFloor.x + x) * globalScale, transform.position.y + ( _selTFloor.y + _layerIndex) * globalScale, transform.position.z + (_selTFloor.z + y) * globalScale);

                    if (layerCount < 2)
                    {
                        InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, true, containers[s], _rndTilesetIndex);
                    }
                    else
                    {
                        if (!buildOverlappingTiles)
                        {
                            if (_layerCount == 0 && _layerCount + 1 < layerCount)
                            {

                                //only instantiate if tiles are not overlapping
                                if (!_map[x, y] && !worldMap[_layerCount + 1].map[x, y])
                                {
                                    InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, true, containers[s], _rndTilesetIndex);
                                }

                            }
                            else
                            {
                                InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, true, containers[s], _rndTilesetIndex);
                            }
                        }
                        else
                        {
                            InstantiateBlocks(_map, x, y, _layerIndex, _layerCount, _rebuild, true, containers[s], _rndTilesetIndex);
                        }
                    }

                }
            }
        }


        void InstantiateBlocks(bool[,] _map, int x, int y, int _layerIndex, int _layerCount, bool _rebuild, bool _createFloor, GameObject _container, int _rndTileSetIndex)
        {

            if (worldMap[_layerIndex].gameObjectsMap[x, y] != null && !firstTimeBuild)
            {
                return;
            }

            if (presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileI == null)
            {
                presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileI = new GameObject("empty_tileI");
                presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileI.transform.parent = _container.transform;
            }
            else if (presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileC == null)
            {
                presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileC = new GameObject("empty_tileC");
                presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileC.transform.parent = _container.transform;
            }
            else if ( presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileCi == null )
            {
                presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileCi = new GameObject("empty_tileCi");
                presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileCi.transform.parent = _container.transform;
            }
            else if (presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileB == null)
            {
                presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileB = new GameObject("empty_tileB");
                presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileB.transform.parent = _container.transform;
            }
            else if (presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileF == null)
            {
                presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileF = new GameObject("empty_tileF");
                presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileF.transform.parent = _container.transform;
            }

            // I = Tile with one edge
            // C = Corner tile
            // Ci = Corner tile inverted
            // B = Full tile
            // F = floor tile
            GameObject _instance = null;
           
            //TileWorldCache.TileType _type = TileWorldCache.TileType.I;

            if (_map[x, y] == invert)
            {

                int cbs = TileWorldNeighbourCounter.CountCrossNeighbours(_map, x, y, 1, _createFloor, invert);

                //different block types according to different neighbours count
                //B = full tile
                if (cbs == 0)
                {
                   
                    
                    _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileB, origin, Quaternion.identity) as GameObject;
                  
                    
                    AddYOffsetRotation(3, _instance, _rndTileSetIndex, _layerIndex);
                   
                    
                    
                    int _itc = TileWorldNeighbourCounter.CountInnerTerrainBlocks(_map, x, y, 1, invert);

                    //only assign the very first time
                    var _li = 1;


                    if (_itc >= (((_li * 2) + 1) * ((_li * 2) + 1)) - 1)
                    {

                        if (!_rebuild)
                        {
                            //this will only be executed on first layer layerCount = 0
                            if (_layerCount < worldMap.Count - 1) //.mountainMaps.Count)
                            {
                                worldMap[_layerCount + 1].map[x, y] = false;
                            }
                        }

                    }
                    else
                    {
                        //This is important for assigning new orientations
                        //even though we do not use the integer values
                        int _itv = TileWorldNeighbourCounter.CountDiagonalNeighbours(_map, x, y, 1, true, invert);

                       
                            DestroyImmediate(_instance.gameObject);
                            
                            _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileCi, origin, Quaternion.identity) as GameObject;
                        
                              if (orientation == "swsenw")
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                            }
                            else if (orientation == "swsene")
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
                            }
                            else if (orientation == "senwne")
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                            }
                            else if (orientation == "swnwne")
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 270, 0));
                            }
                        
                    

                        AddYOffsetRotation(2, _instance, _rndTileSetIndex, _layerIndex);
                    }

                }


                //I = one edge tile
                if (cbs == 1)
                {   
                   
                        _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileI, origin, Quaternion.identity) as GameObject;
                    
                    
                         if (orientation == "w")
                        {
                            _instance.transform.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
                        }
                        else if (orientation == "n")
                        {
                            _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                        }
                        else if (orientation == "e")
                        {
                            _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
                        }
                        else if (orientation == "s")
                        {
                            _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                        }
                    

                   

                    AddYOffsetRotation(0, _instance, _rndTileSetIndex, _layerIndex);

                    //if we are at the edge of the map
                    //replace this tile with a block tileB
                    if (x == width - 1 || x == 0 || y == height - 1 || y == 0)
                    {
                      
                            
                            DestroyImmediate(_instance.gameObject);

                            _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileB, origin, Quaternion.identity) as GameObject;
                       


                        AddYOffsetRotation(3, _instance, _rndTileSetIndex, _layerIndex);
                    }

                    //theres one special case here
                    //check how many neighbours we have diagonal
                    int _idd = TileWorldNeighbourCounter.CountDiagonalNeighbours(_map, x, y, 1, false, invert);

                    //if theres only one neighbour
                    //change this tile with a Ci tile
                    if (_idd == 1)
                    {
                        //check if tile is on a border edge
                        if (x == 0 || y == 0 || x == width - 1 || y == height - 1)
                        {
                          
                                
                                DestroyImmediate(_instance.gameObject);
                                
                                _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileCi, origin, Quaternion.identity) as GameObject;
                            
                                if (orientation == "ne")
                                {
                                    _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                                }
                                else if (orientation == "nw")
                                {
                                    _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
                                }
                                else if (orientation == "sw")
                                {
                                    _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                                }
                                else if (orientation == "se")
                                {
                                    _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 270, 0));
                                }
                            
                           

                            AddYOffsetRotation(2, _instance, _rndTileSetIndex, _layerIndex);
                        }
                    }

                }

                //C = corner
                if (cbs == 2)
                {
                    //for all four corners we have to check if theres a diagonal neighbour
                    //if theres one, instantiate a inverted corner instead of a block tile.
                    int _gc = 0;

                    
                        //Instantiate corner
                        _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileC, origin, Quaternion.identity) as GameObject;
                    


                    AddYOffsetRotation(1, _instance, _rndTileSetIndex, _layerIndex);

                    //special case
                    //those are no corner blocks but blocks with a west and east side
                    //or north and south side.
                    //mostly on the border of a map
                    //also we have to check if it is in a single row
                    //next to a double row, then we have to replace it by an inverted corner
                    if (orientation == "we")
                    {
                        
                            //TileWorldCache.Instance.DespawnTile(_instance, _type);
                            DestroyImmediate(_instance);
                            //_type = TileWorldCache.TileType.I;
                            //_instance = TileWorldCache.Instance.SpawnTile(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileI, _type, origin, Quaternion.identity);
                            _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileI, origin, Quaternion.identity) as GameObject;
                        
                        
                             //if its on the right border edge
                            if (x > 0)
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 270, 0));
                            }
                            //else its on the left border edge
                            else
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
                            }

                      
                       
                        AddYOffsetRotation(0, _instance, _rndTileSetIndex, _layerIndex);

                    }
                    else if (orientation == "ns")
                    {
                        
                            
                            DestroyImmediate(_instance);

                            _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileI, origin, Quaternion.identity) as GameObject;

                            //if its on the upper edge
                            if (y > 0)
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                            }
                            //else its on the lower border edge
                            else
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                            }


                        AddYOffsetRotation(0, _instance, _rndTileSetIndex, _layerIndex);

                    }

                    //Normal Corner blocks
                    //--------------------
                    else if (orientation == "nw")
                    {
                       
                        _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                        

                        AddYOffsetRotation(1, _instance, _rndTileSetIndex, _layerIndex);

                        //Special cases!
                        //--------------
                        //bottomleft corner replace with a block tile or if theres one diagonal neighbour replace with an inverted corner
                        if (y == 0 && x == 0)
                        {
                            _gc = TileWorldNeighbourCounter.CountDiagonalNeighbours(_map, x, y, 1, false, invert);

                            if (_gc == 1)
                            {
                               
                                DestroyImmediate(_instance);
                                    
                                _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileCi, origin, Quaternion.identity) as GameObject;
                               

                                AddYOffsetRotation(2, _instance, _rndTileSetIndex, _layerIndex);
                            }
                            //bottom left corner
                            else
                            {
                               
                                DestroyImmediate(_instance);
                                _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileB, origin, Quaternion.identity) as GameObject;
                              

                                AddYOffsetRotation(3, _instance, _rndTileSetIndex, _layerIndex);
                            }
                        }

                        //check if tile is at the left or bottom border edge
                        //if it is change corner tile to edge tile
                        else if ((y != height - 1 && y != 0 && x == 0) || (x != width - 1 && x != 0 && y == 0))
                        {
                           
                            DestroyImmediate(_instance);
                            _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileI, origin, Quaternion.identity) as GameObject;
                           
                                //left side
                            if (y != height - 1 && y != 0 && x == 0)
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                            }
                            //bottom side
                            else if (x != width - 1 && x != 0 && y == 0)
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 270, 0));
                            }
                            
                            

                            AddYOffsetRotation(0, _instance, _rndTileSetIndex, _layerIndex);
                        }
                        //-----------


                    }
                    else if (orientation == "ne")
                    {
                      
                        _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
                        
                        
                        AddYOffsetRotation(1, _instance, _rndTileSetIndex, _layerIndex);

                        //Special cases!
                        //--------------
                        //bottomRight corner replace with a block tile or if theres one diagonal neighbour replace with an inverted corner
                        if (y == 0 && x == width - 1)
                        {
                            _gc = TileWorldNeighbourCounter.CountDiagonalNeighbours(_map, x, y, 1, false, invert);

                            if (_gc == 1)
                            {
                               
                                DestroyImmediate(_instance);
                                _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileCi, origin, Quaternion.identity) as GameObject;
                               

                                AddYOffsetRotation(2, _instance, _rndTileSetIndex, _layerIndex);
                            }
                            //bottom right corner
                            else
                            {
                                
                                DestroyImmediate(_instance);
                                _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileB, origin, Quaternion.identity) as GameObject;
                              

                                AddYOffsetRotation(3, _instance, _rndTileSetIndex, _layerIndex);
                            }
                        }

                        //check if tile is at the right or bottom border edge
                        //if it is change corner tile to edge tile
                        else if ((y != height - 1 && y != 0 && x == width - 1) || (x != width && x != 0 && y == 0))
                        {
                            
                            DestroyImmediate(_instance);
                            _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileI, origin, Quaternion.identity) as GameObject;
                            
                            //right side
                            if (y != height - 1 && y != 0 && x == width - 1)
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                            }
                            //bottom side
                            else if ((x != width && x != 0 && y == 0))
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
                            }
                           

                            AddYOffsetRotation(0, _instance, _rndTileSetIndex, _layerIndex);
                        }
                        //--------------

                    }
                    else if (orientation == "es")
                    {
                       
                        _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                       
                        
                        AddYOffsetRotation(1, _instance, _rndTileSetIndex, _layerIndex);

                        //Special cases!
                        //--------------
                        //topRight corner replace with a block tile or if theres one diagonal neighbour replace with an inverted corner
                        if (y == height - 1 && x == width - 1)
                        {
                            _gc = TileWorldNeighbourCounter.CountDiagonalNeighbours(_map, x, y, 1, false, invert);

                            if (_gc == 1)
                            {
                                
                                DestroyImmediate(_instance);
                                _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileCi, origin, Quaternion.identity) as GameObject;
                               

                                AddYOffsetRotation(2, _instance, _rndTileSetIndex, _layerIndex);
                            }
                            else
                            {
                                
                                DestroyImmediate(_instance);
                                _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileB, origin, Quaternion.identity) as GameObject;
                              

                                AddYOffsetRotation(3, _instance, _rndTileSetIndex, _layerIndex);
                            }
                        }

                        //check if tile is at the right or top border edge
                        //if it is change corner tile to edge tile
                        if ((y != height - 1 && y != 0 && x == width - 1) || (x != 0 && x != width - 1 && y == height - 1))
                        {
                           
                            DestroyImmediate(_instance);
                            _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileI, origin, Quaternion.identity) as GameObject;
                            
                            //right side
                            if (y != height - 1 && y != 0 && x == width - 1)
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                            }
                            //top side
                            else if (x != 0 && x != width - 1 && y == height - 1)
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
                            }
                            
                            
                           

                            AddYOffsetRotation(0, _instance, _rndTileSetIndex, _layerIndex);
                        }
                        //--------------

                    }
                    else if (orientation == "ws")
                    {
                       
                        _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 270, 0));
                       
                        AddYOffsetRotation(1, _instance, _rndTileSetIndex, _layerIndex);

                        //Special cases!
                        //--------------
                        //topLeft corner replace with a block tile or if theres one diagonal neighbour replace with an inverted corner	
                        if (y == height - 1 && x == 0)
                        {
                            //_gc = CountDiagonalNeighbours(_map, x, y, 1, false);

                            if (_gc == 1)
                            {
                               
                                DestroyImmediate(_instance);
                                _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileCi, origin, Quaternion.identity) as GameObject;
                                

                                AddYOffsetRotation(2, _instance, _rndTileSetIndex, _layerIndex);
                            }
                            else
                            {
                               
                                DestroyImmediate(_instance);
                                _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileB, origin, Quaternion.identity) as GameObject;
                              

                                AddYOffsetRotation(3, _instance, _rndTileSetIndex, _layerIndex);
                            }
                        }

                        //check if tile is at the left or top border edge
                        //if it is change corner tile to edge tile
                        if ((y != height - 1 && y != 0 && x == 0) || (x != 0 && x != width - 1 && y == height - 1))
                        {
                            
                            DestroyImmediate(_instance);
                            _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileI, origin, Quaternion.identity) as GameObject;
                            
                            
                                //left side
                            if (y != height - 1 && y != 0 && x == 0)
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                            }
                            //top side
                            else if (x != 0 && x != width - 1 && y == height - 1)
                            {
                                _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 270, 0));
                            }
                            
                         

                            AddYOffsetRotation(0, _instance, _rndTileSetIndex, _layerIndex);
                        }
                        //--------------
                    }
                }


                //Ci = corner inverted
                if (cbs == 3)
                {
                   
                    _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileCi, origin, Quaternion.identity) as GameObject;
                    
                    
                    if (orientation == "nwe")
                    {
                        _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                    }
                    else if (orientation == "wes")
                    {
                        _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                    }
                    else if (orientation == "nws")
                    {
                        _instance.transform.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
                    }
                    else if (orientation == "nes")
                    {
                        _instance.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
                    }
                    
                   

                    AddYOffsetRotation(2, _instance, _rndTileSetIndex, _layerIndex);

                }

            }
            //create floor tile
            else if (_createFloor)
            {

                if (presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileF != null)
                {
                    
                    _instance = Instantiate(presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].tileF, origin, Quaternion.identity) as GameObject;
                  

                    AddYOffsetRotation(4, _instance, _rndTileSetIndex, _layerIndex);
                }
               

            }

            //parent instance to container
            if (_instance != null)
            {
                _instance.transform.parent = _container.transform;
            }


            if (scaleTiles && _instance != null)
            {
                _instance.transform.localScale = new Vector3(_instance.transform.localScale.x * globalScale, _instance.transform.localScale.y * globalScale, _instance.transform.localScale.z * globalScale);
            }

           
            worldMap[_layerIndex].gameObjectsMap[x, y] = _instance;
           
            ////assign tile type
            //worldMap[_layerIndex].type[x, y] = _type;
        }

        void AddYOffsetRotation(int _type, GameObject _instance, int _rndTileSetIndex, int _layerIndex)
        {
            _instance.transform.rotation *= Quaternion.Euler(new Vector3(0, presets[layerPresetIndex[_layerIndex]].tiles[_rndTileSetIndex].yRotationOffset[_type], 0));
        }
        #endregion

        //--------------------
        //MAP OPTIMIZATION
        //--------------------
        #region optimization
        //this should get rid of all cells with only one or two neighbouring cells
        //occured when generating maps with multiple layers.
        void OptimizePass(int _index)
        {
     
                bool[,] _tmpMap = new bool[width, height];

                for (int h = 0; h < height; h++)
                {
                    for (int g = 0; g < width; g++)
                    {
                        _tmpMap[g, h] = worldMap[_index].map[g, h];
                    }
                }

            
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {

                        if (_tmpMap[x, y] == invert)
                        {

                            int _countN = TileWorldNeighbourCounter.CountCrossNeighbours(_tmpMap, x, y, 1, false, !invert);
                            if (_countN <= 2)
                            {
                                //check if each neighbour is on the opposite side
                                switch (orientation)
                                {
                                    case "ns":
                                        worldMap[_index].map[x, y] = !invert;
                                        break;
                                    case "we":
                                        worldMap[_index].map[x, y] = !invert;
                                        break;
                                    case "n":
                                        worldMap[_index].map[x, y] = !invert;
                                        break;
                                    case "s":
                                        worldMap[_index].map[x, y] = !invert;
                                        break;
                                    case "w":
                                        worldMap[_index].map[x, y] = !invert;
                                        break;
                                    case "e":   
                                        worldMap[_index].map[x, y] = !invert;
                                        break;
                                    case "":                  
                                        worldMap[_index].map[x, y] = !invert;
                                        break;
                                }
                            }


                            //fill tiles between narrow spaces
                            //example:
                            /*
                                x = tile
				
                                ooxxo
                                oFxxo
                                oxxFo
                                oxxoo
			
                                F = fails must be filled with a block tile
                            */

                            int _countD = TileWorldNeighbourCounter.CountDiagonalNeighbours(_tmpMap, x, y, 1, false, !invert);
                            if (_countD <= 2)
                            {
                               
                                switch(orientation)
                                {
                                    case "swne":
                                        int _countCA = TileWorldNeighbourCounter.CountCrossNeighbours(_tmpMap, x, y, 1, false, !invert);
                                        if (_countCA == 3)
                                        {
                                            worldMap[_index].map[x - 1, y] = invert;
                                            worldMap[_index].map[x + 1, y] = invert;
                                            worldMap[_index].map[x, y - 1] = invert;
                                            worldMap[_index].map[x, y + 1] = invert;
                                        }
                                        else if (_countCA == 4)
                                        {
                                            worldMap[_index].map[x - 1, y + 1] = invert;
                                            worldMap[_index].map[x + 1, y - 1] = invert;
                                            worldMap[_index].map[x - 1, y - 1] = invert;
                                            worldMap[_index].map[x + 1, y + 1] = invert;
                                        }        
                                        break;
                                    case "nesw":
                                        int _countCB = TileWorldNeighbourCounter.CountCrossNeighbours(_tmpMap, x, y, 1, false, !invert);
                                        if (_countCB == 3)
                                        {
                                            worldMap[_index].map[x - 1, y] = invert;
                                            worldMap[_index].map[x + 1, y] = invert;
                                            worldMap[_index].map[x, y - 1] = invert;
                                            worldMap[_index].map[x, y + 1] = invert;
                                        }
                                        else if (_countCB == 4)
                                        {
                                            worldMap[_index].map[x - 1, y + 1] = invert;
                                            worldMap[_index].map[x + 1, y - 1] = invert;
                                            worldMap[_index].map[x - 1, y - 1] = invert;
                                            worldMap[_index].map[x + 1, y + 1] = invert;
                                        }
                                        break;
                                    case "senw":
                                        int _countCC = TileWorldNeighbourCounter.CountCrossNeighbours(_tmpMap, x, y, 1, false, !invert);
                                        if (_countCC == 3)
                                        {
                                            worldMap[_index].map[x - 1, y] = invert;
                                            worldMap[_index].map[x + 1, y] = invert;
                                            worldMap[_index].map[x, y - 1] = invert;
                                            worldMap[_index].map[x, y + 1] = invert;
                                        }
                                        else if (_countCC == 4)
                                        {
                                            worldMap[_index].map[x - 1, y + 1] = invert;
                                            worldMap[_index].map[x + 1, y - 1] = invert;
                                            worldMap[_index].map[x - 1, y - 1] = invert;
                                            worldMap[_index].map[x + 1, y + 1] = invert;
                                        }
                                        break;
                                    case "nwse":
                                        int _countCD = TileWorldNeighbourCounter.CountCrossNeighbours(_tmpMap, x, y, 1, false, !invert);
                                        if (_countCD == 3)
                                        {
                                            worldMap[_index].map[x - 1, y] = invert;
                                            worldMap[_index].map[x + 1, y] = invert;
                                            worldMap[_index].map[x, y - 1] = invert;
                                            worldMap[_index].map[x, y + 1] = invert;
                                        }
                                        else if (_countCD == 4)
                                        {
                                            worldMap[_index].map[x - 1, y + 1] = invert;
                                            worldMap[_index].map[x + 1, y - 1] = invert;
                                            worldMap[_index].map[x - 1, y - 1] = invert;
                                            worldMap[_index].map[x + 1, y + 1] = invert;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
        }

        //same as OptimizePass but does it only in a certain radius from x and y position
        /// <summary>
        /// Does an optimization pass only in a certain area from x and y position
        /// </summary>
        /// <param name="_index"></param>
        /// <param name="_x"></param>
        /// <param name="_y"></param>
        public void OptimizePassPartial(int _index, int _x, int _y)
        {
            //create a new partial map 9x9
            bool[,] _tmpMap = new bool[9, 9];

            for (int h = -4; h < 5; h++)
            {
                for (int g = -4; g < 5; g++)
                {
                    
                    if (_x + g < worldMap[_index].map.GetLength(0) && _x + g >= 0 && _y + h < worldMap[_index].map.GetLength(1) && _y + h >= 0)
                    {
                        _tmpMap[g + 4, h + 4] = worldMap[_index].map[_x + g, _y + h];
                    }
                    else
                    {
                        _tmpMap[g + 4, h + 4] = !invert;
                    }
                }
            }

            
            for (int y = -3; y < 4; y++)
            {
                for (int x = -3; x < 4; x++)
                {

                    if (_tmpMap[x + 4, y + 4] == invert)
                    {

                        int _countN = TileWorldNeighbourCounter.CountCrossNeighbours(_tmpMap, x + 4, y + 4, 1, false, !invert);
                        if (_countN <= 2)
                        {
                            //check if each neighbour is on the opposite side
                            switch (orientation)
                            {
                                case "ns":
                                    worldMap[_index].map[_x + x, _y + y] = !invert;
                                    break;
                                case "we":
                                    worldMap[_index].map[_x + x, _y + y] = !invert;
                                    break;
                                case "n":
                                    worldMap[_index].map[_x + x, _y + y] = !invert;
                                    break;
                                case "s":
                                    worldMap[_index].map[_x + x, _y + y] = !invert;
                                    break;
                                case "w":
                                    worldMap[_index].map[_x + x, _y + y] = !invert;
                                    break;
                                case "e":
                                    worldMap[_index].map[_x + x, _y + y] = !invert;
                                    break;
                                case "":
                                    worldMap[_index].map[_x + x, _y + y] = !invert;
                                    break;
                            }
                        }


                        //fill tiles between narrow spaces
                        //example:
                        /*
                            x = tile
				
                            ooxxo
                            oFxxo
                            oxxFo
                            oxxoo
			
                            F = fails must be filled with a block tile
                        */

                        int _countD = TileWorldNeighbourCounter.CountDiagonalNeighbours(_tmpMap, x + 4, y + 4, 1, false, !invert);
                        if (_countD <= 2)
                        {
                               
                            switch(orientation)
                            {
                                case "swne":
                                    int _countCA = TileWorldNeighbourCounter.CountCrossNeighbours(_tmpMap, x + 4, y + 4, 1, false, !invert);
                                    if (_countCA == 3)
                                    {
                                        worldMap[_index].map[(_x + x) - 1, _y + y] = invert;
                                        worldMap[_index].map[(_x + x) + 1, _y + y] = invert;
                                        worldMap[_index].map[_x + x, (_y + y) - 1] = invert;
                                        worldMap[_index].map[_x + x, (_y + y) + 1] = invert;
                                    }
                                    else if (_countCA == 4)
                                    {
                                        worldMap[_index].map[(_x + x) - 1, (_y + y) + 1] = invert;
                                        worldMap[_index].map[(_x + x) + 1, (_y + y) - 1] = invert;
                                        worldMap[_index].map[(_x + x) - 1, (_y + y) - 1] = invert;
                                        worldMap[_index].map[(_x + x) + 1, (_y + y) + 1] = invert;
                                    }        
                                    break;
                                case "nesw":
                                    int _countCB = TileWorldNeighbourCounter.CountCrossNeighbours(_tmpMap, x + 4, y + 4, 1, false, !invert);
                                    if (_countCB == 3)
                                    {
                                        worldMap[_index].map[(_x + x) - 1, _y + y] = invert;
                                        worldMap[_index].map[(_x + x) + 1, _y + y] = invert;
                                        worldMap[_index].map[(_x + x), (_y + y) - 1] = invert;
                                        worldMap[_index].map[_x + x, (_y + y) + 1] = invert;
                                    }
                                    else if (_countCB == 4)
                                    {
                                        worldMap[_index].map[(_x + x) - 1, (_y + y) + 1] = invert;
                                        worldMap[_index].map[(_x + x) + 1, (_y + y) - 1] = invert;
                                        worldMap[_index].map[(_x + x) - 1, (_y + y) - 1] = invert;
                                        worldMap[_index].map[(_x + x) + 1, (_y + y) + 1] = invert;
                                    }
                                    break;
                                case "senw":
                                    int _countCC = TileWorldNeighbourCounter.CountCrossNeighbours(_tmpMap, x + 4, y + 4, 1, false, !invert);
                                    if (_countCC == 3)
                                    {
                                        worldMap[_index].map[(_x + x) - 1, _y + y] = invert;
                                        worldMap[_index].map[(_x + x) + 1, _y + y] = invert;
                                        worldMap[_index].map[_x + x, (_y + y) - 1] = invert;
                                        worldMap[_index].map[_x + x, (_y + y) + 1] = invert;
                                    }
                                    else if (_countCC == 4)
                                    {
                                        worldMap[_index].map[(_x + x) - 1, (_y + y) + 1] = invert;
                                        worldMap[_index].map[(_x + x) + 1, (_y + y) - 1] = invert;
                                        worldMap[_index].map[(_x + x) - 1, (_y + y) - 1] = invert;
                                        worldMap[_index].map[(_x + x) + 1, (_y + y) + 1] = invert;
                                    }
                                    break;
                                case "nwse":
                                    int _countCD = TileWorldNeighbourCounter.CountCrossNeighbours(_tmpMap, x + 4, y + 4, 1, false, !invert);
                                    if (_countCD == 3)
                                    {
                                        worldMap[_index].map[(_x + x) - 1, _y + y] = invert;
                                        worldMap[_index].map[(_x + x) + 1, _y + y] = invert;
                                        worldMap[_index].map[_x + x, (_y + y) - 1] = invert;
                                        worldMap[_index].map[_x + x, (_y + y) + 1] = invert;
                                    }
                                    else if (_countCD == 4)
                                    {
                                        worldMap[_index].map[(_x + x) - 1, (_y + y) + 1] = invert;
                                        worldMap[_index].map[(_x + x) + 1, (_y + y) - 1] = invert;
                                        worldMap[_index].map[(_x + x) - 1, (_y + y) - 1] = invert;
                                        worldMap[_index].map[(_x + x) + 1, (_y + y) + 1] = invert;
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        //--------------------
        //EDITOR METHODS
        //--------------------
        #region editormethods
      

        void NewContainer()
        { 
            
            if (worldName == "")
            {
                worldName = "TWC_World";
            }

            worldContainer = GameObject.Find(worldName);

            if (worldContainer != null)
            {
                DestroyImmediate(worldContainer);
            }

            //delete old container list and create a new one
            if (containers.Count > 0)
            {
                for (int c = 0; c < containers.Count; c++)
                {
                    if (containers[c] != null)
                    {
                        DestroyImmediate(containers[c].gameObject);
                    }
                }
            }

            worldContainer = new GameObject();
            

            worldContainer.name = worldName;

            containers = new List<GameObject>();
        }


        /// <summary>
        /// Editor method. Fill the current layer
        /// </summary>
        public void FillLayerBlock()
        {
            for (int y = 0; y < worldMap[mapIndex].map.GetLength(1); y++)
            {
                for (int x = 0; x < worldMap[mapIndex].map.GetLength(0); x++)
                {
                    if (worldMap[mapIndex].map[x, y] != invert)
                    {
                        worldMap[mapIndex].map[x, y] = invert;
                    }
                }
            }

            UpdateMap();
        }

      
        /// <summary>
        /// Editor method. Clear the current layer.
        /// </summary>
        public void FillLayerFloor()
        {
            for (int y = 0; y < worldMap[mapIndex].map.GetLength(1); y++)
            {
                for (int x = 0; x < worldMap[mapIndex].map.GetLength(0); x++)
                {
                    if (worldMap[mapIndex].map[x, y] == invert)
                    {

                        worldMap[mapIndex].map[x, y] = !invert;

                    }
                }
            }

            UpdateMap();
        }


        /// <summary>
        /// add new layer
        /// </summary>
        /// <param name="_addNewLayerFromEdit"></param>
        /// <param name="_useMask"></param>
        /// <param name="_selectedMask"></param>
        /// <param name="_duplicate"></param>
        /// <param name="_duplicateFromIndex"></param>
        public void AddNewLayer(bool _addNewLayerFromEdit, bool _useMask, int _selectedMask, bool _duplicate, int _duplicateFromIndex)
        {
            //simple fix for blocking layer creation the first time
            //if there's no map generated
            if (worldMap.Count < 1)
                return;

            if (invert)
            {
                worldMap.Add(new Maps(width, height, false, _useMask, _selectedMask));
            }
            else
            {
                worldMap.Add(new Maps(width, height, true, _useMask, _selectedMask));
            }

            //ResizeStringArray(false, 0);
            ResizeIntArray(false, 0);

            if (_duplicate)
            {
                worldMap[worldMap.Count - 1].map = (bool[,])worldMap[_duplicateFromIndex].map.Clone();
            }


            if (_addNewLayerFromEdit)
            {
                layerCount++;
                UpdateMap();
            }


        }

        /// <summary>
        /// Editor method. Remove layer.
        /// </summary>
        /// <param name="_inx"></param>
        public void RemoveLayer(int _inx)
        {

            for (int y = 0; y < worldMap[_inx].gameObjectsMap.GetLength(1); y++)
            {
                for (int x = 0; x < worldMap[_inx].gameObjectsMap.GetLength(0); x++)
                {
                    if (worldMap[_inx].gameObjectsMap[x, y] != null)
                    {
                        DestroyImmediate(worldMap[_inx].gameObjectsMap[x, y].gameObject);
                    }
                }
            }

            ResizeIntArray(true, _inx);

            worldMap.RemoveAt(_inx);
            mapIndex = 0;
            layerCount--;


            //ResizeStringArray(true, _inx);
        }

        /// <summary>
        /// Editor method. Add a new mask to layer
        /// </summary>
        /// <param name="_inx"></param>
        public void AddNewMask(int _inx)
        {
            worldMap[_inx].useMask = true;

            worldMap[_inx].maskMap = new bool[width, height];
            worldMap[_inx].maskMap = iMasks[worldMap[_inx].selectedMask].ApplyMask(worldMap[_inx].map, GetComponent<TileWorldCreator>());
        }

        /// <summary>
        /// Editor method. Clear all settings.
        /// </summary>
        /// <param name="_clearPresets"></param>
        /// <param name="_layerCount"></param>
        public void ClearSettings(bool _clearPresets, int _layerCount)
        {
            worldMap = new List<Maps>();


            layerPresetIndex = new int[_layerCount];
            

            if (_clearPresets)
            {
                presets = new List<Presets>();
                presets.Add(new Presets());
                
                //blockLayerPresets = new string[0];

                layerPresetIndex = new int[0];
                blockLayerPresets = new string[0];

                ResizeStringArray(false, 0);
                ResizeIntArray(false, 0);

                worldMap = new List<Maps>();
                worldMap.Add(new Maps(width, height, true, false, 0));
                settings = new Settings();
                AssignSettings();
            }


            mapIndex = 0;

        }

        /// <summary>
        /// Editor method. Copy map from a layer.
        /// </summary>
        public void CopyMapFromLayer()
        {
            cloneMap = new bool[width, height];

            cloneMap = (bool[,])worldMap[mapIndex].map.Clone();
        }

        /// <summary>
        /// Editor method. Paste copied map to a layer.
        /// </summary>
        public void PasteMapToLayer()
        {
            worldMap[mapIndex].map = (bool[,])cloneMap.Clone();

            UpdateMap();
        }

        // this is only for the layer presets editor
        //EditorGUILayout.popup
        //we have to resize the int[] and string[] array by preserving the correct values
        public void ResizeIntArray(bool _remove, int _index)
        {

            List<int> _tmpInt = new List<int>();


            for (int s = 0; s < layerPresetIndex.Length; s++)
            {
                _tmpInt.Add(layerPresetIndex[s]);
            }


            if (_remove)
            {
                _tmpInt.RemoveAt(_index);


                layerPresetIndex = new int[_tmpInt.Count];

                for (int n = 0; n < layerPresetIndex.Length; n++)
                {
                    layerPresetIndex[n] = _tmpInt[n];
                }

            }
            else
            {
                _tmpInt.Add(0);


                layerPresetIndex = new int[_tmpInt.Count];

                for (int m = 0; m < _tmpInt.Count; m++)
                {
                    layerPresetIndex[m] = _tmpInt[m];
                }
            }
        }

        public void ResizeStringArray(bool _remove, int _index)
        {
            List<string> _tmpString = new List<string>();

            for (int i = 0; i < blockLayerPresets.Length; i++)
            {
                _tmpString.Add(blockLayerPresets[i]);
            }

            if (_remove)
            {
                _tmpString.RemoveAt(_index);

                blockLayerPresets = new string[_tmpString.Count];

                for (int m = 0; m < blockLayerPresets.Length; m++)
                {
                    blockLayerPresets[m] = _tmpString[m];
                }
            }
            else
            {
                _tmpString.Add("Preset: " + blockLayerPresets.Length.ToString());


                blockLayerPresets = new string[_tmpString.Count];
                for (int a = 0; a < _tmpString.Count; a++)
                {
                    blockLayerPresets[a] = "Preset: " + a;
                }
            }
        }
        #endregion

        //--------------------
        //MAP MODIFICATION
        //--------------------
        #region mapmodifications

        /// <summary>
        /// Add new tiles to the given position in a certain range
        /// set heighDependent = true if you want to make sure to add tiles only on the layer with the same height.
        /// set rebuild = true if you want to rebuild the map after modifying it.
        /// set optimize = true if you wish to optimize the map before building it.(makes sure there are no strange build behaviours)
        /// </summary>
        /// <param name="_position"></param>
        /// <param name="_range"></param>
        /// <param name="_heightDependent"></param>
        /// <param name="_rebuild"></param>
        /// <param name="_optimize"></param>
        public void AddTiles(Vector3 _position, int _range, bool _heightDependent, bool _rebuild, bool _optimize)
        {
            for (int l = 0; l < layerCount; l++)
            {
                Vector3 _gPAbs = new Vector3(Mathf.Round(_position.x) - 1, Mathf.Round(_position.y), Mathf.Round(_position.z) - 1);

                int _layerHeightIndex = l;

                //if height dependant is true, set layer index according to the transform height of the object
                if (_heightDependent)
                {
                    _layerHeightIndex = (int)_gPAbs.y;
                }

                if (_layerHeightIndex < layerCount && _layerHeightIndex >= 0)
                {
                    for (int rY = 0; rY < _range; rY++)
                    {
                        for (int rX = 0; rX < _range; rX++)
                        {
                            if (!brushx2)
                            {
                                worldMap[_layerHeightIndex].map[(int)_gPAbs.x, (int)_gPAbs.z] = invert;
                            }
                            else
                            {
                                if (_gPAbs.x + rX < 0)
                                {
                                    _gPAbs.x = -1;
                                }
                                if (_gPAbs.z + rY < 0)
                                {
                                    _gPAbs.z = -1;
                                }

                                if (_gPAbs.x + rX >= 0 && _gPAbs.z + rY >= 0 && _gPAbs.x + rX < worldMap[_layerHeightIndex].map.GetLength(0) && _gPAbs.z + rY < worldMap[_layerHeightIndex].map.GetLength(1))
                                {
                                    worldMap[_layerHeightIndex].map[(int)_gPAbs.x + rX, (int)_gPAbs.z + rY] = invert;
                                }

                                if (_gPAbs.x + 1 + rX < worldMap[_layerHeightIndex].map.GetLength(0) && _gPAbs.z + rY < worldMap[_layerHeightIndex].map.GetLength(1) && _gPAbs.z + rY >= 0 && _gPAbs.x + rX >= 0)
                                {
                                    worldMap[_layerHeightIndex].map[(int)_gPAbs.x + 1 + rX, (int)_gPAbs.z + rY] = invert;
                                }

                                if (_gPAbs.x + rX < worldMap[_layerHeightIndex].map.GetLength(0) && _gPAbs.z + 1 + rY < worldMap[_layerHeightIndex].map.GetLength(1) && _gPAbs.x + rX >= 0 && _gPAbs.z + rY >= 0)
                                {
                                    worldMap[_layerHeightIndex].map[(int)_gPAbs.x + rX, (int)_gPAbs.z + 1 + rY] = invert;
                                }

                                if (_gPAbs.x + 1 + rX < worldMap[_layerHeightIndex].map.GetLength(0) && _gPAbs.z + 1 + rY < worldMap[_layerHeightIndex].map.GetLength(1) && _gPAbs.x + rX >= 0 && _gPAbs.z + rY >= 0)
                                {
                                    worldMap[_layerHeightIndex].map[(int)_gPAbs.x + 1 + rX, (int)_gPAbs.z + 1 + rY] = invert;
                                }
                            }
                        }
                    }
                }

            }

            if (_rebuild)
            {
                //this is a custom optimization loop because we are using a radius 
                //instead of just a single point position             
                if (_optimize)
                {
                    for (int l = 0; l < layerCount; l++)
                    {
                        for (int rY = -_range; rY < _range; rY++)
                        {
                            for (int rX = -_range; rX < _range; rX++)
                            {
                                OptimizePassPartial(l, (int)_position.x + rX, (int)_position.z + rY);
                            }
                        }
                    }
                }

                BuildMapPartial(false, false, 0, 0);
            }

        }

        /// <summary>
        /// Remove tiles from the given position in a certain range
        /// set heighDependent = true if you want to make sure to remove tiles only on the layer with the same height.
        /// set rebuild = true if you want to rebuild the map after modifying it.
        /// set optimize = true if you wish to optimize the map before building it.(makes sure there are no strange build behaviours)
        /// </summary>
        /// <param name="_position"></param>
        /// <param name="_range"></param>
        /// <param name="_heightDependent"></param>
        /// <param name="_rebuild"></param>
        /// <param name="_optimize"></param>
        public void RemoveTiles(Vector3 _position, int _range, bool _heightDependent, bool _rebuild, bool _optimize)
        {
            for (int l = 0; l < layerCount; l++)
            {

                Vector3 _gPAbs = new Vector3(Mathf.Round(_position.x) - 1, Mathf.Round(_position.y), Mathf.Round(_position.z) - 1);

                int _layerHeightIndex = l;

                //if height dependent is true, set layer index according to the transform height of the object
                if (_heightDependent)
                {
                    _layerHeightIndex = (int)_gPAbs.y;
                }

                if (_layerHeightIndex < layerCount && _layerHeightIndex >= 0)
                {

                    for (int rY = 0; rY < _range; rY++)
                    {
                        for (int rX = 0; rX < _range; rX++)
                        {

                            if (!brushx2)
                            {
                                worldMap[_layerHeightIndex].map[(int)_gPAbs.x, (int)_gPAbs.z] = !invert;
                            }
                            else
                            {
                                //if x or z position of position is out of grid
                                //set a correct int value
                                if (_gPAbs.x + rX < 0)
                                {
                                    _gPAbs.x = -1;
                                }
                                if (_gPAbs.z + rY < 0)
                                {
                                    _gPAbs.z = -1;
                                }

                                if (_gPAbs.x + rX >= 0 && _gPAbs.z + rY >= 0 && _gPAbs.x + rX < worldMap[_layerHeightIndex].map.GetLength(0) && _gPAbs.z + rY < worldMap[_layerHeightIndex].map.GetLength(1))
                                {
                                    worldMap[_layerHeightIndex].map[(int)_gPAbs.x + rX, (int)_gPAbs.z + rY] = !invert;
                                }

                                if (_gPAbs.x + 1 + rX < worldMap[_layerHeightIndex].map.GetLength(0) && _gPAbs.z + rY < worldMap[_layerHeightIndex].map.GetLength(1) && _gPAbs.z + rY >= 0 && _gPAbs.x + rX >= 0)
                                {
                                    worldMap[_layerHeightIndex].map[(int)_gPAbs.x + 1 + rX, (int)_gPAbs.z + rY] = !invert;
                                }

                                if (_gPAbs.x + rX < worldMap[_layerHeightIndex].map.GetLength(0) && _gPAbs.z + 1 + rY < worldMap[_layerHeightIndex].map.GetLength(1) && _gPAbs.x + rX >= 0 && _gPAbs.z + rY >= 0)
                                {
                                    worldMap[_layerHeightIndex].map[(int)_gPAbs.x + rX, (int)_gPAbs.z + 1 + rY] = !invert;
                                }

                                if (_gPAbs.x + 1 + rX < worldMap[_layerHeightIndex].map.GetLength(0) && _gPAbs.z + 1 + rY < worldMap[_layerHeightIndex].map.GetLength(1) && _gPAbs.x + rX >= 0 && _gPAbs.z + rY >= 0)
                                {
                                    worldMap[_layerHeightIndex].map[(int)_gPAbs.x + 1 + rX, (int)_gPAbs.z + 1 + rY] = !invert;
                                }
                            }
                        }
                    }
                }
            }

            if (_rebuild)
            {
                //this is a custom optimization loop because we are using a radius 
                //instead of just a single point position             
                if (_optimize)
                {
                    for (int l = 0; l < layerCount; l++)
                    {
                        for (int rY = -_range; rY < _range; rY++)
                        {
                            for (int rX = -_range; rX < _range; rX++)
                            {
                                OptimizePassPartial(l, (int)_position.x + rX, (int)_position.z + rY);
                            }
                        }
                    }
                }

                BuildMapPartial(false, false, 0, 0);
            }
        }

        #endregion

        //--------------------
        // DRAW GRID AND BRUSH
        //--------------------
        #region scenegui
        void OnDrawGizmosSelected()
        {
            if (!showGridAlways)
            {
                DrawGrid();
                DrawBrush();
            }

#if UNITY_EDITOR
            if (PrefabUtility.GetPrefabType(this.gameObject) != PrefabType.DisconnectedPrefabInstance)
            {
                PrefabUtility.DisconnectPrefabInstance(this.gameObject);
                
                if (presets.Count == 0)
                {
                    presets.Add(new Presets());
                    layerPresetIndex = new int[0];
                    blockLayerPresets = new string[0];

                    ResizeStringArray(false, 0);
                    ResizeIntArray(false, 0);
                    
                }
            }          
#endif
        }

        void OnDrawGizmos()
        {
            if (showGridAlways)
            {
                DrawGrid();
                DrawBrush();
            }
        }

        void DrawGrid()
        {

            if (!showGrid)
                return;
            if (worldMap.Count < 1 || presets.Count < 1)
                return;
            //Load back multidimensional array from single dim array
            if (worldMap[0].map == null)
            {
                //Debug.Log("load one dim back");
                for (int l = 0; l < layerCount; l++)
                {
                    worldMap[l].map = new bool[width, height];
                    int _index = 0;

                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            worldMap[l].map[x, y] = worldMap[l].mapSingle[_index];
                            _index++;
                        }
                    }
                }
            }

            //if map length is zero load mapSingle array to multidim array
            if (worldMap[0].map.Length == 0)
            {
                LoadBack();
            }

            Vector3 pos = new Vector3(transform.position.x, (transform.position.y + globalScale + (mapIndex * globalScale) + presets[layerPresetIndex[mapIndex]].tiles[0].blockOffset.y), transform.position.z);

            //draw grid
            int _step1 = 0;
            
            for (float z = 0; z <= (float)height * globalScale; z += 1 * globalScale)
            {
                if (_step1 == 0)
                {
                    Gizmos.color = Color.white;
                    _step1 = 1;
                }
                else
                {
                    Gizmos.color = Color.grey;
                    _step1 = 0;
                }
                Gizmos.DrawLine(new Vector3(pos.x, pos.y, pos.z + z),
                    new Vector3(pos.x + width * globalScale, pos.y, pos.z + z));
            }

            int _step2 = 0;
            for (float x = 0; x <= (float)width * globalScale; x += 1 * globalScale)
            {
                if (_step2 == 0)
                {
                    Gizmos.color = Color.white;
                    _step2 = 1;
                }
                else
                {
                    Gizmos.color = Color.grey;
                    _step2 = 0;
                }

                Gizmos.DrawLine(new Vector3(pos.x + x, pos.y, pos.z),
                    new Vector3(pos.x + x, pos.y, pos.z + height * globalScale));//
            }


            //draw cells
            if (worldMap.Count > 0)
            {
                for (int y = 0; y < worldMap[mapIndex].map.GetLength(1); y++)
                {
                    for (int x = 0; x < worldMap[mapIndex].map.GetLength(0); x++)
                    {
                        if (!invert)
                        {
                            //draw block cells
                            if (!worldMap[mapIndex].map[x, y])
                            {
                                Gizmos.color = cellColor;
                                Gizmos.DrawCube(new Vector3((pos.x + (x  + 0.5f) * globalScale), pos.y, (pos.z + ( y  + 0.5f ) * globalScale)), new Vector3(1 * globalScale, 0.05f, 1 * globalScale));
                                //Gizmos.DrawCube()
                            }

                            //draw floor
                            if (worldMap[mapIndex].map[x, y] && createFloor)
                            {
                                Gizmos.color = floorCellColor;
                                Gizmos.DrawCube(new Vector3((pos.x + (x + 0.5f) * globalScale ) , pos.y, (pos.z + (y + 0.5f ) * globalScale ) ), new Vector3(1 * globalScale, 0.05f, 1 * globalScale));
                            }
                        }
                        else
                        {
                            if (worldMap[mapIndex].map[x, y])
                            {
                                Gizmos.color = cellColor;
                                Gizmos.DrawCube(new Vector3((pos.x + (x + 0.5f) * globalScale), pos.y, (pos.z + (y + 0.5f )* globalScale )), new Vector3(1 * globalScale, 0.05f, 1 * globalScale));

                            }
                            //draw floor
                            if (!worldMap[mapIndex].map[x, y] && createFloor)
                            {
                                Gizmos.color = floorCellColor;
                                Gizmos.DrawCube(new Vector3((pos.x + (x + 0.5f ) * globalScale), pos.y, (pos.z + (y + 0.5f ) * globalScale )), new Vector3(1 * globalScale, 0.05f, 1 * globalScale));
                            }
                        }
                    }
                }
            }

            //draw mask cells
            if (worldMap[mapIndex].paintMask)
            {
                //bool[,] _maskMap = iMasks[worldMap[mapIndex].selectedMask].ApplyMask(worldMap[mapIndex].map, this);
                for (int ym = 0; ym < worldMap[mapIndex].maskMap.GetLength(1); ym++)
                {
                    for (int xm = 0; xm < worldMap[mapIndex].maskMap.GetLength(0); xm++)
                    {
                        if (!invert)
                        {
                            //draw block cells
                            if (!worldMap[mapIndex].maskMap[xm, ym]) // && worldMap[mapIndex].maskMap[xm, ym] != !worldMap[mapIndex].map[xm, ym])
                            {
                                Gizmos.color = maskColor;
                                Gizmos.DrawCube(new Vector3((pos.x + (xm + 0.5f) * globalScale), (pos.y + 0.1f), (pos.z + (ym + 0.5f) * globalScale)), new Vector3(1 * globalScale, 0.05f, 1 * globalScale));
                            }
                            else
                            {
                                Gizmos.color = new Color(0f / 255f, 0f / 255f, 0f / 255f, 150f / 255f);
                                Gizmos.DrawCube(new Vector3((pos.x + (xm + 0.5f) * globalScale), (pos.y + 0.1f), (pos.z + (ym + 0.5f) * globalScale)), new Vector3(1 * globalScale, 0.05f, 1 * globalScale));
                            }
                        }
                        else
                        {
                            //draw block cells
                            if (worldMap[mapIndex].maskMap[xm, ym] && worldMap[mapIndex].maskMap[xm, ym] != !worldMap[mapIndex].map[xm, ym])
                            {
                                Gizmos.color = maskColor;
                                Gizmos.DrawCube(new Vector3((pos.x + (xm + 0.5f) * globalScale), (pos.y + 0.1f), (pos.z + (ym + 0.5f) * globalScale)), new Vector3(1 * globalScale, 0.05f, 1 * globalScale));
                            }
                            else
                            {
                                Gizmos.color = new Color(0f / 255f, 0f / 255f, 0f / 255f, 150f / 255f);
                                Gizmos.DrawCube(new Vector3((pos.x + (xm + 0.5f) * globalScale), (pos.y + 0.1f), (pos.z + (ym + 0.5f) * globalScale)), new Vector3(1 * globalScale, 0.05f, 1 * globalScale));
                            }
                        }
                    }
                }
            }

        }


        void DrawBrush()
        {

            if (worldMap.Count < 1)
                return;

            if (mouseOverGrid)
            {
                Gizmos.color = brushColor;

                if (!brushx2)
                {
                    Vector3 _brPos = new Vector3((Mathf.Floor(mouseWorldPosition.x / 1) + 0.5f) * globalScale, (transform.position.y + 1.05f + mapIndex) + globalScale, (Mathf.Floor(mouseWorldPosition.z / globalScale) + 0.5f) * globalScale);
                    Gizmos.DrawCube(_brPos, new Vector3(1, 0.05f, 1));
                }
                else
                {

                    for (int y = 0; y < brushSize; y ++)
                    {
                        for (int x = 0; x < brushSize; x++)
                        {
                           
                            Vector3 _pos = new Vector3((Mathf.Floor(mouseWorldPosition.x / globalScale) + 0.5f + x) * globalScale, (transform.position.y + (1.05f * globalScale) + mapIndex) + presets[layerPresetIndex[mapIndex]].tiles[0].blockOffset.y, (Mathf.Floor(mouseWorldPosition.z / globalScale) + 0.5f + y) * globalScale);

                            if (_pos.x > 0 + transform.position.x && _pos.z > 0 + transform.position.z && _pos.x < transform.position.x + (width * globalScale) && _pos.z < transform.position.z + (height * globalScale))
                            {
                                Gizmos.DrawCube(_pos, new Vector3(1 * globalScale, 0.05f, 1 * globalScale));
                            }   
                        }
                    }

                    //Vector3 _pos1 = new Vector3((Mathf.Floor(mouseWorldPosition.x / globalScale) + 0.5f) * globalScale, (transform.position.y + (1.05f * globalScale) + mapIndex) , (Mathf.Floor(mouseWorldPosition.z / globalScale) + 0.5f) * globalScale);
                    //Vector3 _pos2 = new Vector3((Mathf.Floor(mouseWorldPosition.x / globalScale) + 0.5f + 1) * globalScale, (transform.position.y + (1.05f * globalScale) + mapIndex), (Mathf.Floor(mouseWorldPosition.z / globalScale) + 0.5f) * globalScale);
                    //Vector3 _pos3 = new Vector3((Mathf.Floor(mouseWorldPosition.x / globalScale) + 0.5f) * globalScale, (transform.position.y + (1.05f * globalScale) + mapIndex), (Mathf.Floor(mouseWorldPosition.z / globalScale) + 0.5f + 1) * globalScale);
                    //Vector3 _pos4 = new Vector3((Mathf.Floor(mouseWorldPosition.x / globalScale) + 0.5f + 1) * globalScale, (transform.position.y + (1.05f * globalScale) + mapIndex), (Mathf.Floor(mouseWorldPosition.z / globalScale) + 0.5f + 1) * globalScale);



                    //if (_pos1.x > 0 + transform.position.x && _pos1.z > 0 + transform.position.z)
                    //{
                    //    Gizmos.DrawCube(_pos1, new Vector3(1 * globalScale, 0.05f, 1 * globalScale));
                    //}

                    //if (_pos2.z > 0 + transform.position.z && _pos2.x < + transform.position.x + (width * globalScale))
                    //{
                    //    Gizmos.DrawCube(_pos2, new Vector3(1 * globalScale, 0.05f, 1 * globalScale));
                    //}

                    //if (_pos3.x > 0 + transform.position.x && _pos3.z > 0 + transform.position.z && _pos3.z < + transform.position.z + (height * globalScale))
                    //{
                    //    Gizmos.DrawCube(_pos3, new Vector3(1 * globalScale, 0.05f, 1 * globalScale));
                    //}

                    //if (_pos4.z > 0 + transform.position.z && _pos4.z < transform.position.z + (height * globalScale) && _pos4.x < transform.position.x + (width * globalScale))
                    //{
                    //    Gizmos.DrawCube(_pos4, new Vector3(1 * globalScale, 0.05f, 1 * globalScale));
                    //}
                }
            }
        }

        #endregion
    }  
}