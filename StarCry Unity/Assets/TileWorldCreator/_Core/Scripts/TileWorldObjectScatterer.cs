﻿/* TILE WORLD CREATOR
 * Copyright (c) 2015 doorfortyfour OG
 * 
 * Create awesome tile worlds in seconds.
 *
 *
 * Documentation: http://tileworldcreator.doofortyfour.com
 * Like us on Facebook: http://www.facebook.com/doorfortyfour2013
 * Web: http://www.doorfortyfour.com
 * Contact: mail@doorfortyfour.com Contact us for help, bugs or 
 * share your awesome work you've made with TileWorldCreator
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TileWorld
{

	[RequireComponent (typeof (TileWorldCreator))]
	public class TileWorldObjectScatterer : MonoBehaviour {
	
		//REFERENCE
		public TileWorldCreator creator;
		
		
		//set the correct position
		// 0 = topleft
		// 1 = topCenter
		// 2 = topRight
		// 3 = middleLeft
		// 4 = middleCenter
		// 5 = middleRight
		// 6 = bottomLeft
		// 7 = bottomCenter
		// 8 = bottomRight
		public enum spawnPosition{
			topLeft, 
			topCenter, 
			topRight,
			middleLeft, 
			middleCenter,
			middleRight, 
			bottomLeft, 
			bottomCenter, 
			bottomRight
		};
		
		public int startPositionSelected = 0;
		public int endPositionSelected = 8;
		
		public GameObject startPositionGo;
		public GameObject endPositionGo;
		
		public Vector3 startPositionOffset = new Vector3(0.5f, 0.5f, 0.5f);
		public Vector3 endPositionOffset = new Vector3(0.5f, 0.5f, 0.5f);
		
		public int placeTileTypeSelect = 0; //1 = floor 0 = block
		
		
		public List<GameObject> objects = new List<GameObject>();
		public List<float> weights = new List<float>();
		public List<bool> showObjects = new List<bool>();
		//public List<GameObject> decals = new List<GameObject>();
		public List<Vector3> offsets = new List<Vector3>();
		public List<int> placeTileSelect = new List<int>();

        
        public List<int> placeOnLayerSelected = new List<int>();

		public List<Vector3> rndRotation = new List<Vector3>();
		
		public List<Vector3> rndScalingMin = new List<Vector3>();
		public List<Vector3> rndScalingMax = new List<Vector3>();
		public List<bool> uniformScaling = new List<bool>();
		
		public List<bool> spawnChilds = new List<bool>();
		public List<float> spawnRadius = new List<float>();
		public List<int> childCount = new List<int>();
		
		public List<Vector3> rndRotationChilds = new List<Vector3>();
		
		public List<bool> uniformScalingChilds = new List<bool>();
		public List<Vector3> rndScalingChildsMin = new List<Vector3>();
		public List<Vector3> rndScalingChildsMax = new List<Vector3>();
		
		public GameObject worlContainerObjects = null;
		public List<GameObject> clusters = new List<GameObject>();
		
		public GameObject containerStartEnd = null;
		
      
        float _widthT = 0;
		float _heightT = 0;
		
		int _xOffset = 0;
		//int _yOffset = 0;
		
		public float progress = 0;
		
		//-----------------------------------------------------------------------
	
		public void Awake()
		{
			progress = 0;
		}
		
		//Method called by inspector editor
		//return int to enum
		public void PlaceStartEndObjectsEditor()
		{
			PlaceStartEndObjects(SelectedPositionToEnum(startPositionSelected), SelectedPositionToEnum(endPositionSelected));
		}

        /// <summary>
        /// Place start and end objects on the map at the given spawnPosition
        /// </summary>
        /// <param name="_selectedStartPosition"></param>
        /// <param name="_selectedEndPosition"></param>
		public void PlaceStartEndObjects(spawnPosition _selectedStartPosition, spawnPosition _selectedEndPosition)
		{
			startPositionSelected = SelectedPositionToInt(_selectedStartPosition);
			endPositionSelected = SelectedPositionToInt(_selectedEndPosition);
			
			if (containerStartEnd != null)
			{
				DestroyImmediate(containerStartEnd);
			}
			
			containerStartEnd = new GameObject();
			containerStartEnd.name = creator.worldName + "_StartEnd";
			
			//instantiate start position gameobject
			if (startPositionGo != null)
			{
				var _startPosGO = Instantiate(startPositionGo, GetSpawnPosition(startPositionSelected, startPositionOffset), Quaternion.identity) as GameObject;
				
				//get other position if theres no possibility to place object
				if (_startPosGO.transform.position == new Vector3(-1, -1, -1))
				{
					int _rowIndexStart = 0;
					Vector3 _nPos = new Vector3(-1,-1,-1);
					
					if (startPositionSelected < 3)
					{
						_rowIndexStart = 0;
					}
					else if (startPositionSelected > 2 && startPositionSelected < 6)
					{
						_rowIndexStart = 3;
					}
					else if (startPositionSelected > 5 && startPositionSelected < 9)
					{
						_rowIndexStart = 6;
					}
					
					//first we check if theres a possible place in the user selected row (top, middle, bottom)
					for (int p = 0; p < 3; p ++)
					{
						if (p - _rowIndexStart < 3)
						{
							_nPos = GetSpawnPosition(p + _rowIndexStart, startPositionOffset);
							if(_nPos != new Vector3(-1, -1, -1))
							{
								_startPosGO.transform.position = _nPos;
								break;
							}
						}
					}
					
					//if theres nothing in the user selected row try different row
					if (_nPos == new Vector3(-1,-1,-1))
					{
						
						for (int r = 0; r < 3; r ++)
						{
							if (_rowIndexStart == 0 && _nPos == new Vector3(-1,-1,-1))
							{
								_rowIndexStart = 3;
								
								for (int p = 0; p < 3; p ++)
								{
									if (p - _rowIndexStart < 3)
									{
										_nPos = GetSpawnPosition(p + _rowIndexStart, startPositionOffset);
										if(_nPos != new Vector3(-1, -1, -1))
										{
											
											_startPosGO.transform.position = _nPos;
											break;
										}
									}
								}
								
							}
							else if (_rowIndexStart == 3 && _nPos == new Vector3(-1,-1,-1))
							{
								_rowIndexStart = 6;
								
								for (int p = 0; p < 3; p ++)
								{
									if (p - _rowIndexStart < 3)
									{
										_nPos = GetSpawnPosition(p + _rowIndexStart, startPositionOffset);
										if(_nPos != new Vector3(-1, -1, -1))
										{
											
											_startPosGO.transform.position = _nPos;
											break;
										}
									}
								}
							}
							else if (_rowIndexStart == 6 && _nPos == new Vector3(-1,-1,-1))
							{
								_rowIndexStart = 0;
								
								for (int p = 0; p < 3; p ++)
								{
									if (p - _rowIndexStart < 3)
									{
										_nPos = GetSpawnPosition(p + _rowIndexStart, startPositionOffset);
										if(_nPos != new Vector3(-1, -1, -1))
										{
											
											_startPosGO.transform.position = _nPos;
											break;
										}
									}
								}
							}
						}
					}
					
					//for (int p = 0; p < 9; p ++)
					//{
					//	if (startPositionSelected + p < 9)
					//	{
							
					//		Vector3 _nPos = GetSpawnPosition(startPositionSelected + p, startPositionOffset);
					//		if(_nPos != new Vector3(-1, -1, -1))
					//		{
					//			_startPosGO.transform.position = _nPos;
					//			break;
					//		}
					//	}
					//	else
					//	{
					//		Vector3 _nPos = GetSpawnPosition(p, startPositionOffset);
					//		if(_nPos != new Vector3(-1, -1, -1))
					//		{
					//			_startPosGO.transform.position = _nPos;
					//			break;	
					//		}
					//	}
					//}	
				}
				
                //add prefab position offset
                _startPosGO.transform.position += this.transform.position;

				_startPosGO.gameObject.transform.parent = containerStartEnd.transform;	
			}
			//instantiate end position gameobject
			if (endPositionGo != null)
			{
				var _endPosGO = Instantiate(endPositionGo, GetSpawnPosition(endPositionSelected, endPositionOffset), Quaternion.identity) as GameObject;
				
				//get other position if theres no possibility to place object
				if (_endPosGO.transform.position == new Vector3(-1, -1, -1))
				{
					
					int _rowIndexEnd = 0;
					Vector3 _ePos = new Vector3(-1,-1,-1);
					
					if (endPositionSelected < 3)
					{
						_rowIndexEnd = 0;
					}
					else if (endPositionSelected > 2 && endPositionSelected < 6)
					{
						_rowIndexEnd = 3;
					}
					else if (endPositionSelected > 5 && endPositionSelected < 9)
					{
						_rowIndexEnd = 6;
					}
					
					//first we check if theres a possible place in the user selected row (top, middle, bottom)
					for (int p = 0; p < 3; p ++)
					{
						if (p - _rowIndexEnd < 3)
						{
							_ePos = GetSpawnPosition(p + _rowIndexEnd, endPositionOffset);
							if(_ePos != new Vector3(-1, -1, -1))
							{
								_endPosGO.transform.position = _ePos;
								break;
							}
						}
					}
					
					//if theres nothing in the user selected row try different row
					if (_ePos == new Vector3(-1,-1,-1))
					{
						
						for (int r = 0; r < 3; r ++)
						{
							if (_rowIndexEnd == 0 && _ePos == new Vector3(-1,-1,-1))
							{
								_rowIndexEnd = 6;
								
								for (int p = 0; p < 3; p ++)
								{
									if (p - _rowIndexEnd < 3)
									{
										_ePos = GetSpawnPosition(p + _rowIndexEnd, endPositionOffset);
										if(_ePos != new Vector3(-1, -1, -1))
										{
											
											_endPosGO.transform.position = _ePos;
											break;
										}
									}
								}
								
							}
							else if (_rowIndexEnd == 3 && _ePos == new Vector3(-1,-1,-1))
							{
								_rowIndexEnd = 0;
								
								for (int p = 0; p < 3; p ++)
								{
									if (p - _rowIndexEnd < 3)
									{
										_ePos = GetSpawnPosition(p + _rowIndexEnd, endPositionOffset);
										if(_ePos != new Vector3(-1, -1, -1))
										{
											
											_endPosGO.transform.position = _ePos;
											break;
										}
									}
								}
							}
							else if (_rowIndexEnd == 6 && _ePos == new Vector3(-1,-1,-1))
							{
								_rowIndexEnd = 3;
								
								for (int p = 0; p < 3; p ++)
								{
									if (p - _rowIndexEnd < 3)
									{
										_ePos = GetSpawnPosition(p + _rowIndexEnd, endPositionOffset);
										if(_ePos != new Vector3(-1, -1, -1))
										{
											
											_endPosGO.transform.position = _ePos;
											break;
										}
									}
								}
							}
						}
					}
					
					//for (int p = 0; p < 9; p ++)
					//{
					//	if (endPositionSelected + p < 9)
					//	{
							
					//		Vector3 _nPos = GetSpawnPosition(endPositionSelected + p, endPositionOffset);
					//		if(_nPos != new Vector3(-1, -1, -1))
					//		{
					//			_endPosGO.transform.position = _nPos;
					//			break;
					//		}
					//	}
					//	else
					//	{
					//		Vector3 _nPos = GetSpawnPosition(p, endPositionOffset);
					//		if(_nPos != new Vector3(-1, -1, -1))
					//		{
					//			_endPosGO.transform.position = _nPos;
					//			break;	
					//		}
					//	}
					//}	
				}

                //add prefab position offset
                _endPosGO.transform.position += this.transform.position;

				_endPosGO.gameObject.transform.parent = containerStartEnd.transform;	
			}
	
		}
		
		
		//returns position for spawning objects according to selected position from the user
		Vector3 GetSpawnPosition(int _selectedPosition, Vector3 _posOffset)
		{
			bool _invert = false;
				
			if (placeTileTypeSelect == 0)
			{
				_invert = creator.invert;
			}
			else
			{
				_invert = !creator.invert;
			}	
			
			creator = GetComponent<TileWorldCreator>();
			
		
			//get a third of width and height 
			_widthT = Mathf.Ceil(creator.width / 3f);
			_heightT = Mathf.Ceil(creator.height / 3f);
			
			int _count = 0;
			_xOffset = 0;
			//_yOffset = 0;
			
			int _yOffsetReversed = 0;
			
			//set offset according to selected position
			switch (_selectedPosition)
			{
				case 0: 
					_xOffset = 0;
					//_yOffset = 0;
					_yOffsetReversed = 2;
					break;
				case 1:
					_xOffset = 1;
					//_yOffset = 0;
					_yOffsetReversed = 2;
					break;
				case 2:
					_xOffset = 2;
					//_yOffset = 0;
					_yOffsetReversed = 2;
					break;
				case 3:
					_xOffset = 0;
					//_yOffset = 1;
					_yOffsetReversed = 1;
					break;
				case 4:
					_xOffset = 1;
					//_yOffset = 1;
					_yOffsetReversed = 1;
					break;
				case 5:
					_xOffset = 2;
					//_yOffset = 1;
					_yOffsetReversed = 1;
					break;
				case 6:
					_xOffset = 0;
					//_yOffset = 2;
					_yOffsetReversed = 0;
					break;
				case 7:
					_xOffset = 1;
					//_yOffset = 2;
					_yOffsetReversed = 0;
					break;
				case 8:
					_xOffset = 2;
					//_yOffset = 2;
					_yOffsetReversed = 0;
					break;
			}

			
			bool[,] _tmpMap = new bool[(int)_widthT, (int)_heightT];
			
			//a new temp map array with one third of the grid size
			//store all values in this new array and count how many blocks there are.
			for (int y = 0; y < _heightT; y ++)
			{
				for (int x = 0; x < _widthT; x ++)
				{
					if (x  + ((int)_widthT * _xOffset) < creator.width && y + ((int)_heightT * _yOffsetReversed) < creator.height)
					{
						
						_tmpMap[x, y] = (creator.worldMap[0].map[x + ((int)_widthT * _xOffset), y + ((int)_heightT * _yOffsetReversed)]);
						
						if (creator.worldMap[0].map[x + ((int)_widthT * _xOffset), y + ((int)_heightT * _yOffsetReversed)] == creator.invert)
						{
							_count ++;
						}
					}
				}
				
			}
			
			//create a new vector 3 array for storing all possible spawn positions
			//we will then choose randomly from it.
			List<Vector3> _SpawnPositions = new List<Vector3>();
			int _ccn = 0;
			
			if (_count >= 4)
			{
				for (int a = 0; a < _heightT; a ++)
				{
					for (int b = 0; b < _widthT; b ++)
					{
						//_ccn = TileWorldNeighbourCounter.CountCrossNeighbours(_tmpMap, b, a, 1, false, _invert); 
						_ccn = TileWorldNeighbourCounter.CountDiagonalNeighbours(_tmpMap, b, a, 1, false, !_invert);

                         //Debug.Log( a + ((int)_heightT * _yOffset));
                        if (_ccn >= 4)
                        {
                            //Debug.Log(_ccn);
                            _SpawnPositions.Add(new Vector3((b + ((int)_widthT * _xOffset) + _posOffset.x) * creator.globalScale, (_posOffset.y) * creator.globalScale, (a + ((int)_heightT * _yOffsetReversed) + _posOffset.z)* creator.globalScale) );
                        }
					}
				}
							
			}
			
			//return position
			Vector3 _pos = new Vector3(-1, -1, -1);
			
			if (_SpawnPositions.Count > 0)
			{
				_pos = _SpawnPositions[Random.Range(0, _SpawnPositions.Count - 1)];
			}
			
			return _pos;
			
		}
		
		
        /// <summary>
        /// Instantiates Random objects including childs
        /// </summary>
        /// <param name="_merge"></param>
        /// <param name="_multiMaterialMesh"></param>
        /// <param name="_addCollider"></param>
		public void PlaceRandomObjects(bool _merge, bool _multiMaterialMesh, bool _addCollider)
		{
			progress = 0; 
			StartCoroutine(PlaceRandomObjectsIE(_merge,_multiMaterialMesh, _addCollider));
			
		}

        public IEnumerator PlaceRandomObjectsIE(bool _merge, bool _multiMaterialMesh, bool _addCollider)
		{
            //int _ccn = 0;
			bool _invert = false;
			
			if (worlContainerObjects != null)
			{
				DestroyImmediate(worlContainerObjects);
			}
			
			worlContainerObjects = new GameObject();
            worlContainerObjects.name = creator.worldName + "_Objects";
			
			
			for (int o = 0; o < clusters.Count; o ++)
			{
				Destroy(clusters[o]);
			}
			
			clusters = new List<GameObject>();
			
			
			for (int i = 0; i < objects.Count; i ++)
			{
				
				if (placeTileSelect[i] == 0)
				{
					_invert = true;
				}
				else
				{
					_invert = false;
				}

                for (int l = 0; l < creator.layerCount; l++)
                {
                    //Build world in clusters
                    //Divide width and height by clusterSize
                    //Build each cluster and parent objects in a new gameobject
                    float _clusterSize = (float)creator.clusterSize;

                    float xSizeF = Mathf.Floor(creator.width / ((float)creator.width / _clusterSize));
                    float ySizeF = Mathf.Floor(creator.height / ((float)creator.height / _clusterSize));

                    int xSize = (int)xSizeF;
                    int ySize = (int)ySizeF;

                    int xStep = 0;
                    int yStep = 0;

                    int index = 0;

                    GameObject _container = null;

                    float _progressStep = (100f / (Mathf.Ceil((float)creator.width / _clusterSize) * Mathf.Ceil((float)creator.height / _clusterSize))) / objects.Count;

                    for (int s = 0; s < Mathf.Ceil((float)creator.width / _clusterSize) * Mathf.Ceil((float)creator.height / _clusterSize); s++)
                    {
                        if (clusters.Count != Mathf.Ceil((float)creator.width / _clusterSize) * Mathf.Ceil((float)creator.height / _clusterSize))
                        {

                            _container = new GameObject();
                            _container.name = "cluster_" + s.ToString();
                            clusters.Add(_container);

                        }
                        //for (int y = 0; y < grid.height; y ++)
                        //{
                        //	for (int x = 0; x < grid.width; x ++)
                        //	{
                        for (int y = 0 + (ySize * yStep); y < ySize + (ySize * yStep); y++)
                        {
                            for (int x = 0 + (xSize * xStep); x < xSize + (xSize * xStep); x++)
                            {
                                float _rnd = Random.Range(0.0f, 1.0f);

                                //_ccn = TileWorldNeighbourCounter.CountDiagonalNeighbours(creator.worldMap[0].map, x, y, 1, false, creator.invert);
                                int _ccn2 = TileWorldNeighbourCounter.CountInnerTerrainBlocks(creator.worldMap[l].map, x, y, 1, creator.invert);
                                
                                //place on block
                                if (_invert && y < creator.height && y >= 0 && x < creator.width && x >= 0)
                                {
                                    //place objects on selected layer
                                    if (l == placeOnLayerSelected[i])
                                    {
                                        //if highest layer do not check for layer above
                                        if (l == creator.layerCount - 1)
                                        {
                                            if ((_ccn2 == 8)) // && creator.worldMap[l + 1].map[x, y] != creator.invert))
                                            {
                                                if (weights[i] > _rnd && objects[i] != null)
                                                {
                                                    InstantiateObjects(x, y, i, l, clusters[s], _invert);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (_ccn2 == 8 && creator.worldMap[l + 1].map[x, y] != creator.invert)
                                            {
                                                if (weights[i] > _rnd && objects[i] != null)
                                                {
                                                    InstantiateObjects(x, y, i, l, clusters[s], _invert);
                                                }
                                            }
                                        }
                                    }
                                }
                                //place on floor
                                else if (y < creator.height && y >= 0 && x < creator.width && x >= 0)
                                {
                                    if (l == 0)
                                    {
                                        if (_ccn2 == 0 && creator.worldMap[0].map[x, y] == !creator.invert)// && creator.worldMap[1].map[x ,y] != !creator.invert)
                                        {
                                            if (weights[i] > _rnd && objects[i] != null)
                                            {
                                                InstantiateObjects(x, y, i, 0, clusters[s], _invert);
                                            }
                                        }
                                    }
                                }


                            } //x
                        } //y

                        if (index >= (creator.width / _clusterSize) - 1)
                        {
                            yStep++;
                            xStep = 0;
                            index = 0;
                        }
                        else
                        {
                            xStep++;
                            index++;
                        }



                        clusters[s].transform.parent = worlContainerObjects.transform;

                        progress += _progressStep;

                        yield return null;
                    }	// s	
                } // l
			}
			
			
			if (_merge)
			{			
				for (int c = 0; c < clusters.Count; c ++)
				{
					TileWorldCombiner.CombineMesh(clusters[c], _addCollider);

                    if (_multiMaterialMesh)
                    {
                        yield return new WaitForEndOfFrame();

                        TileWorldCombiner.CombineMulitMaterialMesh(clusters[c], _addCollider);
                    }
				}	
			}
			
			progress = 100;	
		}
		
		public void PlaceRandomObjectsEditor(bool _merge, bool _multiMaterialMesh, bool _addCollider)
		{
			if (creator.worldMap.Count < 1)
				return;
			
            //int _ccn = 0;
			bool _invert = false;
			
			if (worlContainerObjects != null)
			{
				DestroyImmediate(worlContainerObjects);
			}
			
			worlContainerObjects = new GameObject();
			worlContainerObjects.name = creator.worldName + "_Objects";
			
			
			for (int o = 0; o < clusters.Count; o ++)
			{
				DestroyImmediate(clusters[o]);
			}
			
			clusters = new List<GameObject>();
			
			
			for (int i = 0; i < objects.Count; i ++)
			{
				
				if (placeTileSelect[i] == 0)
				{
					_invert = true;
				}
				else
				{
					_invert = false;
				}	

				for (int l = 0; l < creator.layerCount; l++)
                {
				    //Build world in clusters
				    //Divide width and height by clusterSize
				    //Build each cluster and parent objects in a new gameobject
				    float _clusterSize = (float)creator.clusterSize;
				
				    float xSizeF = Mathf.Floor(creator.width / ((float)creator.width / _clusterSize));
				    float ySizeF = Mathf.Floor(creator.height / ((float)creator.height / _clusterSize));
				
				    int xSize = (int)xSizeF;
				    int ySize = (int)ySizeF;
				
				    int xStep = 0;
				    int yStep = 0;
				
				    int index = 0;
				
				    GameObject _container = null;

                
                    for (int s = 0; s < Mathf.Ceil((float)creator.width / _clusterSize) * Mathf.Ceil((float)creator.height / _clusterSize); s++)
                    {
                        if (clusters.Count != Mathf.Ceil((float)creator.width / _clusterSize) * Mathf.Ceil((float)creator.height / _clusterSize))
                        {

                            _container = new GameObject();
                            _container.name = "cluster_" + s.ToString();
                            clusters.Add(_container);

                        }
                        for (int y = 0 + (ySize * yStep); y < ySize + (ySize * yStep); y++)
                        {
                            for (int x = 0 + (xSize * xStep); x < xSize + (xSize * xStep); x++)
                            {
                                float _rnd = Random.Range(0.0f, 1.0f);

                                //_ccn = TileWorldNeighbourCounter.CountDiagonalNeighbours(creator.worldMap[0].map, x, y, 1, false, creator.invert);
                                int _ccn2 = TileWorldNeighbourCounter.CountInnerTerrainBlocks(creator.worldMap[l].map, x, y, 1, creator.invert);
                                
                                //place on terrain
                                if (_invert && y < creator.height && y >= 0 && x < creator.width && x >= 0)
                                {

                                        //place objects on selected layer
                                        if (l == placeOnLayerSelected[i])
                                        {           
                                            //if highest layer do not check for layer above
                                            if (l == creator.layerCount - 1)
                                            {
                                                if ((_ccn2 == 8)) // && creator.worldMap[l + 1].map[x, y] != creator.invert))
                                                {
                                                    if (weights[i] > _rnd && objects[i] != null)
                                                    {
                                                        InstantiateObjects(x, y, i, l, clusters[s], _invert);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                
                                                if (_ccn2 == 8 && creator.worldMap[l + 1].map[x, y] != creator.invert)
                                                {
                                                    if (weights[i] >= _rnd && objects[i] != null)
                                                    {
                                                        InstantiateObjects(x, y, i, l, clusters[s], _invert);
                                                    }
                                                }
                                            }
                                        }                                   
                                }
                                //place on water
                                else if (y < creator.height && y >= 0 && x < creator.width && x >= 0)
                                {
                                    if (l == 0)
                                    {
                                        if (_ccn2 == 0 && creator.worldMap[0].map[x, y] == !creator.invert)// && creator.worldMap[1].map[x ,y] != !creator.invert)
                                        {
                                            if (weights[i] > _rnd && objects[i] != null)
                                            {
                                                InstantiateObjects(x, y, i, 0, clusters[s], _invert);
                                            }
                                        }
                                    }

                                }
                            } //x
                        } //y

                        if (index >= (creator.width / _clusterSize) - 1)
                        {
                            yStep++;
                            xStep = 0;
                            index = 0;
                        }
                        else
                        {
                            xStep++;
                            index++;
                        }

                        //if (_merge)
                        //{
                        //	Transform[] _childs = _container.GetComponentsInChildren<Transform>();

                        //	if (_childs.Length-1 > 0)
                        //	{
                        //		TileWorldCombiner.CombineMesh(_container,_addCollider);
                        //	}	
                        //}

                        clusters[s].transform.parent = worlContainerObjects.transform;
                    }	// s
                } //l
			}
		}
		
		//instantiate the random object and instantiates childs if enabled
		void InstantiateObjects(int x, int y, int i, int _layer, GameObject _container, bool _invert)
		{
			float _rndScaleX = Random.Range(rndScalingMin[i].x, rndScalingMax[i].x);
			float _rndScaleY = Random.Range(rndScalingMin[i].y, rndScalingMax[i].y);
			float _rndScaleZ = Random.Range(rndScalingMin[i].z, rndScalingMax[i].z);
			
			float _rndRotX = Random.Range(0, rndRotation[i].x);
			float _rndRotY = Random.Range(0, rndRotation[i].y);
			float _rndRotZ = Random.Range(0, rndRotation[i].z);
			
			Quaternion _qt = Quaternion.Euler(new Vector3(_rndRotX, _rndRotY, _rndRotZ));

            Vector3 _pos = new Vector3((x + offsets[i].x) * creator.globalScale, (offsets[i].y + _layer) * creator.globalScale, (y + offsets[i].z) * creator.globalScale);

			var _inst =  Instantiate(objects[i].gameObject, _pos, _qt) as GameObject;
			_inst.transform.parent = _container.transform;
			
            //add offset position
            _inst.transform.position = new Vector3(transform.position.x + _inst.transform.position.x, transform.position.y + _inst.transform.position.y, transform.position.z + _inst.transform.position.z);

			if (uniformScaling[i])
			{
                _inst.transform.localScale = new Vector3(_rndScaleX * creator.globalScale, _rndScaleX * creator.globalScale, _rndScaleX * creator.globalScale);
			}
			else
			{
                _inst.transform.localScale = new Vector3(_rndScaleX * creator.globalScale, _rndScaleY * creator.globalScale, _rndScaleZ * creator.globalScale);
			}
			
			SpawnChilds(x, y, _pos, i, _layer, _invert, _container);
		}
		
		//Instantiate Child objects randomly in a given radius
		//and check if the spawning position is inside of a full block
		void SpawnChilds(int _x, int _y, Vector3 _pos, int _index, int _layer, bool _invert, GameObject _container)
		{
			if (spawnChilds[_index])
			{
				for (int i = 0; i < childCount[_index]; i ++)
				{
					
					float _rndRotX = Random.Range(0, rndRotationChilds[_index].x);
					float _rndRotY = Random.Range(0, rndRotationChilds[_index].y);
					float _rndRotZ = Random.Range(0, rndRotationChilds[_index].z);
					
					float _rndScaleX = Random.Range(rndScalingChildsMin[_index].x, rndScalingChildsMax[_index].x);
					float _rndScaleY = Random.Range(rndScalingChildsMin[_index].y, rndScalingChildsMax[_index].y);
					float _rndScaleZ = Random.Range(rndScalingChildsMin[_index].z, rndScalingChildsMax[_index].z);
					
					Quaternion _qt = Quaternion.Euler(new Vector3(_rndRotX, _rndRotY, _rndRotZ));
                    
					GameObject _inst = Instantiate (objects[_index].gameObject, Random.insideUnitSphere * spawnRadius[_index] + _pos, _qt) as GameObject;
					
                    //_inst.transform.position = new Vector3((transform.position.x + _inst.transform.position.x), (transform.position.y + offsets[_index].y) + _layer, (transform.position.z + _inst.transform.position.z));
                    //add offset position
                    _inst.transform.position = new Vector3((transform.position.x + _inst.transform.position.x), (transform.position.y + _pos.y), (transform.position.z + _inst.transform.position.z));
                   
					if (uniformScalingChilds[_index])
					{
						_inst.transform.localScale = new Vector3((_inst.transform.localScale.x * _rndScaleX) * creator.globalScale, (_inst.transform.localScale.y * _rndScaleX) * creator.globalScale, (_inst.transform.localScale.z * _rndScaleX) * creator.globalScale);
					}
					else
					{
						_inst.transform.localScale = new Vector3((_inst.transform.localScale.x * _rndScaleX) * creator.globalScale, (_inst.transform.localScale.y * _rndScaleY) * creator.globalScale, (_inst.transform.localScale.z * _rndScaleZ) * creator.globalScale);
					}
					
					//Check if child position is inside of a full block tile
					float _xArrayPos = Mathf.Ceil((_inst.transform.position.x - transform.position.x) - 1)  / creator.globalScale;
                    float _yArrayPos = Mathf.Ceil((_inst.transform.position.z - transform.position.z) - 1) / creator.globalScale;

                   
                    if (_xArrayPos < creator.width && _yArrayPos < creator.height && _xArrayPos >= 0 && _yArrayPos >= 0)
					{
                        //bool _check = false;
						
                        //if (creator.invert)
                        //{
                        //    _check = !_invert;
                        //}
                        //else
                        //{
                        //    _check = _invert;
                        //}
						
						
                        //check if spawned child object is placed on a inner tile cell
                        int _ccn2 = TileWorldNeighbourCounter.CountInnerTerrainBlocks(creator.worldMap[_layer].map, (int)_xArrayPos, (int)_yArrayPos, 1, creator.invert);

                        if (_ccn2 < 8)
                        {
                            DestroyImmediate(_inst.gameObject);
                        }
                        else
                        {
                            _inst.transform.parent = _container.transform;
                        }
					
					}
					else
					{
						DestroyImmediate(_inst.gameObject);
					}
					
					
						
				}
			}
		}
		
		
		public bool mapExist()
		{
			if (creator == null)
			{
				creator = GetComponent<TileWorldCreator>();
			}
			
			bool _return = false;
			
			if (creator.worldMap.Count < 1)
			{
				_return = false;		
			}
			else
			{
				if (creator.worldMap[0].map != null)
				{
					_return = true;	
				}
				else 
				{
					_return = false;
				}
			}
			
			return _return;
		}
		
	
		/// <summary>
		/// Merge random placed objects. Uses settings from TileWorldCreator.
		/// </summary>
		public void Merge()
		{
			for (int c = 0; c < clusters.Count; c ++)
			{
				TileWorldCombiner.CombineMesh(clusters[c], creator.addCollider);

                if (creator.createMultiMaterialMesh)
                {
                    TileWorldCombiner.CombineMulitMaterialMesh(clusters[c], creator.addCollider);
                }
			}
		}
		
		//Return selected enum position to int value
		int SelectedPositionToInt(spawnPosition _selectedPosition)
		{
			int _return = 0;
			
			switch (_selectedPosition)
			{
			case spawnPosition.topLeft:
				_return = 0;
				break;
			case spawnPosition.topCenter:
				_return = 1;
				break;
			case spawnPosition.topRight:
				_return = 2;
				break;
			case spawnPosition.middleLeft:
				_return = 3;
				break;
			case spawnPosition.middleCenter:
				_return = 4;
				break;
			case spawnPosition.middleRight:
				_return = 5;
				break;
			case spawnPosition.bottomLeft:
				_return = 6;
				break;
			case spawnPosition.bottomCenter:
				_return = 7;
				break;
			case spawnPosition.bottomRight:
				_return = 8;
				break;
			}
			
			return _return;
			
		}
		
		//return selected int position to enum
		spawnPosition SelectedPositionToEnum(int _selectedPosition)
		{
			spawnPosition _return = spawnPosition.topLeft;
			
			switch (_selectedPosition)
			{
			case 0:
				_return = spawnPosition.topLeft;
				break;
			case 1:
				_return = spawnPosition.topCenter;
				break;
			case 2:
				_return = spawnPosition.topRight;
				break;
			case 3:
				_return = spawnPosition.middleLeft;
				break;
			case 4:
				_return = spawnPosition.middleCenter;
				break;
			case 5:
				_return = spawnPosition.middleRight;
				break;
			case 6:
				_return = spawnPosition.bottomLeft;
				break;
			case 7:
				_return = spawnPosition.bottomCenter;
				break;
			case 8:
				_return = spawnPosition.bottomRight;
				break;
			}
			
			return _return;
			
		}
        

        public string[] GetLayers()
        {
            string[] _placeOnLayer = new string[creator.layerCount];
            for (int i = 0; i < creator.layerCount; i ++ )
            {
                _placeOnLayer[i] = "layer: " + (i + 1);
            }

            return _placeOnLayer;
        }
	}
}
