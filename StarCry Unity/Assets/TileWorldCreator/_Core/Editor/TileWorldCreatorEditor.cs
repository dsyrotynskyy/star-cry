﻿/* TILE WORLD CREATOR
 * Copyright (c) 2015 doorfortyfour OG
 * 
 * Create awesome tile worlds in seconds.
 *
 *
 * Documentation: http://tileworldcreator.doofortyfour.com
 * Like us on Facebook: http://www.facebook.com/doorfortyfour2013
 * Web: http://www.doorfortyfour.com
 * Contact: mail@doorfortyfour.com Contact us for help, bugs or 
 * share your awesome work you've made with TileWorldCreator
*/

#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

using TileWorld;



[CustomEditor(typeof(TileWorldCreator))]
public class TileWorldCreatorEditor : Editor
{

    //-------------------------------------------------------

    //REFERENCE
    public TileWorldCreator creator;

    //Undo stack
    public TileWorldUndoStack<bool[]> undoStack;
    public TileWorldUndoStack<bool[]> redoStack;

    //Editor
    Plane groundPlane = new Plane(Vector3.up, new Vector3(0f, 0f, 0f));
    Vector3 currentCameraPosition;
    Vector3 lastCameraPosition;


    //GUI Images
    Texture2D guiPresetImage1;
    Texture2D guiPresetImage2;
    Texture2D guiPresetImage3;
    Texture2D guiPresetImage4;
    Texture2D guiPresetImage5;
    Texture2D iconLayer;
    Texture2D iconFillFloor;
    Texture2D iconFillBlock;
    Texture2D iconDuplicate;
    Texture2D iconMaskLayerArrow;
    Texture2D iconUndockWindow;
    Texture2D iconCopyMap;
    Texture2D iconPasteMap;
    Texture2D iconCurrentCopyMode;

    Texture2D topLogo;

    //GUI Colors
    //Color guiGreen = new Color (0f / 255f, 255f / 255f, 0f / 255f);
    Color guiRed = new Color(255f / 255f, 100f / 255f, 100f / 255f);
    Color guiBlue = new Color(0f / 255f, 180f / 255f, 255f / 255f);
    Color guiBluelight = new Color (180f / 255f, 240f / 255f, 255f / 255f);

    bool[] tmpMap = new bool[0];
    //save the cells user has painted on, so we know where we should do an optimization pass
    bool[,] paintRegisterMap = new bool[0, 0];
    

    string[] _maskNames;

    string currentScene;

    bool[,] cloneMap;

    //----------------------------------------------------------------

    private void OnEnable()
    {
              
        //get script Reference
        creator = (TileWorldCreator)target;

        //SceneView.onSceneGUIDelegate = GridUpdate;

        LoadResources();

        undoStack = new TileWorldUndoStack<bool[]>();
        redoStack = new TileWorldUndoStack<bool[]>();

        //get all available masks
        TileWorldMaskLookup.GetMasks(out creator.iMasks, out _maskNames);
        
        currentScene = EditorApplication.currentScene;
        EditorApplication.hierarchyWindowChanged += HierarchyWindowChanged;
    }


    void HierarchyWindowChanged()
    {
        if (currentScene != EditorApplication.currentScene)
        {
            
            //reset first time build on startup
            TileWorldCreator[] _creator = GameObject.FindObjectsOfType<TileWorldCreator>();
            for (int i = 0; i < _creator.Length; i++)
            {
                _creator[i].firstTimeBuild = true;
            }
            
            currentScene = EditorApplication.currentScene;
        }
    }


    void Callback(object obj)
    {
        //Debug.Log("selected: " + obj);
        switch (obj.ToString())
        {
            case "item 1":
                LoadMap();
                break;
            case "item 2":
                Save();
                break;
            case "item 3":

                if (EditorUtility.DisplayDialog("Reset all settings?",
                    "Are you sure you want to reset all settings including presets and layers?", "Yes", "No"))
                {
                    creator.ClearSettings(true, 1);
                }

                break;
            case "item 4":
                TileWorldAbout.InitAbout();
                break;
            case "item 5":
                Application.OpenURL("http://tileworldcreator.doorfortyfour.com/documentation.html");
                break;
        }
    }


    public override void OnInspectorGUI()
    {
       

        if (GUILayout.Button(topLogo, "TextArea"))
        {
            Event evt = Event.current;
            Rect contextRect = new Rect(0, 0, Screen.width, 300);

            Vector2 mousePos = evt.mousePosition;
            if (contextRect.Contains(mousePos))
            {
                // create menu
                GenericMenu menu = new GenericMenu();

                menu.AddItem(new GUIContent("Load Map"), false, Callback, "item 1");
                menu.AddItem(new GUIContent("Save Map"), false, Callback, "item 2");
                menu.AddSeparator("");
                menu.AddItem(new GUIContent("Reset all"), false, Callback, "item 3");
                menu.AddSeparator("");
                menu.AddItem(new GUIContent("About"), false, Callback, "item 4");
                menu.AddItem(new GUIContent("Help"), false, Callback, "item 5");
                menu.AddSeparator("");
                //menu.AddItem (new GUIContent ("SubMenu/MenuItem3"), false, Callback, "item 3");

                menu.ShowAsContext();
                evt.Use();
            }
        }


        if (creator.presets.Count < 1)
        {
            EditorGUILayout.HelpBox("No presets defined", MessageType.Info);

        }

        //SHOW SETTINGS
        //-------------
        GUILayout.BeginHorizontal("Box");

        creator.showGeneralWorldSettings = GUILayout.Toggle(creator.showGeneralWorldSettings, ("Settings"), GUI.skin.GetStyle("foldout"), GUILayout.ExpandWidth(true), GUILayout.Height(18));

        if (GUILayout.Button(iconUndockWindow, "ToolbarButton", GUILayout.Width(25)))
        {
            TileWorldCreatorEditorSettingsWindow.InitWindow(creator.gameObject);
        }

        GUILayout.EndHorizontal();

        if (creator.showGeneralWorldSettings)
        {
            ShowSettings();
        }

        //SHOW PRESETS
        //------------
        GUILayout.BeginHorizontal("Box");
        creator.showBlockPresets = GUILayout.Toggle(creator.showBlockPresets, ("Presets"), GUI.skin.GetStyle("foldout"), GUILayout.ExpandWidth(true), GUILayout.Height(18));

        if (GUILayout.Button(iconUndockWindow, "ToolbarButton",GUILayout.Width(25)))
        {
            TileWorldCreatorEditorPresetsWindow.InitWindow(creator.gameObject);
        }
        GUILayout.EndHorizontal();

        if (creator.showBlockPresets)
        {
            ShowPresets();
        }


        //SHOW EDIT MAP
        //-------------
        GUILayout.BeginHorizontal("Box");
        creator.showEdit = GUILayout.Toggle(creator.showEdit, ("Edit map"), GUI.skin.GetStyle("foldout"), GUILayout.ExpandWidth(true), GUILayout.Height(18));

        if (GUILayout.Button(iconUndockWindow, "ToolbarButton", GUILayout.Width(25)))
        {
            TileWorldCreatorEditorEditMapWindow.InitWindow(creator.gameObject);
        }

        GUILayout.EndHorizontal();

        if (creator.showEdit)
        {
            ShowEdit();
        }

        if (GUILayout.Button("Generate New", GUILayout.Height(30)))
        {
            //reset undo stack
            undoStack = new TileWorldUndoStack<bool[]>();
            redoStack = new TileWorldUndoStack<bool[]>();

            paintRegisterMap = new bool[creator.width, creator.height];
           

            creator.GenerateMaps();

            for (int li = 0; li < creator.layerCount; li++)
            {
                creator.worldMap[li].maskMap = new bool[creator.width, creator.height];
                creator.worldMap[li].maskMap = creator.iMasks[creator.worldMap[li].selectedMask].ApplyMask(creator.worldMap[li].map, creator.GetComponent<TileWorldCreator>());
            }
        }

        if (GUILayout.Button("Build", GUILayout.Height(30)))
        {
            if (creator.firstTimeBuild)
            {              
                //creator.BuildMapEditor();
                creator.BuildMapComplete(false, false, true);
               
            }
            else
            {
                if (creator.buildOverlappingTiles)
                {
                    //creator.BuildMapPartial(true);
                    creator.BuildMapPartial(false, false, 0, 0);
                }
                else
                {
                    creator.firstTimeBuild = true;
                    //creator.BuildMapEditor();
                    creator.BuildMapComplete(false, false, true);
                }
            }
        }


        if (creator.mergeReady)
        {
            GUI.enabled = true;
        }
        else
        {
            GUI.enabled = false;
        }

        if (GUILayout.Button("Merge", GUILayout.Height(30)))
        {
            Merge();
            creator.mergeReady = false;
        }

        GUI.enabled = true;


        SceneView.RepaintAll();

        if (GUI.changed)
        {
            creator.firstTimeBuild = true;
        }
        
    }


    //SHOW SETTUNGS
    //-----------------
    public void ShowSettings()
    {

        //SETTINGS
        //--------
        GUILayout.BeginVertical("Box", GUILayout.ExpandWidth(true));

            GUILayout.BeginVertical("Box");
            creator.worldName = EditorGUILayout.TextField("Name:", creator.worldName);
            creator.invert = EditorGUILayout.Toggle("Invert:", creator.invert);
            creator.floodunjoined = EditorGUILayout.Toggle("Flood unjoined:", creator.floodunjoined);
            creator.floodholes = EditorGUILayout.Toggle("Flood holes:", creator.floodholes);
            creator.createFloor = EditorGUILayout.Toggle("Water:", creator.createFloor);      
            //creator.optimize = EditorGUILayout.Toggle("Optimize:", creator.optimize);
            //if (creator.optimize)
            //{
                //creator.optimizeSteps = EditorGUILayout.IntField("Optimize steps:", creator.optimizeSteps);
            //}
            GUILayout.EndVertical();

            GUILayout.BeginVertical("Box");

            creator.layerCount = EditorGUILayout.IntField("number of layers:", creator.layerCount);
            creator.layerInset = EditorGUILayout.IntField("layer inset:", creator.layerInset);

            if (creator.layerCount > 1)
            {
                creator.buildOverlappingTiles = EditorGUILayout.Toggle("Build overlapping tiles", creator.buildOverlappingTiles);
            }

            GUILayout.EndVertical();

            GUILayout.BeginVertical("Box");


            creator.width = EditorGUILayout.IntField("grid width:", creator.width);
            creator.height = EditorGUILayout.IntField("grid height:", creator.height);
            creator.globalScale = EditorGUILayout.Slider("Scale:", creator.globalScale, 1, 100);
            creator.scaleTiles = EditorGUILayout.Toggle("Scale tiles:", creator.scaleTiles);

            GUILayout.EndVertical();

            GUILayout.BeginVertical("Box");
            //creator.deathLimit = EditorGUILayout.IntField("Death limit:", creator.deathLimit);

            //creator.birthLimit = EditorGUILayout.IntField("Birth limit:", creator.birthLimit);

            creator.chanceToStartAlive = EditorGUILayout.Slider("Chance to start alive:", creator.chanceToStartAlive, 0f, 1f);

            creator.numberOfSteps = EditorGUILayout.IntField("Number of steps:", creator.numberOfSteps);

            creator.randomSeed = EditorGUILayout.IntField("Random seed:", creator.randomSeed);


            creator.minSize = EditorGUILayout.IntField("Minimum size:", creator.minSize);
            if (creator.minSize >= (creator.width * creator.height) / 2)
            {
                EditorGUILayout.HelpBox("Minimum size can only be max 50% of the width * height size of the map." + "\n" +
                    "Try to set a lower minimum size", MessageType.Warning);
                creator.minSize = (creator.width * creator.height) / 2;
            }
            GUILayout.EndVertical();

            GUILayout.BeginVertical("Box");
            GUILayout.Label("Merge settings:");

            EditorGUILayout.HelpBox("Cluster size defines the size of a cluster in width and height." + "\n" +
                "if you wish only one cluster set the size to the max length of your map (max width or max height)", MessageType.Info);

            creator.clusterSize = EditorGUILayout.IntField("Cluster Size: ", creator.clusterSize);
            creator.createMultiMaterialMesh = EditorGUILayout.Toggle("Multi material mesh: ", creator.createMultiMaterialMesh);
            creator.addCollider = EditorGUILayout.Toggle("Add mesh collider: ", creator.addCollider);

            GUILayout.EndVertical();


        GUILayout.EndVertical();
    }

    //SHOW PRESETS
    //------------------
    public void ShowPresets()
    {
        //PRESETS
        //-------
        GUILayout.BeginVertical("Box", GUILayout.ExpandWidth(false));


            if (GUILayout.Button("Add new preset"))
            {
                AddNewPreset();
            }

            EditorGUILayout.BeginVertical("TextArea");


            for (int i = 0; i < creator.presets.Count; i++)
            //for (int i = 0; i < creator.blocks.Count; i ++)
            {

                GUI.color = guiBlue;
                GUILayout.BeginVertical("Box");

                GUI.color = Color.white;

                GUILayout.BeginHorizontal();

                creator.presets[i].showPresetEditor = GUILayout.Toggle(creator.presets[i].showPresetEditor, ("Preset: " + i), GUI.skin.GetStyle("foldout"), GUILayout.ExpandWidth(true), GUILayout.Height(18));

                //duplicate current preset
                if (GUILayout.Button(iconDuplicate, "ToolbarButton", GUILayout.Height(18), GUILayout.Width(25)))
                {
                    DuplicatePreset(i);
                }

                //delete current preset
                GUI.color = guiRed;
                if (i > 0)
                {
                    if (GUILayout.Button("x", "ToolbarButton", GUILayout.Height(18), GUILayout.Width(25)))
                    {
                        DeletePreset(i);
                    }
                }
                GUI.color = Color.white;

                GUILayout.EndHorizontal();

                if (i < creator.presets.Count)
                {

                    if (creator.presets[i].showPresetEditor)
                    {


                        GUILayout.BeginHorizontal();
                        //GUILayout.Label("");	

                        if (GUILayout.Button("+ tileset", GUILayout.Height(18), GUILayout.Width(100)))
                        {
                            AddNewTileSet(i);
                        }

                        GUILayout.EndHorizontal();

                        //TILE SETS
                        //---------

                        for (int t = 0; t < creator.presets[i].tiles.Count; t++)
                        {

                            GUILayout.BeginVertical("TextArea");

                            //------------------------------------
                            GUILayout.BeginHorizontal("Box");



                            GUI.color = guiRed;
                            if (t > 0)
                            {
                                if (GUILayout.Button("x", "MiniButton", GUILayout.Height(18), GUILayout.Width(20)))
                                {
                                    DeleteTileSet(i, t);
                                }
                            }
                            GUI.color = Color.white;

                            GUILayout.Label("Tileset: " + t);

                            GUILayout.EndHorizontal();
                            //--------------------------------------
                            
                            
                            DragDropArea(i, t);


                            if (t < creator.presets[i].tiles.Count)
                            {
                                GUILayout.BeginHorizontal();
                                GUILayout.BeginVertical();
                                GUILayout.Label(guiPresetImage1, GUILayout.MaxWidth(50), GUILayout.MaxHeight(50));
                                creator.presets[i].tiles[t].tileI = EditorGUILayout.ObjectField(creator.presets[i].tiles[t].tileI, typeof(GameObject), true, GUILayout.MaxWidth(50)) as GameObject;
                                GUILayout.Label("yRotOff:");
                                creator.presets[i].tiles[t].yRotationOffset[0] = EditorGUILayout.FloatField("", creator.presets[i].tiles[t].yRotationOffset[0], GUILayout.Width(50));
                                GUILayout.EndVertical();
                                GUILayout.FlexibleSpace();
                                GUILayout.BeginVertical();
                                GUILayout.Label(guiPresetImage2, GUILayout.MaxWidth(50), GUILayout.MaxHeight(50));
                                creator.presets[i].tiles[t].tileC = EditorGUILayout.ObjectField(creator.presets[i].tiles[t].tileC, typeof(GameObject), true, GUILayout.MaxWidth(50)) as GameObject;
                                GUILayout.Label("yRotOff:");
                                creator.presets[i].tiles[t].yRotationOffset[1] = EditorGUILayout.FloatField("", creator.presets[i].tiles[t].yRotationOffset[1], GUILayout.Width(50));
                                GUILayout.EndVertical();
                                GUILayout.FlexibleSpace();
                                GUILayout.BeginVertical();
                                GUILayout.Label(guiPresetImage3, GUILayout.MaxWidth(50), GUILayout.MaxHeight(50));
                                creator.presets[i].tiles[t].tileCi = EditorGUILayout.ObjectField(creator.presets[i].tiles[t].tileCi, typeof(GameObject), true, GUILayout.MaxWidth(50)) as GameObject;
                                GUILayout.Label("yRotOff:");
                                creator.presets[i].tiles[t].yRotationOffset[2] = EditorGUILayout.FloatField("", creator.presets[i].tiles[t].yRotationOffset[2], GUILayout.Width(50));
                                GUILayout.EndVertical();
                                GUILayout.FlexibleSpace();
                                GUILayout.BeginVertical();
                                GUILayout.Label(guiPresetImage4, GUILayout.MaxWidth(50), GUILayout.MaxHeight(50));
                                creator.presets[i].tiles[t].tileB = EditorGUILayout.ObjectField(creator.presets[i].tiles[t].tileB, typeof(GameObject), true, GUILayout.MaxWidth(50)) as GameObject;
                                GUILayout.Label("yRotOff:");
                                creator.presets[i].tiles[t].yRotationOffset[3] = EditorGUILayout.FloatField("", creator.presets[i].tiles[t].yRotationOffset[3], GUILayout.Width(50));
                                GUILayout.EndVertical();
                                GUILayout.FlexibleSpace();
                                GUILayout.BeginVertical();
                                GUILayout.Label(guiPresetImage5, GUILayout.MaxWidth(50), GUILayout.MaxHeight(50));
                                creator.presets[i].tiles[t].tileF = EditorGUILayout.ObjectField(creator.presets[i].tiles[t].tileF, typeof(GameObject), true, GUILayout.MaxWidth(50)) as GameObject;
                                GUILayout.Label("yRotOff:");
                                creator.presets[i].tiles[t].yRotationOffset[4] = EditorGUILayout.FloatField("", creator.presets[i].tiles[t].yRotationOffset[4], GUILayout.Width(50));
                                GUILayout.EndVertical();
                                GUILayout.EndHorizontal();

                                GUILayout.Space(20);
                                creator.presets[i].tiles[t].blockOffset = EditorGUILayout.Vector3Field("terrain offset: ", creator.presets[i].tiles[t].blockOffset);
                                creator.presets[i].tiles[t].floorOffset = EditorGUILayout.Vector3Field("water offset: ", creator.presets[i].tiles[t].floorOffset);

                            }

                            GUILayout.EndVertical();
                        }
                    }

                }

                GUILayout.EndVertical();

            }
            GUILayout.EndVertical();


        GUILayout.EndVertical();
    }

    void DragDropArea(int _i, int _t)
    {
        Event evt = Event.current;
        Rect drop_area = GUILayoutUtility.GetRect(0.0f, 50.0f, GUILayout.ExpandWidth(true));
        GUI.Box(drop_area, "Drag and drop tile prefabs here for automatic assignment" + "\n (alphabetical order = tile assignment from left to right)");

        switch (evt.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!drop_area.Contains(evt.mousePosition))
                    return;

                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                if (evt.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();

                    var a = 0;
                    Object[] _obj = DragAndDrop.objectReferences;
                    List<Object> _objList = new List<Object>();
                    for (int o = 0; o < _obj.Length; o ++ )
                    {
                        _objList.Add(_obj[o]);
                    }

                    _objList.Sort(CompareByName);

                    for (int i = 0; i < _objList.Count; i++)
                    {
                        if (a == 0)
                        {
                            creator.presets[_i].tiles[_t].tileI = _objList[i] as GameObject;
                        }
                        else if (a == 1)
                        {
                            creator.presets[_i].tiles[_t].tileC = _objList[i] as GameObject;
                        }
                        else if (a == 2)
                        {
                            creator.presets[_i].tiles[_t].tileCi = _objList[i] as GameObject;
                        }
                        else if (a == 3)
                        {
                            creator.presets[_i].tiles[_t].tileB = _objList[i] as GameObject;
                        }
                        else if (a >= 4)
                        {
                            creator.presets[_i].tiles[_t].tileF = _objList[i] as GameObject;
                        }

                        a++;
                    
                    }
                      
                }
                break;
        }
    }

    int CompareByName(Object _item1, Object _item2)
    {
        return _item1.name.CompareTo(_item2.name);
    }

    //SHOW EDIT
    //------------------
    public void ShowEdit()
    {
        //SHOW EDIT
        GUILayout.BeginVertical("Box", GUILayout.ExpandWidth(false));

      
            EditorGUILayout.HelpBox("Left Click Add / Right Click Remove Cell" + "\n" + "Undo/Redo: Z + Y" + "\n" + "Show/Hide grid: H", MessageType.Info);

            GUILayout.BeginHorizontal();
            GUILayout.Label("Show Grid:");
            creator.showGrid = EditorGUILayout.Toggle(creator.showGrid);

            GUILayout.Label("Show Grid on deselect:");
            creator.showGridAlways = EditorGUILayout.Toggle(creator.showGridAlways);
            GUILayout.EndHorizontal();

            creator.autoBuild = EditorGUILayout.Toggle("Automatic build:", creator.autoBuild);

            //GUILayout.BeginHorizontal();
            //creator.brushx2 = EditorGUILayout.Toggle("Brush size x2:", creator.brushx2);
            //GUILayout.Label("enable recommended");
            //GUILayout.EndHorizontal();
            creator.brushSize = EditorGUILayout.IntSlider("Brush size:", creator.brushSize, 2, 5 );    

            creator.cellColor = EditorGUILayout.ColorField("Cell Color:", creator.cellColor);
            creator.brushColor = EditorGUILayout.ColorField("Brush Color:", creator.brushColor);
            creator.floorCellColor = EditorGUILayout.ColorField("Floor Color:", creator.floorCellColor);
            creator.maskColor = EditorGUILayout.ColorField("Mask Color:", creator.maskColor);

            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical("Box");
            EditorGUILayout.BeginHorizontal();


            //fill layer
            if (GUILayout.Button(iconFillBlock, GUILayout.Width(35), GUILayout.Height(35)))
            {
                if (!creator.worldMap[creator.mapIndex].paintMask)
                {
                    creator.FillLayerBlock();
                }
                else
                {
                    creator.worldMap[creator.mapIndex].maskMap = new bool[creator.width, creator.height];
                }
            }

            if (GUILayout.Button(iconFillFloor, GUILayout.Width(35), GUILayout.Height(35)))
            {
                if (!creator.worldMap[creator.mapIndex].paintMask)
                {
                    creator.FillLayerFloor();
                }
                else
                {
                    for (int ym = 0; ym < creator.worldMap[creator.mapIndex].maskMap.GetLength(1); ym++)
                    {
                        for (int xm = 0; xm < creator.worldMap[creator.mapIndex].maskMap.GetLength(0); xm++)
                        {
                            creator.worldMap[creator.mapIndex].maskMap[xm, ym] = true;
                        }
                    }
                }
            }

            //copy / paste layer    
            //copy map from layer
            if (GUILayout.Button(iconCopyMap, GUILayout.Width(35), GUILayout.Height(35)))
            {
                creator.CopyMapFromLayer();              
            }
            //paste map to layer
            if (GUILayout.Button(iconPasteMap, GUILayout.Width(35), GUILayout.Height(35)))
            {
                creator.PasteMapToLayer();
            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            GUILayout.BeginVertical("Box");
            GUILayout.BeginHorizontal();
            GUILayout.Label(iconLayer, GUILayout.Width(20), GUILayout.Height(20));
            GUILayout.Label("Layers");
            GUILayout.EndHorizontal();


            GUILayout.BeginVertical("TextArea");

            //show layers
            for (int l = creator.worldMap.Count - 1; l >= 0; l--)
            {
                if (creator.mapIndex == l)
                {
                    GUI.color = guiBlue;
                }
                GUILayout.BeginHorizontal("Box");

                GUILayout.Label(iconLayer, GUILayout.Width(20), GUILayout.Height(18));

                if (GUILayout.Button("Layer: " + (l + 1).ToString(), "Label", GUILayout.Height(18)))
                {
                    creator.mapIndex = l;

                    for (int pm = 0; pm < creator.worldMap.Count; pm ++  )
                    {
                        creator.worldMap[pm].paintMask = false;
                    }
                        
                }

                GUI.color = Color.white;

                //EditorGUILayout.LayerField(0, GUILayout.Width(50));
                creator.layerPresetIndex[l] = EditorGUILayout.Popup(creator.layerPresetIndex[l], creator.blockLayerPresets);

                //duplicate layer
                if (GUILayout.Button(iconDuplicate, "ToolbarButton", GUILayout.Height(18), GUILayout.Width(20)))
                {
                    creator.AddNewLayer(true, false, 0, true, l);
                }


                GUI.color = guiRed;
                if (l != 0)
                {
                    if (GUILayout.Button("x", "ToolbarButton", GUILayout.Height(18), GUILayout.Width(20)))
                    {

                        creator.RemoveLayer(l);
                    }
                }
                GUI.color = Color.white;

                GUILayout.EndHorizontal();

                //Mask Layers
                if (l < creator.worldMap.Count)
                {
                    if (creator.worldMap[l].useMask)
                    {
                        if (creator.worldMap[l].paintMask)
                        {
                            GUI.color = guiBluelight;
                        }
                        
                        GUILayout.BeginHorizontal();

                        GUILayout.Label(iconMaskLayerArrow, GUILayout.Width(20), GUILayout.Height(18));

                        GUILayout.BeginHorizontal("Box");
                        if (GUILayout.Button("Mask: " + (l + 1).ToString(), "Label", GUILayout.Height(18)))
                        {       
                            creator.mapIndex = l;


                            for (int pm = 0; pm < creator.worldMap.Count; pm++)
                            {
                                

                                if (pm == creator.mapIndex)
                                {
                                    if (!creator.worldMap[l].paintMask)
                                    { 
                                        creator.worldMap[l].paintMask = true;
                                    }
                                    else
                                    {
                                        creator.worldMap[l].paintMask = false;
                                    }
                                }
                                else { creator.worldMap[pm].paintMask = false; }
                            }

                            
                        }

                        GUI.color = Color.white;

                        creator.worldMap[l].selectedMask = EditorGUILayout.Popup(creator.worldMap[l].selectedMask, _maskNames);

                        if (GUILayout.Button("apply", "ToolbarButton", GUILayout.Height(15), GUILayout.Width(50)))
                        {
                            creator.worldMap[l].maskMap = new bool[creator.width, creator.height];
                            creator.worldMap[l].maskMap = creator.iMasks[creator.worldMap[l].selectedMask].ApplyMask(creator.worldMap[l].map, creator.GetComponent<TileWorldCreator>());


                            if (creator.firstTimeBuild)
                            {
                                //creator.BuildMapEditor();
                                creator.BuildMapComplete(false, false, true);

                                
                            }
                            else
                            {
                                if (creator.buildOverlappingTiles)
                                {
                                    //creator.BuildMapPartial(true);
                                    creator.BuildMapPartial(false, false, 0, 0);
                                }
                                else
                                {
                                    creator.firstTimeBuild = true;
                                    //creator.BuildMapEditor();
                                    creator.BuildMapComplete(false, false, true);
                                }
                            }

                            //update one dim array to make sure they are updated when saving map
                            creator.UpdateMap();
                        }

                        GUI.color = guiRed;
                        if (GUILayout.Button("x", "ToolbarButton", GUILayout.Height(15), GUILayout.Width(20)))
                        {
                            creator.worldMap[l].useMask = false;
                        }
                        GUI.color = Color.white;

                        GUILayout.EndHorizontal();
                       
                        GUILayout.EndHorizontal();
                        GUILayout.Space(4);
                    }
                }


                //GUILayout.Space(6);
            }
            GUILayout.Space(6);
            GUILayout.EndVertical();


            GUILayout.BeginHorizontal();
            
            if (GUILayout.Button("Add new layer"))
            {
                creator.AddNewLayer(true, false, 0, false, 0);
            }

            if (GUILayout.Button("Add mask"))
            {
                if (!creator.worldMap[creator.mapIndex].useMask)
                {
                    creator.AddNewMask(creator.mapIndex);   
                }
            }

            GUILayout.EndHorizontal();

            GUILayout.EndVertical();


        GUILayout.EndVertical();
    }


    private void OnSceneGUI()
    {
        if (!creator.showGrid)
        {
            Event _eventH = Event.current;

            //Show grid
            if (_eventH.type == EventType.KeyDown)
            {
                if (_eventH.keyCode == KeyCode.H)
                {
                    creator.showGrid = true;
                }
            }

            return;
        }


        Event _event = Event.current;

        //get mouse worldposition
        creator.mouseWorldPosition = GetWorldPosition(_event.mousePosition);

        //Check Mouse Events
        if (_event.type == EventType.MouseDown)
        {
            paintRegisterMap = new bool[creator.width, creator.height];
           

            //On Mouse Down store current camera position and set last position to current
            currentCameraPosition = Camera.current.transform.position;
            lastCameraPosition = currentCameraPosition;


            if (_event.button == 0)
            {

                if (IsMouseOverGrid(_event.mousePosition) && lastCameraPosition == currentCameraPosition)
                {
                    RegisterMap();
                    if (creator.invert)
                    {
                        PaintCell(true);
                    }
                    else
                    {
                        PaintCell(false);
                    }
                }
            }
            else if (_event.button == 1)
            {
                if (IsMouseOverGrid(_event.mousePosition) && lastCameraPosition == currentCameraPosition)
                {
                    RegisterMap();

                    if (creator.invert)
                    {
                        PaintCell(false);
                    }
                    else
                    {
                        PaintCell(true);
                    }
                    //block right mouse button navigation input
                    _event.Use();
                }
            }
        }

        // If Mouse Drag and left click. Start paint
        else if (_event.type == EventType.MouseDrag && _event.type != EventType.KeyDown)
        {
            //get current camera position, if camera position were unchanged we can be sure
            //user did not navigate in the scene
            currentCameraPosition = Camera.current.transform.position;
            //add cell
            if (_event.button == 0)
            {
                if (IsMouseOverGrid(_event.mousePosition) && lastCameraPosition == currentCameraPosition)
                {

                    if (creator.invert)
                    {
                        PaintCell(true);
                    }
                    else
                    {
                        PaintCell(false);
                    }
                }
            }

            //remove cell
            else if (_event.button == 1)
            {
                if (IsMouseOverGrid(_event.mousePosition) && lastCameraPosition == currentCameraPosition)
                {
                    if (creator.invert)
                    {
                        PaintCell(false);
                    }
                    else
                    {
                        PaintCell(true);
                    }


                    //block right mouse button navigation input
                    _event.Use();
                }
            }

        }

        else if (_event.type == EventType.MouseMove)
        {
            //check if mouse pointer is over grid
            creator.mouseOverGrid = IsMouseOverGrid(_event.mousePosition);
        }

        else if (_event.type == EventType.MouseUp)
        {
            currentCameraPosition = Camera.current.transform.position;
            lastCameraPosition = currentCameraPosition;

            if (IsMouseOverGrid(_event.mousePosition) && lastCameraPosition == currentCameraPosition)
            {   
                //Vector3 _gP = GetGridPosition(creator.mouseWorldPosition);
                RegisterUndo();
            }

#if UNITY_4_6
            Screen.showCursor = true;
#elif UNITY_5_0
		Cursor.visible = true;
#endif
        }

        else if (_event.type == EventType.Layout)
        {
            //this allows _event.Use() to actually function and block mouse input
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(GetHashCode(), FocusType.Passive));
        }



        else if (_event.type == EventType.KeyDown)
        {
            //UNDO
            if (_event.keyCode == (KeyCode.Z))
            {
                _event.Use();
                DoUndo();
            }

            //REDO
            else if (_event.keyCode == (KeyCode.Y))
            {
                _event.Use();
                DoRedo();
            }

            //Hide grid
            else if (_event.keyCode == KeyCode.H)
            {

                creator.showGrid = false;

            }

        }
    }


    //Paint cell true = add // false = delete
    void PaintCell(bool _add)
    {
        if (creator.worldMap.Count < 1)
            return;

        if (_add)
        {
            
            Vector3 _gP = GetGridPosition(creator.mouseWorldPosition);
            
            if (!creator.brushx2)
            {
                //register painted cell
                paintRegisterMap[(int)_gP.x, (int)_gP.z] = true;
               

                creator.worldMap[creator.mapIndex].map[(int)_gP.x, (int)_gP.z] = true;
            }
            else
            {
                //if x or z position of mouse is out of grid
                //set a correct int value
                if (_gP.x < 0)
                {
                    _gP.x = -1;
                }
                if (_gP.z < 0)
                {
                    _gP.z = -1;
                }


                for (int y = 0; y < creator.brushSize; y ++ )
                {
                    for (int x = 0; x < creator.brushSize; x++)
                    {
                        if (_gP.x + x >= 0 && _gP.z + y >= 0 && _gP.x + x < creator.worldMap[creator.mapIndex].map.GetLength(0) && _gP.z + y < creator.worldMap[creator.mapIndex].map.GetLength(1))
                        {
                            if (!creator.worldMap[creator.mapIndex].paintMask)
                            {
                                creator.worldMap[creator.mapIndex].map[(int)_gP.x + x, (int)_gP.z + y] = true;
                            }
                            else
                            {
                                creator.worldMap[creator.mapIndex].maskMap[(int)_gP.x + x, (int)_gP.z + y] = true;
                            }

                            //register painted cell
                            paintRegisterMap[(int)_gP.x + x, (int)_gP.z + y] = true;
                            
                        }
                    }
                }

          
            }

            creator.UpdateMap();
            EditorUtility.SetDirty(creator);

        }
        else
        {

            Vector3 _gP = GetGridPosition(creator.mouseWorldPosition);
             
            if (!creator.brushx2)
            {
                //register painted cell
                paintRegisterMap[(int)_gP.x, (int)_gP.z] = true;
               

                creator.worldMap[creator.mapIndex].map[(int)_gP.x, (int)_gP.z] = false;
            }
            else
            {

                if (_gP.x < 0)
                {
                    _gP.x = -1;
                }
                if (_gP.z < 0)
                {
                    _gP.z = -1;
                }


                for (int y = 0; y < creator.brushSize; y++)
                {
                    for (int x = 0; x < creator.brushSize; x++)
                    {
                        if (_gP.x + x >= 0 && _gP.z + y >= 0 && _gP.x + x < creator.worldMap[creator.mapIndex].map.GetLength(0) && _gP.z + y < creator.worldMap[creator.mapIndex].map.GetLength(1))
                        {
                            if (!creator.worldMap[creator.mapIndex].paintMask)
                            {
                                creator.worldMap[creator.mapIndex].map[(int)_gP.x + x, (int)_gP.z + y] = false;
                            }
                            else
                            {
                                creator.worldMap[creator.mapIndex].maskMap[(int)_gP.x + x, (int)_gP.z + y] = false;
                            }

                            //register painted cell
                            paintRegisterMap[(int)_gP.x + x, (int)_gP.z + y] = true;
                            
                        }
                    }
                }

            }

            creator.UpdateMap();
            EditorUtility.SetDirty(creator);

#if UNITY_4_6
            Screen.showCursor = false;
#elif UNITY_5_0
		Cursor.visible = false;
#endif
        }




    }

    private void DoUndo()
    {
        if (undoStack.Count > 0)
        {
            redoStack.Push(creator.worldMap[creator.mapIndex].mapSingle);

            bool[] _tmp = undoStack.Pop();

            if (_tmp != null)
            {
                creator.worldMap[creator.mapIndex].map = new bool[creator.width, creator.height];
                creator.worldMap[creator.mapIndex].mapSingle = _tmp;
                int _index = 0;

                for (int y = 0; y < creator.height; y++)
                {
                    for (int x = 0; x < creator.width; x++)
                    {
                        if (_index < _tmp.Length)
                        {
                            creator.worldMap[creator.mapIndex].map[x, y] = _tmp[_index];
                            _index++;
                        }
                    }
                }

                creator.UpdateMap();

                if (creator.autoBuild)
                {
                    if (creator.firstTimeBuild)
                    {
                        creator.BuildMapComplete(false, false, true);
                    }
                    else
                    {
                        if (creator.buildOverlappingTiles)
                        {  
                            creator.BuildMapPartial(false, false, 0, 0);      
                        }
                        else
                        {
                            creator.firstTimeBuild = true;
                            creator.BuildMapComplete(false, false, true);
                        }
                    }
                   
                }
            }
        }
    }


    private void DoRedo()
    {
        if (redoStack.Count > 0)
        {
            undoStack.Push(creator.worldMap[creator.mapIndex].mapSingle);

            bool[] _tmp = redoStack.Pop();

            if (_tmp != null)
            {
                creator.worldMap[creator.mapIndex].map = new bool[creator.width, creator.height];
                creator.worldMap[creator.mapIndex].mapSingle = _tmp;
                int _index = 0;

                for (int y = 0; y < creator.height; y++)
                {
                    for (int x = 0; x < creator.width; x++)
                    {
                        creator.worldMap[creator.mapIndex].map[x, y] = _tmp[_index];
                        _index++;
                    }
                }

                creator.UpdateMap();

                if (creator.autoBuild)
                {
                    if (creator.firstTimeBuild)
                    {
                       creator.BuildMapComplete(false, false, true);
                    }
                    else
                    {
                        if (creator.buildOverlappingTiles)
                        {                           
                            creator.BuildMapPartial(false, false, 0, 0);                
                        }
                        else
                        {
                            creator.firstTimeBuild = true;
                            creator.BuildMapComplete(false, false, true);
                        }
                    }
                }
            }
        }
    }

    //Register Map
    //store mapSingle array to temp map
    private void RegisterMap()
    {
        if (creator.worldMap.Count < 1)
            return;

        tmpMap = new bool[creator.worldMap[creator.mapIndex].mapSingle.Length];
        for (int t = 0; t < creator.worldMap[creator.mapIndex].mapSingle.Length; t++)
        {
            tmpMap[t] = creator.worldMap[creator.mapIndex].mapSingle[t];
        }
    }

    //check if temp map is different than mapSingle
    //if it is, store it as new undo
    private void RegisterUndo()
    {
        if (creator.worldMap.Count > 0)
        {
            if (tmpMap != creator.worldMap[creator.mapIndex].mapSingle)
            {
                undoStack.Push(tmpMap);

                if (creator.autoBuild)
                {
                    if (creator.firstTimeBuild)
                    {
                        creator.BuildMapComplete(false, false, true);
                    }
                    else
                    {
                        if (creator.buildOverlappingTiles)
                        {
                            for (int y = 0; y < creator.height; y++)
                            {
                                for (int x = 0; x < creator.width; x++)
                                {

                                    if (paintRegisterMap[x, y])
                                    {
                                        paintRegisterMap[x, y] = false;
                                        creator.BuildMapPartial(false, true, x, y);
                                    }
                                }
                            }
                           
                        }
                        else
                        {
                            creator.firstTimeBuild = true;
                           creator.BuildMapComplete(false, false, true);
                        }
                    }
                }
            }
        }

       
    }



    // check if mouse pointer is over grid
    bool IsMouseOverGrid(Vector2 _mousePos)
    {
        Ray _ray = HandleUtility.GUIPointToWorldRay(_mousePos); //Camera.current.ScreenToWorldPoint(_pos);
        float _dist = 0.0f;
        bool _return = false;

        if (groundPlane.Raycast(_ray, out _dist))
        {
            Vector3 _worldPos = _ray.origin + _ray.direction.normalized * _dist;

            int _off = 0;
            if (!creator.brushx2)
            {
                _off = 0;
            }
            else
            {
                _off = -1;
            }

            if (_worldPos.x > _off + creator.transform.position.x && _worldPos.x < (creator.width * creator.globalScale) + creator.transform.position.x && _worldPos.z > _off + creator.transform.position.z && _worldPos.z < (creator.height * creator.globalScale) + creator.transform.position.z)
            {
                _return = true;
            }
        }

        return _return;
    }

    //return mouse world position
    Vector3 GetWorldPosition(Vector2 _mousePos)
    {
        Ray _ray = HandleUtility.GUIPointToWorldRay(_mousePos);
        float _dist = 0.0f;
        Vector3 _return = new Vector3(0, 0);

        if (groundPlane.Raycast(_ray, out _dist))
        {
            _return = _ray.origin + _ray.direction.normalized * _dist;
        }

        return _return;
    }

    //return the exact grid / cell position
    Vector3 GetGridPosition(Vector3 _mousePos)
    {
        Vector3 _gridPos = new Vector3((Mathf.Floor(_mousePos.x - creator.transform.position.x / 1) / creator.globalScale) , 0.05f, (Mathf.Floor(_mousePos.z - creator.transform.position.z/ 1) / creator.globalScale));
       
        return _gridPos;
    }


    //Save and Load Methods
    //--------
    void Save()
    {
        var _path = EditorUtility.SaveFilePanel("Save", Application.dataPath, "map", "xml");

        TileWorldCreatorSaveLoad.Save(_path, creator);
    }



    void LoadMap()
    {
        var _path = EditorUtility.OpenFilePanel("Load", Application.dataPath, "xml");

        TileWorldCreatorSaveLoad.Load(_path, creator);
    }
    //---------------


    void GridUpdate(SceneView sceneview)
    {
        //Event e = Event.current;
        //if (e.isMouse && e.button == 0)
        //{
        //	Debug.Log("hello");
        //}
    }

    void LoadResources()
    {
        var _path = ReturnInstallPath.GetInstallPath("Editor", this); // GetInstallPath();

        guiPresetImage1 = AssetDatabase.LoadAssetAtPath(_path + "Res/guipreset1.png", typeof(Texture2D)) as Texture2D;
        guiPresetImage2 = AssetDatabase.LoadAssetAtPath(_path + "Res/guipreset2.png", typeof(Texture2D)) as Texture2D;
        guiPresetImage3 = AssetDatabase.LoadAssetAtPath(_path + "Res/guipreset3.png", typeof(Texture2D)) as Texture2D;
        guiPresetImage4 = AssetDatabase.LoadAssetAtPath(_path + "Res/guipreset4.png", typeof(Texture2D)) as Texture2D;
        guiPresetImage5 = AssetDatabase.LoadAssetAtPath(_path + "Res/guipreset5.png", typeof(Texture2D)) as Texture2D;

        iconLayer = AssetDatabase.LoadAssetAtPath(_path + "Res/icon_layer.png", typeof(Texture2D)) as Texture2D;
        iconFillFloor = AssetDatabase.LoadAssetAtPath(_path + "Res/icon_fillfloor.png", typeof(Texture2D)) as Texture2D;
        iconFillBlock = AssetDatabase.LoadAssetAtPath(_path + "Res/icon_fillblock.png", typeof(Texture2D)) as Texture2D;
        iconDuplicate = AssetDatabase.LoadAssetAtPath(_path + "Res/icon_duplicate.png", typeof(Texture2D)) as Texture2D;
        iconMaskLayerArrow = AssetDatabase.LoadAssetAtPath(_path + "Res/masklayerarrow.png", typeof(Texture2D)) as Texture2D;
        iconUndockWindow = AssetDatabase.LoadAssetAtPath(_path + "Res/icon_undock.png", typeof(Texture2D)) as Texture2D;
        iconCopyMap = AssetDatabase.LoadAssetAtPath(_path + "Res/icon_copy.png", typeof(Texture2D)) as Texture2D;
        iconPasteMap = AssetDatabase.LoadAssetAtPath(_path + "Res/icon_paste.png", typeof(Texture2D)) as Texture2D;


        topLogo = AssetDatabase.LoadAssetAtPath(_path + "Res/topLogo.png", typeof(Texture2D)) as Texture2D;
    }


    public void Merge()
    {
        if (creator.containers.Count < 1)
        {
            creator.mergeReady = true;
        }

        for (int c = 0; c < creator.containers.Count; c++)
        {
            TileWorldCombiner.CombineMesh(creator.containers[c], creator.addCollider);

            if (creator.createMultiMaterialMesh)
            {
                 TileWorldCombiner.CombineMulitMaterialMesh(creator.containers[c], creator.addCollider);
            }
        }
    }

  

    //EDITOR METHODS
    //------------------
    /// <summary>
    /// Editor method. Add a new preset.
    /// </summary>
    void AddNewPreset()
    {
        //blocks.Add(new TileSets());
        creator.presets.Add(new TileWorldCreator.Presets());

        creator.ResizeStringArray(false, creator.presets.Count - 1);
        //ResizeIntArray(false, presets.Count -1);
    }


    /// <summary>
    /// Editor method. Duplicate a preset
    /// </summary>
    /// <param name="_index"></param>
    void DuplicatePreset(int _index)
    {
        AddNewPreset();

        if (creator.presets[_index].tiles.Count > 1)
        {
            for (int t1 = 0; t1 < creator.presets[_index].tiles.Count - 1; t1++)
            {
                creator.presets[creator.presets.Count - 1].tiles.Add(new TileWorldCreator.TileSets());
            }

        }


        for (int t2 = 0; t2 < creator.presets[_index].tiles.Count; t2++)
        {
            creator.presets[creator.presets.Count - 1].tiles[t2].tileI = creator.presets[_index].tiles[t2].tileI;
            creator.presets[creator.presets.Count - 1].tiles[t2].tileC = creator.presets[_index].tiles[t2].tileC;
            creator.presets[creator.presets.Count - 1].tiles[t2].tileCi = creator.presets[_index].tiles[t2].tileCi;
            creator.presets[creator.presets.Count - 1].tiles[t2].tileB = creator.presets[_index].tiles[t2].tileB;
            creator.presets[creator.presets.Count - 1].tiles[t2].tileF = creator.presets[_index].tiles[t2].tileF;

            creator.presets[creator.presets.Count - 1].tiles[t2].floorOffset = creator.presets[_index].tiles[t2].floorOffset;
            creator.presets[creator.presets.Count - 1].tiles[t2].blockOffset = creator.presets[_index].tiles[t2].blockOffset;
        }


    }


    /// <summary>
    /// Editor method. Add a new tileset to the given preset index
    /// </summary>
    /// <param name="_index"></param>
    void AddNewTileSet(int _index)
    {
        creator.presets[_index].tiles.Add(new TileWorldCreator.TileSets());
    }


    /// <summary>
    /// Editor method. Delete tileset
    /// </summary>
    /// <param name="_presetIndex"></param>
    /// <param name="_tileIndex"></param>
    void DeleteTileSet(int _presetIndex, int _tileIndex)
    {
        creator.presets[_presetIndex].tiles.RemoveAt(_tileIndex);
    }

    /// <summary>
    /// Editor method. Delete preset
    /// </summary>
    /// <param name="_inx"></param>
    void DeletePreset(int _inx)
    {
        if (_inx != 0)
        {
            creator.presets.RemoveAt(_inx);
            //blocks.RemoveAt(_inx);

            creator.ResizeStringArray(true, _inx);
            //ResizeIntArray(false, presets.Count -1);
        }


    }



    void AssignSettings()
    {
        creator.invert = creator.settings.invert;
        creator.floodunjoined = creator.settings.floodunjoined;
        creator.floodholes = creator.settings.floodholes;
        creator.createFloor = creator.settings.createFloor;

        creator.layerCount = creator.settings.layerCount;
        creator.layerInset = creator.settings.layerInset;

        creator.chanceToStartAlive = creator.settings.chanceToStartAlive;
        creator.numberOfSteps = creator.settings.numberOfSteps;

        creator.width = creator.settings.width;
        creator.height = creator.settings.height;

        creator.randomSeed = creator.settings.randomSeed;
        creator.minSize = creator.settings.minSize;
        creator.clusterSize = creator.settings.clusterSize;
        creator.buildOverlappingTiles = creator.settings.buildOverlappingTiles;
    }

   
}
#endif