﻿/* TILE WORLD CREATOR
 * Copyright (c) 2015 doorfortyfour OG
 * 
 * Create awesome tile worlds in seconds.
 *
 *
 * Documentation: http://tileworldcreator.doofortyfour.com
 * Like us on Facebook: http://www.facebook.com/doorfortyfour2013
 * Web: http://www.doorfortyfour.com
 * Contact: mail@doorfortyfour.com Contact us for help, bugs or 
 * share your awesome work you've made with TileWorldCreator
*/

#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using TileWorld;

[CustomEditor(typeof(TileWorldObjectScatterer))]
public class TileWorldObjectScattererEditor : Editor
{

    //Reference
    TileWorldObjectScatterer scatterer;

    string[] options = new string[] { "", "", "", "", "", "", "", "", "" };

    string[] placeOptions = new string[] { "place on terrain", "place on water" };

    string[] placeLayers = new string[] { "layer 1" };

    bool showSetStartEndPosition = false;
    bool showSetRandomObjects = false;

    float _uniformScalingMin = 0.0f;
    float _uniformScalingMax = 0.0f;
    float _uniformScalingChildsMin = 0.0f;
    float _uniformScalingChildsMax = 0.0f;


    //GUI Color
    Color guiRed = new Color(255f / 255f, 100f / 255f, 100f / 255f);
    Color guiBlue = new Color(0f / 255f, 180f / 255f, 255f / 255f);
    Texture2D iconDuplicate;

    void OnEnable()
    {
        scatterer = (TileWorldObjectScatterer)target;

        placeLayers = scatterer.GetLayers();
        LoadResources();
    }


    public override void OnInspectorGUI()
    {
        if (!scatterer.mapExist())
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.HelpBox("No existing map found!" + "\n" + "Generate a new one.", MessageType.Error);
            EditorGUILayout.EndHorizontal();
        }

        //Show Set Start/End position
        //---------------------------
        GUILayout.BeginVertical("Box", GUILayout.ExpandWidth(true));
        //showSetStartEndPosition = EditorGUILayout.Foldout(showSetStartEndPosition, "Set Start/End Position");		
        showSetStartEndPosition = GUILayout.Toggle(showSetStartEndPosition, ("Set Start/End Position"), GUI.skin.GetStyle("foldout"), GUILayout.ExpandWidth(true), GUILayout.Height(18));

        if (showSetStartEndPosition)
        {
            EditorGUILayout.BeginVertical("Box");
            GUILayout.Label("Set start position: (player start)");
            EditorGUILayout.BeginHorizontal("TextArea");
            EditorGUILayout.BeginHorizontal("Box");
            scatterer.startPositionSelected = GUILayout.SelectionGrid(scatterer.startPositionSelected, options, 3, EditorStyles.toggle, GUILayout.Width(50));
            EditorGUILayout.BeginVertical();
            GUILayout.Label("Position: " + GetPosition(scatterer.startPositionSelected));
            scatterer.startPositionGo = EditorGUILayout.ObjectField("Gameobject:", scatterer.startPositionGo, typeof(GameObject), true, GUILayout.MinWidth(50)) as GameObject;
            scatterer.startPositionOffset = EditorGUILayout.Vector3Field("Offset:", scatterer.startPositionOffset);

            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();


            EditorGUILayout.BeginVertical("Box");
            GUILayout.Label("Set end position: (level end)");
            EditorGUILayout.BeginHorizontal("TextArea");
            EditorGUILayout.BeginHorizontal("Box");
            scatterer.endPositionSelected = GUILayout.SelectionGrid(scatterer.endPositionSelected, options, 3, EditorStyles.toggle, GUILayout.Width(50));
            EditorGUILayout.BeginVertical();
            GUILayout.Label("Position: " + GetPosition(scatterer.endPositionSelected));
            scatterer.endPositionGo = EditorGUILayout.ObjectField("Gameobject:", scatterer.endPositionGo, typeof(GameObject), true, GUILayout.MinWidth(50)) as GameObject;
            scatterer.endPositionOffset = EditorGUILayout.Vector3Field("offset", scatterer.endPositionOffset);

            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            scatterer.placeTileTypeSelect = GUILayout.SelectionGrid(scatterer.placeTileTypeSelect, placeOptions, placeOptions.Length, EditorStyles.radioButton);


            if (GUILayout.Button("Place start/end objects", GUILayout.Height(30)))
            {
                scatterer.PlaceStartEndObjectsEditor();
            }

        }
        GUILayout.EndVertical();


        //Show Set Random objects
        //-----------------------
        GUILayout.BeginVertical("Box", GUILayout.ExpandWidth(true));
        //showSetRandomObjects = EditorGUILayout.Foldout(showSetRandomObjects, "Set Random objects");		
        showSetRandomObjects = GUILayout.Toggle(showSetRandomObjects, ("Set Random objects"), GUI.skin.GetStyle("foldout"), GUILayout.ExpandWidth(true), GUILayout.Height(18));

        if (showSetRandomObjects)
        {
            EditorGUILayout.BeginVertical("Box");
            GUILayout.Label("Random objects");

            DragDropArea();

            if (GUILayout.Button("Add new Gameobject"))
            {
                AddNewRandomObjects();
            }
            

           EditorGUILayout.BeginVertical("TextArea");

           Event evt = Event.current;
           if (evt.type != EventType.DragPerform)
           {

               for (int i = 0; i < scatterer.objects.Count; i++)
               {
                   GUI.color = guiBlue;
                   EditorGUILayout.BeginVertical("Box");
                   GUI.color = Color.white;

                   GUILayout.BeginHorizontal();
                   scatterer.showObjects[i] = GUILayout.Toggle(scatterer.showObjects[i], (scatterer.objects[i].name), GUI.skin.GetStyle("foldout"), GUILayout.ExpandWidth(true), GUILayout.Height(18));

                   if (GUILayout.Button(iconDuplicate, "ToolbarButton", GUILayout.Height(18), GUILayout.Width(20)))
                   {
                       DuplicateObject(i);
                   }

                   GUI.color = guiRed;

                   //if (i > 0)
                   //{
                   if (GUILayout.Button("x", "ToolbarButton", GUILayout.Height(18), GUILayout.Width(20)))
                   {
                       RemoveRandomObjects(i);
                   }
                   //}
                   GUI.color = Color.white;

                   GUILayout.EndHorizontal();

                   if (i < scatterer.objects.Count)
                   {
                       if (scatterer.showObjects[i])
                       {

                           if (i < scatterer.objects.Count)
                           {
                               scatterer.objects[i] = EditorGUILayout.ObjectField("Gameobject:", scatterer.objects[i], typeof(GameObject), true) as GameObject;
                               scatterer.weights[i] = EditorGUILayout.Slider("Weight:", scatterer.weights[i], 0f, 1f);
                               scatterer.offsets[i] = EditorGUILayout.Vector3Field("Offset:", scatterer.offsets[i]);
                               scatterer.rndRotation[i] = EditorGUILayout.Vector3Field("Random rotation:", scatterer.rndRotation[i]);

                               scatterer.uniformScaling[i] = EditorGUILayout.Toggle("Uniform scaling:", scatterer.uniformScaling[i]);

                               if (scatterer.uniformScaling[i])
                               {
                                   _uniformScalingMin = EditorGUILayout.FloatField("Random scaling min:", scatterer.rndScalingMin[i].x);
                                   _uniformScalingMax = EditorGUILayout.FloatField("Random scaling max:", scatterer.rndScalingMax[i].x);

                                   scatterer.rndScalingMin[i] = new Vector3(_uniformScalingMin, _uniformScalingMin, _uniformScalingMin);
                                   scatterer.rndScalingMax[i] = new Vector3(_uniformScalingMax, _uniformScalingMax, _uniformScalingMax);
                               }
                               else
                               {
                                   scatterer.rndScalingMin[i] = EditorGUILayout.Vector3Field("Random scaling min:", scatterer.rndScalingMin[i]);
                                   scatterer.rndScalingMax[i] = EditorGUILayout.Vector3Field("Random scaling max:", scatterer.rndScalingMax[i]);
                               }


                               scatterer.spawnChilds[i] = EditorGUILayout.Toggle("Spawn childs:", scatterer.spawnChilds[i]);

                               if (scatterer.spawnChilds[i])
                               {
                                   EditorGUILayout.BeginVertical("Box");
                                   scatterer.childCount[i] = EditorGUILayout.IntField("Child count:", scatterer.childCount[i]);
                                   scatterer.spawnRadius[i] = EditorGUILayout.FloatField("Spawn radius:", scatterer.spawnRadius[i]);
                                   scatterer.rndRotationChilds[i] = EditorGUILayout.Vector3Field("Random rotation:", scatterer.rndRotationChilds[i]);

                                   scatterer.uniformScalingChilds[i] = EditorGUILayout.Toggle("Uniform scaling:", scatterer.uniformScalingChilds[i]);

                                   if (scatterer.uniformScalingChilds[i])
                                   {
                                       _uniformScalingChildsMin = EditorGUILayout.FloatField("Random scaling min:", scatterer.rndScalingChildsMin[i].x);
                                       _uniformScalingChildsMax = EditorGUILayout.FloatField("Random scaling max:", scatterer.rndScalingChildsMax[i].x);

                                       scatterer.rndScalingChildsMin[i] = new Vector3(_uniformScalingChildsMin, _uniformScalingChildsMin, _uniformScalingChildsMin);
                                       scatterer.rndScalingChildsMax[i] = new Vector3(_uniformScalingChildsMax, _uniformScalingChildsMax, _uniformScalingChildsMax);
                                   }
                                   else
                                   {
                                       scatterer.rndScalingChildsMin[i] = EditorGUILayout.Vector3Field("Random scaling min:", scatterer.rndScalingChildsMin[i]);
                                       scatterer.rndScalingChildsMax[i] = EditorGUILayout.Vector3Field("Random scaling max:", scatterer.rndScalingChildsMax[i]);
                                   }


                                   EditorGUILayout.EndVertical();
                               }

                               EditorGUILayout.BeginHorizontal();

                               GUILayout.Label("Place on layer:");

                               //add new list array for downgrade compatibility
                               if (scatterer.placeOnLayerSelected.Count == 0)
                               {
                                   AddPlaceOnLayerSelected();
                               }

                               scatterer.placeOnLayerSelected[i] = EditorGUILayout.Popup(scatterer.placeOnLayerSelected[i], placeLayers);

                               EditorGUILayout.EndHorizontal();

                               if (scatterer.placeOnLayerSelected[i] == 0)
                               {
                                   scatterer.placeTileSelect[i] = GUILayout.SelectionGrid(scatterer.placeTileSelect[i], placeOptions, placeOptions.Length, EditorStyles.radioButton);
                               }
                               else
                               {
                                   scatterer.placeTileSelect[i] = 0;
                               }
                           }


                       }
                   }
                   EditorGUILayout.EndVertical();
               }
           }

            EditorGUILayout.EndVertical();



            EditorGUILayout.EndVertical();


            if (GUILayout.Button("Scatter random objects", GUILayout.Height(30)))
            {
                scatterer.PlaceRandomObjectsEditor(false, false, false);
            }

            if (GUILayout.Button("Merge", GUILayout.Height(30)))
            {
                scatterer.Merge();
            }
        }
        GUILayout.EndVertical();

        //if(GUI.changed){ 
        //	EditorUtility.SetDirty(creator);
        //	creator.SetNewSettings();
        //}

    }


    void DragDropArea()
    {
        Event evt = Event.current;
        Rect drop_area = GUILayoutUtility.GetRect(0.0f, 50.0f, GUILayout.ExpandWidth(true));
        GUI.Box(drop_area, "Drag and drop prefabs here for automatic assignment");

        switch (evt.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!drop_area.Contains(evt.mousePosition))
                    return;

                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                if (evt.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();


                    foreach(Object dropObject in DragAndDrop.objectReferences)
                    {
                        AddNewRandomObjects();
                        scatterer.objects[scatterer.objects.Count - 1] = dropObject as GameObject;
                    }

                }
                break;
        }
    }


    void DuplicateObject(int _i)
    {
        AddNewRandomObjects();

        scatterer.objects[scatterer.objects.Count - 1] = scatterer.objects[_i];
        scatterer.weights[scatterer.weights.Count - 1] = scatterer.weights[_i];
        scatterer.showObjects[scatterer.showObjects.Count - 1] = scatterer.showObjects[_i];
        scatterer.placeTileSelect[scatterer.placeTileSelect.Count - 1] = scatterer.placeTileSelect[_i];
        scatterer.placeOnLayerSelected[scatterer.placeOnLayerSelected.Count - 1] = scatterer.placeOnLayerSelected[_i];
        scatterer.offsets[scatterer.offsets.Count - 1] = scatterer.offsets[_i];
        scatterer.rndRotation[scatterer.rndRotation.Count - 1] = scatterer.rndRotation[_i];
        scatterer.uniformScaling[scatterer.uniformScaling.Count - 1] = scatterer.uniformScaling[_i];
        scatterer.rndScalingMin[scatterer.rndScalingMin.Count - 1] = scatterer.rndScalingMin[_i];
        scatterer.rndScalingMax[scatterer.rndScalingMax.Count - 1] = scatterer.rndScalingMax[_i];
        scatterer.spawnChilds[scatterer.spawnChilds.Count - 1] = scatterer.spawnChilds[_i];
        scatterer.spawnRadius[scatterer.spawnRadius.Count - 1] = scatterer.spawnRadius[_i];
        scatterer.childCount[scatterer.childCount.Count - 1] = scatterer.childCount[_i];
        scatterer.rndRotationChilds[scatterer.rndRotationChilds.Count - 1] = scatterer.rndRotationChilds[_i];
        scatterer.uniformScalingChilds[scatterer.uniformScalingChilds.Count - 1] = scatterer.uniformScalingChilds[_i];
        scatterer.rndScalingChildsMin[scatterer.rndScalingChildsMin.Count - 1] = scatterer.rndScalingChildsMin[_i];
        scatterer.rndScalingChildsMax[scatterer.rndScalingChildsMax.Count - 1] = scatterer.rndScalingChildsMax[_i];
    }

    void AddNewRandomObjects()
    {
        scatterer.objects.Add(null);
        float _f = 0.5f;
        scatterer.weights.Add(_f);
        scatterer.showObjects.Add(false);
        scatterer.placeTileSelect.Add(new int());
        scatterer.placeOnLayerSelected.Add(new int());
        scatterer.offsets.Add(new Vector3(0.5f, 0.5f, 0.5f));
        scatterer.rndRotation.Add(new Vector3(0, 0, 0));
        scatterer.uniformScaling.Add(false);
        scatterer.rndScalingMin.Add(new Vector3(1, 1, 1));
        scatterer.rndScalingMax.Add(new Vector3(1, 1, 1));
        scatterer.spawnChilds.Add(new bool());
        scatterer.spawnRadius.Add(new float());
        scatterer.childCount.Add(new int());
        scatterer.rndRotationChilds.Add(new Vector3(0, 0, 0));
        scatterer.uniformScalingChilds.Add(false);
        scatterer.rndScalingChildsMin.Add(new Vector3(1, 1, 1));
        scatterer.rndScalingChildsMax.Add(new Vector3(1, 1, 1));
    }

    void RemoveRandomObjects(int _i)
    {
        scatterer.objects.RemoveAt(_i);
        scatterer.weights.RemoveAt(_i);
        scatterer.showObjects.RemoveAt(_i);
        scatterer.placeTileSelect.RemoveAt(_i);
        scatterer.placeOnLayerSelected.RemoveAt(_i);
        scatterer.offsets.RemoveAt(_i);
        scatterer.uniformScaling.RemoveAt(_i);
        scatterer.rndRotation.RemoveAt(_i);
        scatterer.rndScalingMin.RemoveAt(_i);
        scatterer.rndScalingMax.RemoveAt(_i);
        scatterer.spawnChilds.RemoveAt(_i);
        scatterer.spawnRadius.RemoveAt(_i);
        scatterer.childCount.RemoveAt(_i);
        scatterer.rndRotationChilds.RemoveAt(_i);
        scatterer.uniformScalingChilds.RemoveAt(_i);
        scatterer.rndScalingChildsMin.RemoveAt(_i);
        scatterer.rndScalingChildsMax.RemoveAt(_i);
    }


    void AddPlaceOnLayerSelected()
    {
        for (int i = 0; i < scatterer.objects.Count; i++)
        {
            scatterer.placeTileSelect.Add(new int());
            scatterer.placeOnLayerSelected.Add(new int());
        }
    }

    string GetPosition(int _inx)
    {
        string _pos = "";
        switch (_inx)
        {
            case 0:
                _pos = "topLeft";
                break;
            case 1:
                _pos = "topCenter";
                break;
            case 2:
                _pos = "topRight";
                break;
            case 3:
                _pos = "middleLeft";
                break;
            case 4:
                _pos = "middleCenter";
                break;
            case 5:
                _pos = "middleRight";
                break;
            case 6:
                _pos = "bottomLeft";
                break;
            case 7:
                _pos = "bottomCenter";
                break;
            case 8:
                _pos = "bottomRight";
                break;
        }
        return _pos;
    }


    void LoadResources()
    {
        var _path = ReturnInstallPath.GetInstallPath("Editor", this);

        iconDuplicate = AssetDatabase.LoadAssetAtPath(_path + "Res/icon_duplicate.png", typeof(Texture2D)) as Texture2D;
    }
}
#endif