﻿using UnityEngine;
using UnityEditor;
using System.Collections;

//Disable Log warnings for assigned but not used variables 
#pragma warning disable 0219
namespace TileWorld
{
    public class TileWorldCreatorEditorPresetsWindow : EditorWindow
    {
        public static TileWorldCreatorEditorPresetsWindow window;
        static TileWorldCreatorEditor[] editor;
        static GameObject obj;

        Vector2 scrollPosition = Vector2.zero;

        public static void InitWindow(GameObject _obj)
        {
            // Get existing open window or if none, make a new one:
            window = (TileWorldCreatorEditorPresetsWindow)EditorWindow.GetWindow(typeof(TileWorldCreatorEditorPresetsWindow));
            editor = (TileWorldCreatorEditor[])Resources.FindObjectsOfTypeAll(typeof(TileWorldCreatorEditor));
            obj = _obj;
        }


        void OnGUI()
        {
            if (GUILayout.Button("select"))
            {
                if (obj != null)
                {
                    Selection.activeGameObject = obj;
                }
                else
                {
                    TileWorldCreator _c = GameObject.FindObjectOfType<TileWorldCreator>();
                    obj = _c.gameObject;
                    InitWindow(obj);
                }
            }

            if (window != null)
            {
                int _tilesets = 0;
                for (int tp = 0; tp < editor[0].creator.presets.Count; tp++)
                {
                    _tilesets += editor[0].creator.presets[tp].tiles.Count;
                }

                scrollPosition = GUI.BeginScrollView(new Rect(0, 25, Screen.width, Screen.height - 50), scrollPosition, new Rect(0, 20, Screen.width - 50, (_tilesets * 295f) + 10f));
                editor[0].ShowPresets();
                GUI.EndScrollView();
            }
        }
    }
}
