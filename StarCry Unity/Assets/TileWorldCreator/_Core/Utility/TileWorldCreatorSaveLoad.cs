﻿/* TILE WORLD CREATOR
 * Copyright (c) 2015 doorfortyfour OG
 * 
 * TileWorldSaveLoad.cs
 * 
 * handels the save and load functionality of a map
 * 
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;


namespace TileWorld
{
    public class TileWorldCreatorSaveLoad : MonoBehaviour
    {

        static string saveMapContent = "";

        [System.Serializable]
        public class SaveMap
        {
            public bool[] map = new bool[0];
            public bool[] maskMap = new bool[0];

            public int width = 0;
            public int height = 0;

            public bool useMask = false;
            public int selectedMask = 0;

            public SaveMap() { }

            public SaveMap(bool[] _map, bool[] _maskMap, int _w, int _h, bool _useMask, int _selectedMask)
            {
                map = new bool[_map.Length];
                map = _map;

                maskMap = new bool[_maskMap.Length];
                maskMap = _maskMap;

                width = _w;
                height = _h;

                useMask = _useMask;
                selectedMask = _selectedMask;
            }
        }


        //Save and Load Methods
        //--------
        public static void Save(string _path, TileWorldCreator _creator)
        {
            saveMapContent = "";
          
            if (_path == "")
                return;


            for (int i = 0; i < _creator.worldMap.Count; i++)
            {
                bool[] _tmpMapSingle = new bool[_creator.worldMap[i].mapSingle.Length];
                bool[] _tmpMaskMapSingle = new bool[_creator.worldMap[i].maskMapSingle.Length];

                _tmpMapSingle = _creator.worldMap[i].mapSingle;
                _tmpMaskMapSingle = _creator.worldMap[i].maskMapSingle;

                SaveMap _map = new SaveMap(_tmpMapSingle, _tmpMaskMapSingle, _creator.width, _creator.height, _creator.worldMap[i].useMask, _creator.worldMap[i].selectedMask);
                

                AddData<SaveMap>(_map);
            }


            FileStream _fs = new FileStream(_path, FileMode.Create, FileAccess.Write);
            StreamWriter _sw = new StreamWriter(_fs);

            _sw.Write(saveMapContent);

            _sw.Flush();
            _sw.Close();
        }


        static void AddData<T>(T _data)
        {
            StringWriter _sw = new StringWriter();

            XmlSerializer _serializer = new XmlSerializer(typeof(T));
            _serializer.Serialize(_sw, _data);

            //add simple tag between each xml map
            saveMapContent += _sw + "---"; // + System.Environment.NewLine;
            _sw.Close();
        }



        public static void Load(string _path, TileWorldCreator _creator)
        {
 
            if (_path == "")
                return;

            FileStream _file = new FileStream(_path, FileMode.Open, FileAccess.Read);
            StreamReader _sr = new StreamReader(_file);


            saveMapContent = _sr.ReadToEnd();


            //split file in several xml maps
            string[] _sp = new string[] { "---" };
            string[] lines = saveMapContent.Split(_sp, System.StringSplitOptions.None); //("---"[0]);

            _creator.worldMap = new List<TileWorldCreator.Maps>();

            _creator.layerCount = lines.Length - 1;

            //clear all settings except presets
            _creator.ClearSettings(false, lines.Length - 1);

            //load back map for each layer
            for (int l = 0; l < lines.Length - 1; l++)
            {

                SaveMap _map = GetData<SaveMap>(lines[l]);

                _creator.width = _map.width;
                _creator.height = _map.height;



                _creator.worldMap.Add(new TileWorldCreator.Maps(_creator.width, _creator.height, false, _map.useMask, _map.selectedMask));
               

                //Load back multidimensional array from single dim array
                _creator.worldMap[l].map = new bool[_creator.width, _creator.height];
                _creator.worldMap[l].gameObjectsMap = new GameObject[_creator.width, _creator.height];
                _creator.worldMap[l].oldMap = new bool[_creator.width, _creator.height];

                int _index = 0;

                

                for (int y = 0; y < _creator.height; y++)
                {
                    for (int x = 0; x < _creator.width; x++)
                    {
                        _creator.worldMap[l].map[x, y] = _map.map[_index]; // creator.worldMap[l].mapSingle[_index];	

                        if (_map.maskMap.Length > 0)
                        {   
                            _creator.worldMap[l].maskMap[x, y] = _map.maskMap[_index]; 
                        }
                        
                        
                        _index++;
                    }
                }

                _creator.ResizeIntArray(false, 0);


            }

            _sr.Close();
        }


        static T GetData<T>(string _data)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StringReader _sr = new StringReader(_data);
          
            return (T)serializer.Deserialize(_sr);
        }

    }
}
